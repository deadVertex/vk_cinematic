#include "unity.h"
#include "unity_fixture.h"

// NOTE: Stolen from config.h
// Maximum radiance value to clamp to before tone mapping (this is used to
// reduce fireflies)
#define RADIANCE_CLAMP 10

#include "platform.h"
#include "math_lib.h"
#include "asset_loader/asset_loader.h" // For HdrImage
#include "image.h"
#include "cubemap.h"

#include "custom_assertions.h"

#include "cubemap.cpp"

TEST_GROUP(Mapping);

TEST_SETUP(Mapping)
{
}

TEST_TEAR_DOWN(Mapping)
{
}

TEST(Mapping, TestToSphericalCoordinates)
{
    // Given
    vec3 inputDirections[] = {
        Vec3(1, 0, 0),
        Vec3(0, 0, -1),
        Vec3(0, 1, 0),
        Normalize(Vec3(0, 1, 1)),
        Vec3(-1, 0, 0),
    };
    vec2 expectedSphereCoords[] = {
        Vec2(0.0f, PI * 0.5f),
        Vec2(-PI * 0.5f, PI * 0.5f),
        Vec2(0.0f, 0.0f),
        Vec2(PI * 0.5f, PI * 0.25f),
        Vec2(PI, PI * 0.5f),
    };

    for (u32 i = 0; i < ArrayCount(inputDirections); ++i)
    {
        vec3 direction = inputDirections[i];
        vec2 expected = expectedSphereCoords[i];

        // When
        vec2 sphereCoords = ToSphericalCoordinates(direction);

        // Then
        TEST_ASSERT_EQUAL_FLOAT(expected.x, sphereCoords.x);
        TEST_ASSERT_EQUAL_FLOAT(expected.y, sphereCoords.y);
    }
}

TEST(Mapping, TestMapToEquirectangular)
{
    // Given
    vec2 inputSphereCoords[] = {
        Vec2(0.0f, 0.0f),
        Vec2(0.0f, PI),
        Vec2(0.0f, PI * 0.5f),
        Vec2(PI * 0.5f, PI * 0.5f),
        Vec2(PI, PI * 0.5f),
        Vec2(PI * -0.5f, PI * 0.5f),
    };

    vec2 expectedUVs[] = {
        Vec2(0.0f, 1.0f),
        Vec2(0.0f, 0.0f),
        Vec2(0.0f, 0.5f),
        Vec2(0.25f, 0.5f),
        Vec2(0.5f, 0.5f),
        Vec2(0.75f, 0.5f),
    };

    for (u32 i = 0; i < ArrayCount(inputSphereCoords); ++i)
    {
        vec2 sphereCoords = inputSphereCoords[i];
        vec2 expected = expectedUVs[i];

        // When
        vec2 uv = MapToEquirectangular(sphereCoords);

        // Then
        TEST_ASSERT_EQUAL_FLOAT(expected.x, uv.x);
        TEST_ASSERT_EQUAL_FLOAT(expected.y, uv.y);
    }
}

TEST(Mapping, TestMapEquirectangularToSphereCoordinates)
{
    // Given
    vec2 inputUVs[] = {
        Vec2(0.0f, 1.0f),
        Vec2(0.0f, 0.0f),
        Vec2(0.0f, 0.5f),
        Vec2(0.25f, 0.5f),
        Vec2(0.5f, 0.5f),
        Vec2(0.75f, 0.5f),
    };

    vec2 expectedSphereCoords[] = {
        Vec2(0.0f, 0.0f),
        Vec2(0.0f, PI),
        Vec2(0.0f, PI * 0.5f),
        Vec2(PI * 0.5f, PI * 0.5f),
        Vec2(PI, PI * 0.5f),
        Vec2(PI * -0.5f, PI * 0.5f),
    };

    for (u32 i = 0; i < ArrayCount(inputUVs); ++i)
    {
        vec2 uv = inputUVs[i];
        vec2 expected = expectedSphereCoords[i];

        // When
        vec2 sphereCoords = MapEquirectangularToSphereCoordinates(uv);

        // Then
        TEST_ASSERT_EQUAL_FLOAT(expected.x, sphereCoords.x);
        TEST_ASSERT_EQUAL_FLOAT(expected.y, sphereCoords.y);
    }
}

TEST(Mapping, TestMapSphericalToCartesianCoordinates)
{
    // Given
    vec2 inputSphereCoords[] = {
        Vec2(0.0f, PI * 0.5f),
        Vec2(-PI * 0.5f, PI * 0.5f),
        Vec2(0.0f, 0.0f),
        Vec2(PI * 0.5f, PI * 0.25f),
        Vec2(PI, PI * 0.5f),
    };

    vec3 expectedDirections[] = {
        Vec3(1, 0, 0),
        Vec3(0, 0, -1),
        Vec3(0, 1, 0),
        Normalize(Vec3(0, 1, 1)),
        Vec3(-1, 0, 0),
    };

    for (u32 i = 0; i < ArrayCount(inputSphereCoords); ++i)
    {
        vec2 sphereCoords = inputSphereCoords[i];
        vec3 expected = expectedDirections[i];

        // When
        vec3 direction = MapSphericalToCartesianCoordinates(sphereCoords);

        // Then
        TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, expected.x, direction.x, "X axis");
        TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, expected.y, direction.y, "Y axis");
        TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, expected.z, direction.z, "Z axis");

        TEST_ASSERT_EQUAL_FLOAT(1.0f, Length(direction));
    }
}

TEST(Mapping, TestSphereCoordsSample)
{
    f32 phi = 0.0f;
    f32 theta = 0.0f;

    // Get sample vector in tangent space
    vec3 tangentDir = MapSphericalToCartesianCoordinates(Vec2(phi, theta));

    AssertWithinVec3(EPSILON, Vec3(0, 1, 0), tangentDir);

    vec3 normal = Vec3(1, 0, 0);
    vec3 tangent = Vec3(0, 1, 0);
    vec3 bitangent = Vec3(0, 0, -1);

    vec3 worldDir = normal * tangentDir.y + tangent * tangentDir.x +
                    bitangent * tangentDir.z;

    AssertWithinVec3(EPSILON, Vec3(1, 0, 0), worldDir);
}

TEST(Mapping, TestMapCubeMapFaceToVector)
{
    u32 layerIndices[] = {
        CubeMapFace_PositiveX,
        CubeMapFace_NegativeX,
        CubeMapFace_PositiveY,
        CubeMapFace_NegativeY,
        CubeMapFace_PositiveZ,
        CubeMapFace_NegativeZ,
    };

    vec3 directions[] = {
        Vec3(1, 0, 0),
        Vec3(-1, 0, 0),
        Vec3(0, 1, 0),
        Vec3(0, -1, 0),
        Vec3(0, 0, 1),
        Vec3(0, 0, -1),
    };

    for (u32 i = 0; i < ArrayCount(layerIndices); ++i)
    {
        vec3 expected = directions[i];
        vec3 dir = MapCubeMapLayerIndexToVector(layerIndices[i]);
        AssertWithinVec3(EPSILON, expected, dir);
    }
}

TEST(Mapping, TestMapCubeMapFaceToBasisVectors)
{
    u32 layerIndices[] = {
        CubeMapFace_PositiveX,
        CubeMapFace_NegativeX,
        CubeMapFace_PositiveY,
        CubeMapFace_NegativeY,
        CubeMapFace_PositiveZ,
        CubeMapFace_NegativeZ,
    };

    // { forward, up, right } (normal, tangent, bitangent)?
    BasisVectors basisVectors[] = {
        {Vec3(1, 0, 0), Vec3(0, 1, 0), Vec3(0, 0, -1)}, // +X
        {Vec3(-1, 0, 0), Vec3(0, 1, 0), Vec3(0, 0, 1)}, // -X
        {Vec3(0, 1, 0), Vec3(0, 0, -1), Vec3(1, 0, 0)}, // +Y
        {Vec3(0, -1, 0), Vec3(0, 0, 1), Vec3(1, 0, 0)}, // -Y
        {Vec3(0, 0, 1), Vec3(0, 1, 0), Vec3(1, 0, 0)}, // +Z
        {Vec3(0, 0, -1), Vec3(0, 1, 0), Vec3(-1, 0, 0)}, // -Z
    };

    for (u32 i = 0; i < ArrayCount(layerIndices); ++i)
    {
        BasisVectors expected = basisVectors[i];
        BasisVectors actual =
            MapCubeMapLayerIndexToBasisVectors(layerIndices[i]);
        AssertWithinVec3(EPSILON, expected.forward, actual.forward);
        AssertWithinVec3(EPSILON, expected.up, actual.up);
        AssertWithinVec3(EPSILON, expected.right, actual.right);
    }
}

TEST_GROUP_RUNNER(Mapping)
{
    RUN_TEST_CASE(Mapping, TestToSphericalCoordinates);
    RUN_TEST_CASE(Mapping, TestMapToEquirectangular);

    RUN_TEST_CASE(Mapping, TestMapEquirectangularToSphereCoordinates);
    RUN_TEST_CASE(Mapping, TestMapSphericalToCartesianCoordinates);
    RUN_TEST_CASE(Mapping, TestSphereCoordsSample);

    RUN_TEST_CASE(Mapping, TestMapCubeMapFaceToVector);
    RUN_TEST_CASE(Mapping, TestMapCubeMapFaceToBasisVectors);

}
