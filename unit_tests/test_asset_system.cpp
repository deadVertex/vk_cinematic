#include <cstring>

#include "unity.h"
#include "unity_fixture.h"

#include "platform.h"
#include "asset_loader/asset_loader.h"
#include "dictionary.h"
#include "math_lib.h"
#include "image.h"
#include "mesh.h"
#include "aabb.h"
#include "asset_system.h"
#include "custom_assertions.h"

#include "image.cpp"
#include "asset_system.cpp"

TEST_GROUP(AssetSystem);

global MemoryArena memoryArena;
global AssetSystem assetSystem;

TEST_SETUP(AssetSystem)
{
    u32 size = Kilobytes(1);
    InitializeMemoryArena(&memoryArena, malloc(size), size);

    AssetSystemConfig config = {};
    config.maxTextures = 2;
    config.maxMeshes = 2;
    config.imageDataArenaSize = 64;
    config.meshDataArenaSize = 64;
    InitializeAssetSystem(&assetSystem, &memoryArena, config);
}

TEST_TEAR_DOWN(AssetSystem)
{
    free(memoryArena.base);
}

TEST(AssetSystem, MissingTexture)
{
    u32 textureId = 123;

    // Then we can retrieve it via an ID
    TEST_ASSERT_NULL(GetTexture(&assetSystem, textureId));
}

TEST(AssetSystem, AddTexture)
{
    u32 textureId = 123;

    // When we add an asset to the system
    const char *path = "path/to/texture.png";
    RegisterTextureFromDisk(&assetSystem, textureId, path);

    // Then we can retrieve it via an ID
    TEST_ASSERT_TRUE(HasTexture(&assetSystem, textureId));
}

TEST(AssetSystem, AddSingleColorTexture)
{
    u32 textureId = 123;

    // When we add an asset to the system
    RegisterSingleColorTexture(&assetSystem, textureId, Vec4(1, 0, 0, 1));

    // Then we can retrieve it via an ID
    TEST_ASSERT_TRUE(HasTexture(&assetSystem, textureId));
}

internal b32 DummyGenerateSingleColorTexture(
    HdrImage *image, MemoryArena *imageDataArena, vec4 color, void *userData)
{
    HdrImage *dummyImage = (HdrImage *)userData;
    *image = *dummyImage;
    return true;
}

TEST(AssetSystem, GetSingleColorTexture)
{
    u32 textureId = 123;
    RegisterSingleColorTexture(&assetSystem, textureId, Vec4(1, 0, 0, 1));

    AssetSystemGenerator generator = {};
    generator.singleColor = DummyGenerateSingleColorTexture;

    HdrImage generatedImage = {};
    generatedImage.width = 55;

    // When we generate assets
    GenerateAssetData(&assetSystem, generator, &generatedImage);

    // Then we can retrieve the image data
    HdrImage *image = GetTexture(&assetSystem, textureId);
    TEST_ASSERT_NOT_NULL(image);
    TEST_ASSERT_EQUAL_UINT32(generatedImage.width, image->width);
}

internal b32 DummyGenerateCheckerboardTexture(
    HdrImage *image, MemoryArena *imageDataArena, void *userData)
{
    HdrImage *dummyImage = (HdrImage *)userData;
    *image = *dummyImage;
    return true;
}

TEST(AssetSystem, CheckerboardTexture)
{
    u32 textureId = 123;
    RegisterCheckerboardTexture(&assetSystem, textureId);

    AssetSystemGenerator generator = {};
    generator.checkerboard = DummyGenerateCheckerboardTexture;

    HdrImage generatedImage = {};
    generatedImage.width = 123;

    // When we generate assets
    GenerateAssetData(&assetSystem, generator, &generatedImage);

    // Then we can retrieve the image data
    HdrImage *image = GetTexture(&assetSystem, textureId);
    TEST_ASSERT_NOT_NULL(image);
    TEST_ASSERT_EQUAL_UINT32(generatedImage.width, image->width);
}

internal b32 DummyReadTextureFromDisk(
    HdrImage *image, MemoryArena *imageDataArena, const char *path, void *userData)
{
    b32 result = true;
    *image = *(HdrImage *)userData;
    return result;
}

TEST(AssetSystem, LoadTextureFromDisk)
{
    u32 textureId = 123;
    const char *path = "path/to/texture.png";
    RegisterTextureFromDisk(&assetSystem, textureId, path);

    AssetSystemLoader loader = {};
    loader.readTextureFromDisk = &DummyReadTextureFromDisk;

    HdrImage loadedImage = {};
    loadedImage.width = 123;

    void *userData = &loadedImage;

    // When we load assets from disk
    LoadAssetsFromDisk(&assetSystem, loader, userData);

    // Then we can retrieve the image data
    HdrImage *image = GetTexture(&assetSystem, textureId);
    TEST_ASSERT_NOT_NULL(image);
    TEST_ASSERT_EQUAL_UINT32(loadedImage.width, image->width);
}

internal b32 DummyReadMeshFromDisk(MeshData *mesh, Aabb *meshAabb,
    MemoryArena *meshDataArena, const char *path, void *userData)
{
    b32 result = true;
    *mesh = *(MeshData *)userData;
    return result;
}

TEST(AssetSystem, LoadMeshFromDisk)
{
    u32 meshId = 123;
    const char *path = "path/to/mesh.obj";
    RegisterMeshFromDisk(&assetSystem, meshId, path);

    AssetSystemLoader loader = {};
    loader.readMeshFromDisk = &DummyReadMeshFromDisk;

    MeshData loadedMesh = {};
    loadedMesh.vertexCount = 123;

    void *userData = &loadedMesh;

    // When we load assets from disk
    LoadAssetsFromDisk(&assetSystem, loader, userData);

    // Then we can retrieve the mesh data
    MeshData *mesh = GetMesh(&assetSystem, meshId);
    TEST_ASSERT_NOT_NULL(mesh);
    TEST_ASSERT_EQUAL_UINT32(loadedMesh.vertexCount, mesh->vertexCount);
}

internal b32 DummyMeshGenerator(
    MeshData *mesh, Aabb *meshAabb, u32 generatorId, MemoryArena *arena, void *userData)
{
    b32 result = false;
    if (generatorId == 14)
    {
        *mesh = *(MeshData *)userData;
        result = true;
    }
    return result;
}

TEST(AssetSystem, GeneratedMesh)
{
    u32 meshId = 5;
    u32 generatorId = 14;
    RegisterGeneratedMesh(&assetSystem, meshId, generatorId);

    AssetSystemGenerator generator = {};
    generator.mesh = DummyMeshGenerator;

    MeshData generatedMesh = {};
    generatedMesh.vertexCount = 12;

    GenerateAssetData(&assetSystem, generator, &generatedMesh);

    MeshData *mesh = GetMesh(&assetSystem, meshId);
    TEST_ASSERT_NOT_NULL(mesh);
    TEST_ASSERT_EQUAL_UINT32(generatedMesh.vertexCount, mesh->vertexCount);
}

internal b32 DummyMeshAabbGenerator(
    MeshData *mesh, Aabb *meshAabb, u32 generatorId, MemoryArena *arena, void *userData)
{
    b32 result = true;
    *meshAabb = *(Aabb*)userData;
    return result;
}

TEST(AssetSystem, GeneratedMeshAabb)
{
    u32 meshId = 5;
    u32 generatorId = 14;
    RegisterGeneratedMesh(&assetSystem, meshId, generatorId);

    AssetSystemGenerator generator = {};
    generator.mesh = DummyMeshAabbGenerator;

    Aabb generatedAabb = { Vec3(-0.5f), Vec3(0.5f) };
    GenerateAssetData(&assetSystem, generator, &generatedAabb);

    Aabb *meshAabb = GetMeshAabb(&assetSystem, meshId);
    TEST_ASSERT_NOT_NULL(meshAabb);
    AssertWithinVec3(EPSILON, Vec3(-0.5f), meshAabb->min);
    AssertWithinVec3(EPSILON, Vec3(0.5f), meshAabb->max);
}

internal b32 DummyLoadMeshFromDiskAabb(MeshData *mesh, Aabb *meshAabb,
    MemoryArena *meshDataArena, const char *path, void *userData)
{
    b32 result = true;
    *meshAabb = *(Aabb*)userData;
    return result;
}

TEST(AssetSystem, LoadedMeshAabb)
{
    u32 meshId = 123;
    const char *path = "path/to/mesh.obj";
    RegisterMeshFromDisk(&assetSystem, meshId, path);

    AssetSystemLoader loader = {};
    loader.readMeshFromDisk = &DummyLoadMeshFromDiskAabb;

    Aabb loadedMeshAabb = { Vec3(1), Vec3(2) };

    void *userData = &loadedMeshAabb;

    // When we load assets from disk
    LoadAssetsFromDisk(&assetSystem, loader, userData);

    // Then we can retrieve the mesh aabb
    Aabb *meshAabb = GetMeshAabb(&assetSystem, meshId);
    TEST_ASSERT_NOT_NULL(meshAabb);
    AssertWithinVec3(EPSILON, Vec3(1), meshAabb->min);
    AssertWithinVec3(EPSILON, Vec3(2), meshAabb->max);
}

TEST_GROUP_RUNNER(AssetSystem)
{
    RUN_TEST_CASE(AssetSystem, AddTexture);
    RUN_TEST_CASE(AssetSystem, MissingTexture);
    RUN_TEST_CASE(AssetSystem, AddSingleColorTexture);
    RUN_TEST_CASE(AssetSystem, GetSingleColorTexture);
    RUN_TEST_CASE(AssetSystem, CheckerboardTexture);
    RUN_TEST_CASE(AssetSystem, LoadTextureFromDisk);
    RUN_TEST_CASE(AssetSystem, LoadMeshFromDisk);
    RUN_TEST_CASE(AssetSystem, GeneratedMesh);
    RUN_TEST_CASE(AssetSystem, GeneratedMeshAabb);
    RUN_TEST_CASE(AssetSystem, LoadedMeshAabb);
}
