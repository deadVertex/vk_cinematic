#include "unity.h"
#include "unity_fixture.h"

#include "platform.h"
#include "intrinsics.h"
#include "math_lib.h"
#include "tile.h"
#include "work_queue.h"

TEST_GROUP(WorkQueue);

global MemoryArena memoryArena;

TEST_SETUP(WorkQueue)
{
    u32 size = Kilobytes(1);
    InitializeMemoryArena(&memoryArena, malloc(size), size);
}

TEST_TEAR_DOWN(WorkQueue)
{
    free(memoryArena.base);
}

TEST(WorkQueue, TestComputeTiles)
{
    Tile tiles[64] = {};
    u32 tileCount =
        ComputeTiles(10, 10, 2, 2, tiles, ArrayCount(tiles));

    TEST_ASSERT_EQUAL_UINT32(tiles[0].minX, 0);
    TEST_ASSERT_EQUAL_UINT32(tiles[0].minY, 0);
    TEST_ASSERT_EQUAL_UINT32(tiles[0].maxX, 2);
    TEST_ASSERT_EQUAL_UINT32(tiles[0].maxY, 2);
    TEST_ASSERT_EQUAL_UINT32(tiles[1].minX, 2);
    TEST_ASSERT_EQUAL_UINT32(tiles[1].minY, 0);
    TEST_ASSERT_EQUAL_UINT32(tiles[1].maxX, 4);
    TEST_ASSERT_EQUAL_UINT32(tiles[1].maxY, 2);

    TEST_ASSERT_EQUAL_UINT32(tileCount, 5 * 5);
}

TEST(WorkQueue, TestComputeTilesNonDivisible)
{
    Tile tiles[64] = {};
    u32 tileCount =
        ComputeTiles(9, 9, 2, 2, tiles, ArrayCount(tiles));

    TEST_ASSERT_EQUAL_UINT32(tileCount, 5 * 5);

    TEST_ASSERT_EQUAL_UINT32(tiles[24].minX, 8);
    TEST_ASSERT_EQUAL_UINT32(tiles[24].minY, 8);
    TEST_ASSERT_EQUAL_UINT32(tiles[24].maxX, 9);
    TEST_ASSERT_EQUAL_UINT32(tiles[24].maxY, 9);
}

TEST(WorkQueue, TestComputeTilesInsufficientSpace)
{
    Tile tiles[10] = {};
    u32 tileCount =
        ComputeTiles(10, 10, 2, 2, tiles, ArrayCount(tiles));

    TEST_ASSERT_EQUAL_UINT32(tileCount, ArrayCount(tiles));
}

struct TestWorkQueueTask
{
    u32 value;
};

TEST(WorkQueue, TestWorkQueuePush)
{
    // Given a work queue
    WorkQueue queue = CreateWorkQueue(&memoryArena, sizeof(TestWorkQueueTask), 4);

    // When I push on a new task onto the queue
    TestWorkQueueTask task = {};
    task.value = 1;
    TEST_ASSERT_TRUE(WorkQueuePush(&queue, &task, sizeof(task)));

    // Then the length of the queue increases
    TEST_ASSERT_EQUAL_UINT32(1, queue.tail);
}

TEST(WorkQueue, TestWorkQueuePop)
{
    // Given a work queue with tasks queued
    WorkQueue queue =
        CreateWorkQueue(&memoryArena, sizeof(TestWorkQueueTask), 4);

    TestWorkQueueTask tasksToAdd[2];
    tasksToAdd[0].value = 1;
    tasksToAdd[1].value = 2;

    WorkQueuePush(&queue, &tasksToAdd[0], sizeof(tasksToAdd[0]));
    WorkQueuePush(&queue, &tasksToAdd[1], sizeof(tasksToAdd[1]));

    // When I pop items from the queue
    TestWorkQueueTask first =
        *(TestWorkQueueTask *)WorkQueuePop(&queue, sizeof(TestWorkQueueTask));
    TestWorkQueueTask second =
        *(TestWorkQueueTask *)WorkQueuePop(&queue, sizeof(TestWorkQueueTask));

    // Then they are returned in the order they were added
    TEST_ASSERT_EQUAL_UINT32(first.value, 1);
    TEST_ASSERT_EQUAL_UINT32(second.value, 2);
}

TEST_GROUP_RUNNER(WorkQueue)
{
    RUN_TEST_CASE(WorkQueue, TestComputeTiles);
    RUN_TEST_CASE(WorkQueue, TestComputeTilesNonDivisible);
    RUN_TEST_CASE(WorkQueue, TestComputeTilesInsufficientSpace);
    RUN_TEST_CASE(WorkQueue, TestWorkQueuePush);
    RUN_TEST_CASE(WorkQueue, TestWorkQueuePop);
}
