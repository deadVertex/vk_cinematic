#include "unity.h"
#include "unity_fixture.h"

// NOTE: From config.h
#define USE_MT_RAY_TRIANGLE_INTERSECT 1

#include "platform.h"
#include "math_lib.h"
#include "ray_intersection.h"

#include "ray_intersection.cpp"

TEST_GROUP(RayIntersection);

TEST_SETUP(RayIntersection)
{
}

TEST_TEAR_DOWN(RayIntersection)
{
}

TEST(RayIntersection, TestRayIntersectTriangleHit)
{
    vec3 rayOrigin = Vec3(0, 0, 1);
    vec3 rayDirection = Vec3(0, 0, -1);
    vec3 vertices[] = {
        Vec3(-0.5f, -0.5f, -5.0f),
        Vec3(0.5f, -0.5f, -5.0f),
        Vec3(0.0f, 0.5f, -5.0f),
    };

    RayIntersectTriangleResult result = RayIntersectTriangle(
        rayOrigin, rayDirection, vertices[0], vertices[1], vertices[2]);

    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 6.0f, result.t);
    TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, 0.0f, result.normal.x, "X axis");
    TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, 0.0f, result.normal.y, "Y axis");
    TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, 1.0f, result.normal.z, "Z axis");
}

TEST(RayIntersection, TestRayIntersectTriangleMiss)
{
    vec3 rayOrigin = Vec3(0, 0, 1);
    vec3 rayDirection = Vec3(0, 0, -1);

    vec3 offset = Vec3(5.0f, 0.0f, 0.0f);
    vec3 vertices[] = {
        Vec3(-0.5f, -0.5f, -5.0f) + offset,
        Vec3(0.5f, -0.5f, -5.0f) + offset,
        Vec3(0.0f, 0.5f, -5.0f) + offset,
    };

    RayIntersectTriangleResult result = RayIntersectTriangle(
        rayOrigin, rayDirection, vertices[0], vertices[1], vertices[2]);

    TEST_ASSERT_FLOAT_WITHIN(EPSILON, -1.0f, result.t);
}

TEST(RayIntersection, TestRayIntersectTriangleHitUV)
{
    vec3 rayOrigin = Vec3(0, 0, 1);
    vec3 rayDirection = Vec3(0, 0, -1);
    vec3 offset = Vec3(0.5f, 0.5f, 0.0f);
    vec3 vertices[] = {
        Vec3(-0.5f, -0.5f, -5.0f) + offset,
        Vec3(0.5f, -0.5f, -5.0f) + offset,
        Vec3(0.0f, 0.5f, -5.0f) + offset,
    };

    RayIntersectTriangleResult result = RayIntersectTriangle(
        rayOrigin, rayDirection, vertices[0], vertices[1], vertices[2]);

    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 6.0f, result.t);

    f32 w = 1.0f - result.uv.x - result.uv.y;
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 1.0f, w);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 0.0f, result.uv.x);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 0.0f, result.uv.y);
}

/* Test to reproduce issue caused by not checking that all 3 barycentric
 * coordinates are within the 0-1 range when performing the Moller-Trumbore
 * triangle intersection algorithm.
 */
TEST(RayIntersection, TestRayIntersectTriangleMTBarycentricCoordsIssue)
{
    vec3 rayOrigin = Vec3(0.5, 0.5, 1);
    vec3 rayDirection = Vec3(0, 0, -1);

    vec3 vertices[] = {
        Vec3(-0.5f, -0.5f, -5.0f),
        Vec3(0.5f, -0.5f, -5.0f),
        Vec3(0.0f, 0.5f, -5.0f),
    };

    RayIntersectTriangleResult result = RayIntersectTriangleMT(
        rayOrigin, rayDirection, vertices[0], vertices[1], vertices[2]);

    TEST_ASSERT_FLOAT_WITHIN(EPSILON, -1.0f, result.t);
}


TEST_GROUP_RUNNER(RayIntersection)
{
    RUN_TEST_CASE(RayIntersection, TestRayIntersectTriangleHit);
    RUN_TEST_CASE(RayIntersection, TestRayIntersectTriangleMiss);
    RUN_TEST_CASE(RayIntersection, TestRayIntersectTriangleHitUV);
    RUN_TEST_CASE(RayIntersection, TestRayIntersectTriangleMTBarycentricCoordsIssue);
}
