#include "unity.h"

#include "config.h"
#include "platform.h"
#include "intrinsics.h"
#include "profiler.h"
#include "work_queue.h"
#include "math_lib.h"
#include "ray_intersection.h"
#include "mesh.h"
#include "debug.h"
#include "asset_loader/asset_loader.h"
#include "image.h"
#include "tile.h"
#include "cmdline.h"

#include "custom_assertions.h"

#include "debug.cpp"
#include "cmdline.cpp"
#include "mesh_generation.cpp"
#include "ray_intersection.cpp"

#define MEMORY_ARENA_SIZE Megabytes(1)

MemoryArena memoryArena;

void setUp(void)
{
    // set stuff up here
    ClearToZero(memoryArena.base, (u32)memoryArena.size);
    ResetMemoryArena(&memoryArena);
}

void tearDown(void)
{
    // clean stuff up here
}

#if 0
void TestCreateCubeMap()
{
    // Create Equirectangular image of direction vectors
    u32 width = 8;
    u32 height = 4;
    vec3 pixels[8 * 4] = {};
    for (u32 y = 0; y < height; ++y)
    {
        for (u32 x = 0; x < width; ++x)
        {
            // Convert pixel coordinates to UV coordinates
            vec2 uv = Vec2((f32)x / (f32)width, (f32)y / (f32)height);

            // Convert UV coordinates into sphere coordinates
            vec2 sphereCoords = MapEquirectangularToSphereCoordinates(uv);

            // Convert sphere coordinates to cartesian coordinates
            vec3 direction = MapSphericalToCartesianCoordinates(sphereCoords);

            u32 index = y * width + x;
            pixels[index] = direction;

            //printf("[%0.2g, %0.2g, %0.2g] ", direction.x, direction.y, direction.z);
        }

        //printf("\n");
    }

    HdrImage equirectangularImage = {};
    equirectangularImage.width = width;
    equirectangularImage.height = height;
    equirectangularImage.pixels = (f32 *)pixels;

    MemoryArena arena = {};
    InitializeMemoryArena(&arena, malloc(4096), 4096);

    // Create cube map by sampling equirectangular image
    // Create cube map face +Z
    HdrImage image = CreateCubeMap(equirectangularImage, &arena, 3);

    for (u32 y = 0; y < 3; ++y)
    {
        for (u32 x = 0; x < 3; ++x)
        {
            u32 index = (y * 3 + x) * 4;
            vec4 color = *(vec4 *)(image.pixels + index);

            // TOMORROW: Our terrible sampling is struggling at such a low res
            printf("[%0.2g, %0.2g, %0.2g, %0.2g] ", color.x, color.y,
                color.z, color.w);
        }

        printf("\n");
    }

    vec3 expected = Vec3(0, 0, 1);
    vec4 direction = SampleImage(image, Vec2(0.5, 0.5));
    TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, expected.x, direction.x, "X axis");
    TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, expected.y, direction.y, "Y axis");
    TEST_ASSERT_FLOAT_WITHIN_MESSAGE(EPSILON, expected.z, direction.z, "Z axis");

    free(arena.base);
}
#endif

int main()
{
    InitializeMemoryArena(
        &memoryArena, calloc(1, MEMORY_ARENA_SIZE), MEMORY_ARENA_SIZE);

    UNITY_BEGIN();

    free(memoryArena.base);

    return UNITY_END();
}
