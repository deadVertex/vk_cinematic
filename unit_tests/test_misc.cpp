#include "unity.h"
#include "unity_fixture.h"

#include "platform.h"
#include "math_lib.h"
#include "aabb.h"
#include "mesh.h"
#include "data_model.h"
#include "custom_assertions.h"

#include "mesh_generation.cpp"

TEST_GROUP(Misc);

global MemoryArena memoryArena;

TEST_SETUP(Misc)
{
    u32 size = Kilobytes(128);
    InitializeMemoryArena(&memoryArena, malloc(size), size);
}

TEST_TEAR_DOWN(Misc)
{
    free(memoryArena.base);
}

TEST(Misc, TestMemoryArenaFreeBug)
{
    // Given a memory arena with 2 allocations
    MemoryArena testArena = SubAllocateArena(&memoryArena, 64);
    void *p = AllocateBytes(&testArena, 32);
    void *q = AllocateBytes(&testArena, 16);

    // When we free the second allocation
    FreeFromMemoryArena(&testArena, q);

    // Then the first allocation is still the same size
    TEST_ASSERT_EQUAL_UINT32(32, testArena.size);
}

TEST(Misc, IcosahedronTextureCoordBug)
{
    MeshData meshData = CreateIcosahedronMesh(2, &memoryArena);
    for (u32 i = 0; i < meshData.vertexCount; i++)
    {
        VertexPNT vertex = meshData.vertices[i];

        char msg[128];
        snprintf(msg, sizeof(msg),
            "Expected (%g, %g) to be within 0 to 1 range",
            vertex.textureCoord.x, vertex.textureCoord.y);
        TEST_ASSERT_TRUE_MESSAGE(
            vertex.textureCoord.x >= 0.0f && vertex.textureCoord.x <= 1.0f &&
            vertex.textureCoord.y >= 0.0f && vertex.textureCoord.y <= 1.0f, msg);
    }
}

TEST(Misc, BunnyShadow)
{
    DataModelDirectionalLight light = {};
    light.direction = Normalize(Vec3(1, -3, 1)); // Copied from default scene
    // Map from world space into light space
    mat4 changeOfBasis = ChangeOfBasisZ(light.direction);
    // Inverse to transform from world space to light space
    mat4 viewMatrix = Translate(Vec3(0, 0, -5)) * Transpose(changeOfBasis);
    f32 epsilon = 0.001f;
    AssertWithinVec4(epsilon, Vec4(-0.70711, -0.6396, 0.30151, 0.0), viewMatrix.columns[0]);
    AssertWithinVec4(epsilon, Vec4(0.0, -0.4264, -0.90453, 0.0), viewMatrix.columns[1]);
    AssertWithinVec4(epsilon, Vec4(0.70711, -0.6396, 0.30151, 0.0), viewMatrix.columns[2]);
    AssertWithinVec4(epsilon, Vec4(0.0, 0.0, -5, 1), viewMatrix.columns[3]);

    f32 k = 1.0f;
    f32 zNear = 0.1f;
    f32 zFar = 20.0f;
    mat4 projectionMatrix = OrthographicVulkan(-k, k, -k, k, zNear, zFar);
    AssertWithinVec4(epsilon, Vec4(1.0, 0.0, 0.0, 0.0), projectionMatrix.columns[0]);
    AssertWithinVec4(epsilon, Vec4(0.0, -1.0, 0.0, 0.0), projectionMatrix.columns[1]);
    AssertWithinVec4(epsilon, Vec4(0.0, 0.0, -0.05025, 0.0), projectionMatrix.columns[2]);
    AssertWithinVec4(epsilon, Vec4(0.0, 0.0, -0.00503, 1.0), projectionMatrix.columns[3]);

    VertexPNT vertex = {};
    vertex.position = Vec3(-0.08656, 0.14249, 0.00843);

    mat4 modelMatrix = Translate(Vec3(0, -0.02, 0));
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, -0.02f, modelMatrix.columns[3].y);

    vec4 glPosition = projectionMatrix * viewMatrix * modelMatrix *
                      Vec4(vertex.position, 1.0f);
    AssertWithinVec4(epsilon, Vec4(0.06717, 0.00226, 0.25298, 1.0), glPosition);
}

TEST(Misc, ShadowStuff)
{
    DataModelDirectionalLight light = {};
    light.direction = Normalize(Vec3(1, -3, 1)); // Copied from default scene

    // Map from world space into light space
    // NOTE: We negate light direction here because -Z is actually the
    // forward direction for the camera
    mat4 changeOfBasis = ChangeOfBasisZ(-light.direction);

    // Inverse to transform from world space to light space
    mat4 viewMatrix = Translate(Vec3(0, 0, -1)) * Transpose(changeOfBasis);

    f32 k = 1.0f;
    f32 zNear = 0.1f;
    f32 zFar = 50.0f;
    mat4 projectionMatrix = OrthographicVulkan(-k, k, -k, k, zNear, zFar);

    vec3 lightSpaceForward =  changeOfBasis.columns[2].xyz;
    vec3 p0 = light.direction * 0.5f;
    vec3 p1 = light.direction * 5.0f;

    vec3 lightSpaceP0 = TransformPoint(p0, viewMatrix);
    vec3 lightSpaceP1 = TransformPoint(p1, viewMatrix);

    // This is correctly projecting our points onto the z acess in light space
    vec3 lightSpaceOrigin = Vec3(0); // ?
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, -1.5f, lightSpaceP0.z);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, -6.0f, lightSpaceP1.z);

    // Should be center of screen
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 0.0f, lightSpaceP0.x);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 0.0f, lightSpaceP0.y);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 0.0f, lightSpaceP1.x);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 0.0f, lightSpaceP1.y);

    // Check projection
    vec4 ndc0 = projectionMatrix * viewMatrix * Vec4(p0, 1);
    vec4 ndc1 = projectionMatrix * viewMatrix * Vec4(p1, 1);

    // Between -1 and 1 from memory...
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 0.0f, ndc0.x);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 0.0f, ndc0.y);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 0.0f, ndc1.x);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 0.0f, ndc1.y);

    // No perspective to divide
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 1.0f, ndc0.w);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, 1.0f, ndc1.w);

    // Projection matrix maps depth from 0 to 1 (rather than -1 to 1 like OpenGL)
    f32 expected0 = (1.0f / (zNear - zFar)) * -1.5f + zNear / (zNear - zFar);
    f32 expected1 = (1.0f / (zNear - zFar)) * -6.0f + zNear / (zNear - zFar);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, expected0, ndc0.z);
    TEST_ASSERT_FLOAT_WITHIN(EPSILON, expected1, ndc1.z);
}

TEST(Misc, ComputeOrthographicProjectionForScene)
{
    DataModelDirectionalLight light = {};
    light.direction = Normalize(Vec3(1, -3, 1)); // Copied from default scene
                                                 //
    // Map from world space into light space
    // NOTE: We negate light direction here because -Z is actually the
    // forward direction for the camera
    mat4 changeOfBasis = ChangeOfBasisZ(-light.direction);

    // Inverse to transform from world space to light space
    mat4 viewMatrix = Translate(Vec3(0, 0, -1)) * Transpose(changeOfBasis);

    Aabb sceneBoundingBox = {Vec3(-5), Vec3(5)};

    mat4 projectionMatrix = ComputeOrthographicProjectionForBoundingBox(
        viewMatrix, sceneBoundingBox);

    // Check scene bounding points are projected within normalized NDC
    vec4 ndc0 = projectionMatrix * viewMatrix * Vec4(sceneBoundingBox.min, 1);
    vec4 ndc1 = projectionMatrix * viewMatrix * Vec4(sceneBoundingBox.max, 1);
    TEST_ASSERT_TRUE(ndc0.x >= -1.0f);
    TEST_ASSERT_TRUE(ndc0.y >= -1.0f);
    TEST_ASSERT_TRUE(ndc1.x <= 1.0f);
    TEST_ASSERT_TRUE(ndc1.y <= 1.0f);

    TEST_ASSERT_TRUE(ndc0.z >= 0.0f && ndc0.z <= 1.0f);
    TEST_ASSERT_TRUE(ndc1.z >= 0.0f && ndc1.z <= 1.0f);
}

TEST_GROUP_RUNNER(Misc)
{
    RUN_TEST_CASE(Misc, TestMemoryArenaFreeBug);
    RUN_TEST_CASE(Misc, IcosahedronTextureCoordBug);
    RUN_TEST_CASE(Misc, BunnyShadow);
    RUN_TEST_CASE(Misc, ShadowStuff);
    RUN_TEST_CASE(Misc, ComputeOrthographicProjectionForScene);
}
