#include "unity.h"
#include "unity_fixture.h"

#include "platform.h"
#include "math_lib.h"
#include "asset_loader/asset_loader.h" // TODO: Image shouldn't be defined here!
#include "image.h"

TEST_GROUP(Image);

global MemoryArena memoryArena;

TEST_SETUP(Image)
{
    u32 size = Kilobytes(1);
    InitializeMemoryArena(&memoryArena, malloc(size), size);
}

TEST_TEAR_DOWN(Image)
{
    free(memoryArena.base);
}

TEST(Image, TestNearestSampling)
{
    // Given an image   | (0, 0, 0, 0) | (1, 1, 1, 1) |
    HdrImage image = AllocateImage(2, 1, &memoryArena);
    SetPixel(&image, 0, 0, Vec4(0));
    SetPixel(&image, 1, 0, Vec4(1));

    // When we sample the center of the image
    vec4 sample = SampleImageNearest(image, Vec2(0.5, 0.5));

    // Then we get the color of the nearest pixel
    TEST_ASSERT_EQUAL_FLOAT(1, sample.r);
}

TEST(Image, TestBilinearSampling)
{
    // Given an image with a checkerboard pattern
    // | (0, 0, 0, 0) | (1, 1, 1, 1) |
    // | (1, 1, 1, 1) | (0, 0, 0, 0) |
    HdrImage image = AllocateImage(2, 2, &memoryArena);
    SetPixel(&image, 0, 0, Vec4(0));
    SetPixel(&image, 1, 0, Vec4(1));
    SetPixel(&image, 0, 1, Vec4(1));
    SetPixel(&image, 1, 1, Vec4(0));

    // When we sample the center of the image
    vec4 sample = SampleImageBilinear(image, Vec2(0.5, 0.5));

    // Then we get the color of the 4 nearest pixels blended together
    TEST_ASSERT_EQUAL_FLOAT(0.5, sample.r);
}

TEST(Image, TestBilinearSamplingClampEdge)
{
    // Given an image with a checkerboard pattern
    // | (0, 0, 0, 0) | (1, 1, 1, 1) |
    // | (1, 1, 1, 1) | (0, 0, 0, 0) |
    HdrImage image = AllocateImage(2, 2, &memoryArena);
    SetPixel(&image, 0, 0, Vec4(0));
    SetPixel(&image, 1, 0, Vec4(1));
    SetPixel(&image, 0, 1, Vec4(1));
    SetPixel(&image, 1, 1, Vec4(0));

    // When we sample the edge of the image
    vec4 sampleMax = SampleImageBilinear(image, Vec2(1.0, 0.5));
    vec4 sampleMin = SampleImageBilinear(image, Vec2(0.0, 0.5));

    // Then we get the color of the edge pixels blended
    TEST_ASSERT_EQUAL_FLOAT(0.5, sampleMax.r);
    TEST_ASSERT_EQUAL_FLOAT(0.5, sampleMin.r);
}

TEST_GROUP_RUNNER(Image)
{
    RUN_TEST_CASE(Image, TestNearestSampling);
    RUN_TEST_CASE(Image, TestBilinearSampling);
    RUN_TEST_CASE(Image, TestBilinearSamplingClampEdge);
}
