#include "unity.h"
#include "unity_fixture.h"

#include "platform.h"
#include "math_lib.h"
#include "asset_loader/asset_loader.h" // TODO: Image shouldn't be defined here!
#include "image.h"
#include "custom_assertions.h"
#include "cubemap.h"

#include "cubemap.cpp"
#include "image.cpp"

TEST_GROUP(CubeMap);

global MemoryArena memoryArena;

TEST_SETUP(CubeMap)
{
    u32 size = Kilobytes(1);
    InitializeMemoryArena(&memoryArena, malloc(size), size);
}

TEST_TEAR_DOWN(CubeMap)
{
    free(memoryArena.base);
}

TEST(CubeMap, Irradiance)
{
    // Given a pure white HDRi
    HdrImage equirectangularImage =
        CreateSingleColorImage(Vec4(1), 1, 1, &memoryArena);

    // When we create an irradiance cube map
    HdrCubeMap irradianceMap =
        CreateIrradianceCubeMap(equirectangularImage, &memoryArena, 1, 1, 0.025f);

    // Then the irradiance value returned in every direction is 1.0
    //for (u32 i = 0; i < 6; i++)
    {
        u32 i = 0;
        vec4 sample = SampleImageNearest(irradianceMap.images[i], Vec2(0.5, 0.5));
        AssertWithinVec4(0.01f, Vec4(1), sample);
    }
}

TEST_GROUP_RUNNER(CubeMap)
{
    RUN_TEST_CASE(CubeMap, Irradiance);
}
