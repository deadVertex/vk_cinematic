#include "unity_fixture.h"

static void RunAllTests(void)
{
    RUN_TEST_GROUP(SceneApi);
    RUN_TEST_GROUP(AssetSystem);
    RUN_TEST_GROUP(RayIntersection);
    RUN_TEST_GROUP(Image);
    RUN_TEST_GROUP(CubeMap);
    RUN_TEST_GROUP(Mapping);
    RUN_TEST_GROUP(WorkQueue);
    RUN_TEST_GROUP(BVH);
    RUN_TEST_GROUP(SIMDPathTracer);
    RUN_TEST_GROUP(Misc);
}

int main(int argc, const char *argv[])
{
    return UnityMain(argc, argv, RunAllTests);
}
