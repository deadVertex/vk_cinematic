internal b32 sp_AddDirectionalLight(
    sp_LightSystem *lightSystem, vec3 radiance, vec3 direction, u32 id)
{
    Assert(Abs(LengthSq(direction) - 1.0f) <= EPSILON);

    b32 result = false;
    if (lightSystem->directionalLightCount < SP_MAX_DIRECTIONAL_LIGHTS)
    {
        sp_DirectionalLight *light = lightSystem->directionalLights +
                                     lightSystem->directionalLightCount++;

        light->radiance = radiance;
        light->direction = direction;
        light->id = id;

        result = true;
    }

    return result;
}

internal b32 sp_GetRandomDirectionalLight(sp_LightSystem *lightSystem,
    RandomNumberGenerator *rng, sp_DirectionalLight *light)
{
    b32 result = false;
    if (lightSystem->directionalLightCount > 0)
    {
        u32 randomIndex = XorShift32(rng) % lightSystem->directionalLightCount;
        *light = lightSystem->directionalLights[randomIndex];
        result = true;
    }

    return result;
}

internal b32 sp_GetDirectionalLightById(
    sp_LightSystem *lightSystem, u32 id, sp_DirectionalLight *result)
{
    b32 found = false;

    for (u32 i = 0; i < lightSystem->directionalLightCount; i++)
    {
        sp_DirectionalLight *light = lightSystem->directionalLights + i;
        if (light->id == id)
        {
            *result = *light;
            found = true;
            break;
        }
    }

    return found;
}

internal b32 sp_AddPointLight(
    sp_LightSystem *lightSystem, vec3 radiance, vec3 position, u32 id)
{
    b32 result = false;
    if (lightSystem->pointLightCount < SP_MAX_POINT_LIGHTS)
    {
        sp_PointLight *light =
            lightSystem->pointLights + lightSystem->pointLightCount++;

        light->radiance = radiance;
        light->position = position;
        light->id = id;

        result = true;
    }

    return result;
}

internal b32 sp_GetRandomPointLight(sp_LightSystem *lightSystem,
    RandomNumberGenerator *rng, sp_PointLight *light)
{
    b32 result = false;
    if (lightSystem->pointLightCount > 0)
    {
        u32 randomIndex = XorShift32(rng) % lightSystem->pointLightCount;
        *light = lightSystem->pointLights[randomIndex];
        result = true;
    }

    return result;
}

internal b32 sp_GetPointLightById(
    sp_LightSystem *lightSystem, u32 id, sp_PointLight *result)
{
    b32 found = false;

    for (u32 i = 0; i < lightSystem->pointLightCount; i++)
    {
        sp_PointLight *light = lightSystem->pointLights + i;
        if (light->id == id)
        {
            *result = *light;
            found = true;
            break;
        }
    }

    return found;
}
