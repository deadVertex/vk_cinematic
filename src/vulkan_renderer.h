#pragma once

#define VERTEX_BUFFER_SIZE Megabytes(512)
#define INDEX_BUFFER_SIZE Megabytes(2)
#define UNIFORM_BUFFER_SIZE Kilobytes(4)
#define IMAGE_BUFFER_SIZE Megabytes(64)
#define DEBUG_VERTEX_BUFFER_SIZE Megabytes(256)
#define MODEL_MATRICES_BUFFER_SIZE Megabytes(4)
#define LIGHT_BUFFER_SIZE Kilobytes(1) // TODO: Combine this with material buffer?
#define COMPUTE_UNIFORM_BUFFER_SIZE Kilobytes(1)
#define COMPUTE_MESH_BUFFER_SIZE Kilobytes(1)
#define COMPUTE_TILE_QUEUE_BUFFER_SIZE Kilobytes(4)
#define COMPUTE_MAX_TILES_PER_FRAME 64
#define COMPUTE_SCENE_BUFFER_SIZE Kilobytes(4) // FIXME: Pretty sure this buffer is too small
#define COMPUTE_INV_MODEL_MATRICES_BUFFER_SIZE Kilobytes(256)

#define TEXTURE_UPLOAD_BUFFER_SIZE Megabytes(128)

#define SHADOW_PASS_LAYER_COUNT 4

//#define SHADER_PATH "src/shaders"
#define SHADER_PATH "shaders"

const u32 Image_CpuRayTracer = HashStringU32("textures/cpu_ray_tracer");

enum
{
    Output_None = 0,
    Output_ShowDebugDrawing = 1,
};

enum
{
    CameraIndex_Default = 0,
    CameraIndex_Skybox = 1,
    CameraIndex_ShadowPass = 2,
};

struct Mesh
{
    u32 indexCount;
    u32 indexDataOffset;
    u32 vertexDataOffset;
};

struct UniformBufferObject
{
    mat4 viewMatrices[16];
    mat4 projectionMatrices[16];
    vec3 cameraPosition;
    u32 showComparision;
};

struct VulkanQueueFamilyIndices
{
    u32 graphicsQueue;
    u32 presentQueue;
};

struct VulkanImage
{
    VkImage handle;
    VkDeviceMemory memory;
    VkImageView view;
};

struct VulkanBuffer
{
    VkBuffer handle;
    VkDeviceMemory memory;
    void *data;
};

struct VulkanSwapchain
{
    VkSwapchainKHR handle;
    VkImageView imageViews[2]; // TODO: Store as array of VulkanImages
    VkImage images[2];
    VkFramebuffer framebuffers[2];
    u32 imageCount;
    u32 width;
    u32 height;

    VulkanImage depthImage;
};

enum
{
    Primitive_Triangle,
    Primitive_Line,
};

enum
{
    PolygonMode_Fill,
    PolygonMode_Line,
};

enum
{
    CullMode_None,
    CullMode_Front,
    CullMode_Back,
};

struct VulkanPipelineDefinition
{
    u32 vertexStride;
    u32 primitive;
    u32 polygonMode;
    u32 cullMode;
    b32 depthTestEnabled;
    b32 depthWriteEnabled;
    b32 alphaBlendingEnabled;
};

struct VulkanHdrSwapchainFramebuffer
{
    VulkanImage depth;
    VulkanImage color;
    VkFramebuffer handle;
};

struct VulkanHdrSwapchain
{
    VulkanHdrSwapchainFramebuffer framebuffers[2];
    u32 imageCount;
};

struct VulkanShadowBuffer
{
    VulkanImage depth;
    VkFramebuffer handle;
};

struct VulkanShadowPass
{
    VulkanShadowBuffer buffers[2];
    u32 bufferCount;
    VkRenderPass renderPass;
    u32 width;
    u32 height;
};

struct MeshPushConstants
{
    u32 modelMatrixIndex;
    u32 vertexDataOffset;
    u32 cameraIndex;
    i32 layer;
};

struct VulkanMaterial
{
    VkPipeline pipeline;
    VkDescriptorSet descriptorSets[2]; // TODO: Not sure if we actually need 2

    VkPipeline shadowPipeline;
    VkDescriptorSet shadowDescriptorSets[2];
};

struct VulkanComputeShaderStuff
{
    VkShaderModule shader;
    VkDescriptorSetLayout descriptorSetLayout;
    VkDescriptorSet descriptorSet;
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;
};

struct VulkanRenderer
{
    VkInstance instance;
    VkDebugUtilsMessengerEXT messenger;
    VkPhysicalDevice physicalDevice;
    VkSurfaceKHR surface;

    VulkanQueueFamilyIndices queueFamilyIndices;
    VkDevice device;

    VkQueue graphicsQueue;
    VkQueue presentQueue;

    VkRenderPass renderPass;
    VulkanSwapchain swapchain;

    VkRenderPass hdrRenderPass;
    VulkanHdrSwapchain hdrSwapchain;

    VulkanShadowPass shadowPass;

    VkSemaphore acquireSemaphore;
    VkSemaphore releaseSemaphore;

    VkCommandPool commandPool;
    VkDescriptorPool descriptorPool;

    VkCommandBuffer commandBuffer;
    VulkanBuffer vertexDataUploadBuffer;
    VulkanBuffer vertexDataBuffer;
    VulkanBuffer uniformBuffer;
    VulkanBuffer indexUploadBuffer;
    VulkanBuffer indexBuffer;
    VulkanBuffer imageUploadBuffer;
    VulkanBuffer modelMatricesBuffer;
    VulkanBuffer lightBuffer;

    // FIXME: Should be combined with imageUploadBuffer but imageUploadBuffer
    // is currently being used in a strange way for uploading the CPU ray
    // tracing result.
    VulkanBuffer textureUploadBuffer;

    VulkanBuffer debugVertexDataBuffer;

    VkSampler defaultSampler;

    // Triangle stuff
    VkShaderModule testVertexShader;
    VkShaderModule testFragmentShader;

    VkShaderModule shadowFragmentShader;

    VkDescriptorSetLayout descriptorSetLayout;
    VkPipelineLayout pipelineLayout;

    VkDescriptorSetLayout shadowDescriptorSetLayout;
    VkPipelineLayout shadowPipelineLayout;

    VkPipeline pipeline;
    VkPipelineCache pipelineCache;
    VkDescriptorSet descriptorSets[2];

    // Post processing stuff
    VkShaderModule fullscreenQuadVertexShader;
    VkShaderModule postProcessingFragmentShader;
    VkPipeline postProcessPipeline;
    VkDescriptorSet postProcessDescriptorSets[2];

    // Debug draw buffer
    VkShaderModule debugDrawVertexShader;
    VkShaderModule debugDrawFragmentShader;
    VkPipeline debugDrawPipeline;
    VkPipelineLayout debugDrawPipelineLayout;
    VkDescriptorSetLayout debugDrawDescriptorSetLayout;
    VkDescriptorSet debugDrawDescriptorSets[2];

    // Skybox stuff
    VkShaderModule skyboxFragmentShader;
    VkPipeline skyboxPipeline;

    u32 vertexDataUploadBufferSize;
    u32 indexUploadBufferSize;
    u32 debugDrawVertexCount;

    VulkanImage cpuPathTracerImage;

    VulkanComputeShaderStuff convertEquirectangular;
    VulkanComputeShaderStuff generateIrradiance;

    // TODO: Extract this out into a separate object
#define RS_MAX_MESHES 32
    u32 meshKeys[RS_MAX_MESHES];
    Mesh meshValues[RS_MAX_MESHES];
    Dictionary meshes;

#define RS_MAX_TEXTURES 64
    u32 textureKeys[RS_MAX_TEXTURES];
    VulkanImage textureValues[RS_MAX_TEXTURES];
    Dictionary textures;

#define RS_MAX_MATERIALS 32
    u32 materialKeys[RS_MAX_MATERIALS];
    VulkanMaterial materialValues[RS_MAX_MATERIALS];
    Dictionary materials;
};

const u32 Mesh_Cube = HashStringU32("meshes/cube");
