internal HdrImage CreateCheckerBoardImage(MemoryArena *tempArena)
{
    u32 width = 256;
    u32 height = 256;

    HdrImage result = AllocateImage(width, height, tempArena);

    u32 gridSize = 16;
    for (u32 y = 0; y < height; ++y)
    {
        for (u32 x = 0; x < width; ++x)
        {
            vec4 color = Vec4(0.18, 0.18, 0.18, 1);
            if (((x / gridSize) + (y / gridSize)) % 2)
            {
                color = Vec4(0.04, 0.04, 0.04, 1);
            }

            SetPixel(&result, x, y, color);
        }
    }

    return result;
}

internal HdrImage CreateSingleColorImage(
    vec4 color, u32 width, u32 height, MemoryArena *arena)
{
    HdrImage result = AllocateImage(width, height, arena);
    for (u32 y = 0; y < height; y++)
    {
        for (u32 x = 0; x < width; x++)
        {
            SetPixel(&result, x, y, color);
        }
    }

    return result;
}
