#pragma once

enum
{
    AssetSystemTextureSource_Disk,
    AssetSystemTextureSource_SingleColor,
    AssetSystemTextureSource_Checkerboard,
};

struct AssetSystemTexture
{
    u32 source;
    const char *path;
    vec4 singleColor;
    HdrImage data;
};

struct AssetSystemMesh
{
    u32 generator;
    const char *path;
    MeshData data;
    Aabb aabb;
};

struct AssetSystem
{
    MemoryArena imageDataArena;
    MemoryArena meshDataArena;
    Dictionary textures;
    Dictionary meshes;
};

typedef b32 (*AssetSystemLoadTextureFunction)(HdrImage*, MemoryArena*, const char *, void*);
typedef b32 (*AssetSystemLoadMeshFunction)(MeshData*, Aabb*, MemoryArena*, const char *, void*);

struct AssetSystemLoader
{
    AssetSystemLoadTextureFunction readTextureFromDisk;
    AssetSystemLoadMeshFunction readMeshFromDisk;
};

typedef b32 (*AssetSystemGenerateSingleColorTextureFunction)(HdrImage*, MemoryArena*, vec4, void*);
typedef b32 (*AssetSystemGenerateCheckerboardTextureFunction)(HdrImage*, MemoryArena*, void*);
typedef b32 (*AssetSystemGenerateMeshFunction)(MeshData*, Aabb*, u32, MemoryArena*, void*);

struct AssetSystemGenerator
{
    AssetSystemGenerateSingleColorTextureFunction singleColor;
    AssetSystemGenerateCheckerboardTextureFunction checkerboard;
    AssetSystemGenerateMeshFunction mesh;
};

struct AssetSystemConfig
{
    u32 maxTextures;
    u32 maxMeshes;
    u32 imageDataArenaSize;
    u32 meshDataArenaSize;
};
