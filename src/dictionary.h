#pragma once

struct Dictionary
{
    u32 *keys;
    void *values;
    u32 count;
    u32 capacity;
    u32 elementSize;
};

inline Dictionary Dict_Create(
    u32 capacity, u32 elementSize, u32 *keys, void *values)
{
    Dictionary result = {};
    result.keys = keys;
    result.values = values;
    result.capacity = capacity;
    result.elementSize = elementSize;

    return result;
}

inline Dictionary Dict_CreateFromArena_(
    MemoryArena *arena, u32 capacity, u32 elementSize)
{
    u32 *keys = AllocateArray(arena, u32, capacity);
    void *values = AllocateBytes(arena, elementSize * capacity);

    Dictionary result = Dict_Create(capacity, elementSize, keys, values);
    return result;
}

#define Dict_CreateFromArena(ARENA, TYPE, CAPACITY) \
    Dict_CreateFromArena_(ARENA, CAPACITY, sizeof(TYPE))

#define Dict_CreateFromArrays(KEYS, VALUES) \
    Dict_Create(ArrayCount(KEYS), sizeof(VALUES[0]), KEYS, VALUES)

inline void *Dict_FindItem_(Dictionary *dict, u32 key, u32 elementSize)
{
    Assert(elementSize == dict->elementSize);

    void *result = NULL;
    for (u32 i = 0; i < dict->count; ++i)
    {
        if (dict->keys[i] == key)
        {
            result = (u8 *)dict->values + i * elementSize;
            break;
        }
    }

    return result;
}

#define Dict_FindItem(DICT, TYPE, KEY) \
    (TYPE*)Dict_FindItem_(DICT, KEY, sizeof(TYPE))

inline void *Dict_AddItem_(Dictionary *dict, u32 key, u32 elementSize)
{
    Assert(elementSize == dict->elementSize);
    Assert(dict->count < dict->capacity); // TODO: Handle case

    u32 index = dict->count++;
    dict->keys[index] = key;
    void *result = (u8 *)dict->values + index * elementSize;
    return result;
}

#define Dict_AddItem(DICT, TYPE, KEY) \
    (TYPE*)Dict_AddItem_(DICT, KEY, sizeof(TYPE))

inline b32 Dict_RemoveItem(Dictionary *dict, u32 key)
{
    b32 found = false;
    for (u32 i = 0; i < dict->count; ++i)
    {
        if (dict->keys[i] == key)
        {
            // Swap and decrement size
            u32 last = --dict->count;
            dict->keys[i] = dict->keys[last];
            void *src = (u8 *)dict->values + last * dict->elementSize;
            void *dst = (u8 *)dict->values + i * dict->elementSize;
            memcpy(dst, src, dict->elementSize);
            found = true;
            break;
        }
    }

    return found;
}

inline void Dict_Clear(Dictionary *dict)
{
    dict->count = 0;
}
