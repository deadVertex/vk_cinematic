#version 450
#extension GL_GOOGLE_include_directive : require

#include "compute_shader_common.glsl"

layout(binding = 0, rgba32f) uniform writeonly imageCube outputImage;
layout(binding = 1) uniform sampler2D inputImage;

void main()
{
    uint width = gl_NumWorkGroups.x;
    uint height = gl_NumWorkGroups.y;

    uint x = gl_GlobalInvocationID.x;
    uint y = gl_GlobalInvocationID.y;
    uint layerIndex = gl_GlobalInvocationID.z;

    // Map layer index to basis vectors for cube map face
    BasisVectors basis = MapCubeMapLayerIndexToBasisVectors(layerIndex);

    // Convert pixel to cartesian direction vector
    f32 fx = float(x) / float(width);
    f32 fy = float(y) / float(height);

    // Flip Y axis
    fy = 1.0f - fy;

    // Map to -1 to 1
    fx = fx * 2.0f - 1.0f;
    fy = fy * 2.0f - 1.0f;

    vec3 dir = basis.forward + basis.right * fx + basis.up * fy;
    dir = normalize(dir);

    // Sample equirectangular texture using direction vector
    vec2 sphereCoords = ToSphericalCoordinates(dir);
    vec2 uv = MapToEquirectangular(sphereCoords);
    uv.y = 1.0f - uv.y; // Flip Y axis as usual
    vec3 color = texture(inputImage, uv).rgb;

    // Store sample for pixel
    imageStore(outputImage, ivec3(x, y, layerIndex), vec4(color, 1));
}
