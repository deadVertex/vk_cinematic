#version 450

#define RS_MAX_DIRECTIONAL_LIGHTS 4

layout(location = 0) in vec3 fragNormal;
layout(location = 2) in vec3 fragLocalPosition;
layout(location = 3) in vec2 fragTexCoord;
layout(location = 4) in vec3 fragWorldPosition;
layout(location = 5) in vec3 fragCameraPosition;
layout(location = 6) in vec4 fragLightPosition[RS_MAX_DIRECTIONAL_LIGHTS];

void main()
{
}
