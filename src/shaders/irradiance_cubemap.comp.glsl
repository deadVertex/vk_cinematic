#version 450
#extension GL_GOOGLE_include_directive : require

#include "compute_shader_common.glsl"

#define RADIANCE_CLAMP 10.0

layout(binding = 0, rgba32f) uniform writeonly imageCube outputImage;
layout(binding = 1) uniform samplerCube inputImage;

void main()
{
    // TODO: Pass this in as a parameter
    uint width = 32;
    uint height = 32;

    uint x = gl_GlobalInvocationID.x;
    uint y = gl_GlobalInvocationID.y;
    uint layerIndex = gl_GlobalInvocationID.z;

    rng_state = x * 0xC35A4531 ^ y * 0xF125EA62 ^ layerIndex ^ 0xDF70F989;

    BasisVectors basis = MapCubeMapLayerIndexToBasisVectors(layerIndex);

    // Convert pixel to cartesian direction vector
    f32 fx = float(x) / float(width);
    f32 fy = float(y) / float(height);

    // Flip Y axis
    fy = 1.0f - fy;

    // Map to -1 to 1
    fx = fx * 2.0f - 1.0f;
    fy = fy * 2.0f - 1.0f;

    vec3 dir = basis.forward + basis.right * fx + basis.up * fy;
    dir = normalize(dir);

    vec3 irradiance = vec3(0, 0, 0);

    // From https://learnopengl.com/PBR/IBL/Diffuse-irradiance
    vec3 normal = dir;
    vec3 tangent = normalize(cross(basis.up, normal));
    vec3 bitangent = normalize(cross(normal, tangent));

    f32 sampleDelta = 0.01f; // TODO: Parameterize
    u32 sampleCount = 0;
    for (f32 phi = 0.0f; phi < 2.0f * PI; phi += sampleDelta)
    {
        for (f32 theta = 0.0f; theta < 0.5f * PI; theta += sampleDelta)
        {
            // Get sample vector in tangent space
            vec3 tangentDir = MapSphericalToCartesianCoordinates(
                    vec2(phi, theta));

            // Map vector from tangent space to world space so we
            // can sample the environment map
            vec3 worldDir = normal * tangentDir.y +
                tangent * tangentDir.x +
                bitangent * tangentDir.z;

            vec3 radiance = texture(inputImage, worldDir).xyz;
#ifdef RADIANCE_CLAMP
            radiance = clamp(radiance, vec3(0), vec3(RADIANCE_CLAMP));
#endif

            // Add irradiance contribution for integral
            irradiance += radiance * cos(theta) * sin(theta);
            sampleCount++;
        }
    }

    irradiance = PI * irradiance * (1.0f / float(sampleCount));
    imageStore(outputImage, ivec3(x, y, layerIndex), vec4(irradiance, 1));
}
