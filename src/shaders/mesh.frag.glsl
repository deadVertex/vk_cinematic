#version 450

// FIXME: Copied from src/shaders/skybox.frag.glsl
#define PI 3.14159265359f

struct SphereLightData
{
    float px, py, pz;
    float radianceR, radianceG, radianceB;
    float radius;
};

// TODO: Just replace this with a vec3 surely?
struct AmbientLightData
{
    float radianceR, radianceG, radianceB;
};

struct DiskLightData
{
    float nx, ny, nz;
    float px, py, pz;
    float rr, rg, rb;
    float radius;
};

struct DirectionalLightData
{
    float radianceR, radianceG, radianceB;
    float directionX, directionY, directionZ;
};

// NOTE: Needs to be kept in sync with src/rasterizer_data_model.h
#define RS_MAX_SPHERE_LIGHTS 20
#define RS_MAX_DISK_LIGHTS 4
#define RS_MAX_DIRECTIONAL_LIGHTS 4
layout(binding = 10) readonly buffer LightData
{
    uint sphereLightCount;
    SphereLightData sphereLights[RS_MAX_SPHERE_LIGHTS];

    AmbientLightData ambientLight;

    uint diskLightCount;
    DiskLightData diskLights[RS_MAX_DISK_LIGHTS];

    uint directionalLightCount;
    DirectionalLightData directionalLights[RS_MAX_DIRECTIONAL_LIGHTS];
} lightData;

layout(binding = 2) uniform sampler defaultSampler;
layout(binding = 6) uniform textureCube cubeMap;
layout(binding = 7) uniform textureCube irradianceMap;
layout(binding = 8) uniform texture2D checkerBoardTexture;
// TODO: Extract this global constant somewhere (should match number of directionalLightCount)
layout(binding = 11) uniform sampler2DArray shadowMap;

layout(location = 0) out vec4 outputColor;

layout(location = 0) in vec3 fragNormal;
layout(location = 2) in vec3 fragLocalPosition;
layout(location = 3) in vec2 fragTexCoord;
layout(location = 4) in vec3 fragWorldPosition;
layout(location = 5) in vec3 fragCameraPosition;
layout(location = 6) in vec4 fragLightPosition[RS_MAX_DIRECTIONAL_LIGHTS];

float CalculateShadow(uint layer, vec4 p)
{
    // Perform perspective divide in case of a perspective projection matrix
    // (has no effect for orthographic)
    vec3 projectedCoords = p.xyz / p.w;

    // Map to 0-1 range for texture lookup
    vec2 uv = projectedCoords.xy * 0.5 + 0.5;

    // 0.23
    float closestDepth = texture(shadowMap, vec3(uv.xy, layer)).r;

    // 0.24
    float currentDepth = projectedCoords.z;

    float bias = 0.001;
    float shadow = currentDepth - bias > closestDepth ? 1.0 : 0.0;

    // Always bigger by 0.37...
    return shadow;
}

vec3 CalculateDirectionalLighting(vec3 albedo, vec3 N, vec3 V)
{
    vec3 outgoingRadiance = vec3(0);
    for (uint i = 0; i < lightData.directionalLightCount; i++)
    {
        vec3 lightDirection = normalize(vec3(
                    lightData.directionalLights[i].directionX,
                    lightData.directionalLights[i].directionY,
                    lightData.directionalLights[i].directionZ));

        vec3 incomingRadiance = vec3(
                    lightData.directionalLights[i].radianceR,
                    lightData.directionalLights[i].radianceG,
                    lightData.directionalLights[i].radianceB);

        vec3 L = -lightDirection;
        vec3 H = normalize(L + V);
        float cosineTheta = max(0.0, dot(N, L));

        float exponent = 32.0;
        float spec = pow(max(dot(N, H), 0.0), exponent);

        float visibility = 1.0f - CalculateShadow(i, fragLightPosition[i]);

        outgoingRadiance += albedo * visibility * incomingRadiance * cosineTheta;

        // Disable specular for now, want to add point lights first so we can
        // have a better test using a plane
        //outgoingRadiance += spec * incomingRadiance * cosineTheta;
    }

    return outgoingRadiance;
}

vec3 CalculatePointLighting(vec3 albedo, vec3 N, vec3 V)
{
    vec3 outgoingRadiance = vec3(0.0);
    for (uint i = 0; i < lightData.sphereLightCount; i++)
    {
        vec3 sphereCenter = vec3(lightData.sphereLights[i].px,
                                 lightData.sphereLights[i].py,
                                 lightData.sphereLights[i].pz);

        float radius = lightData.sphereLights[i].radius;
        vec3 incomingRadiance = vec3(lightData.sphereLights[i].radianceR,
                                     lightData.sphereLights[i].radianceG,
                                     lightData.sphereLights[i].radianceB);

        vec3 L = sphereCenter - fragWorldPosition;
        float dist = length(L);
        L = normalize(L);
        float cosineTheta = max(0.0, dot(N, L));

        // TODO: Use radius
        //float k = radius / dist;
        //lightRadiance *= (k * k);
        float attenuation = 1.0 / (dist * dist);

        outgoingRadiance += albedo * incomingRadiance * attenuation * cosineTheta;
    }

    return outgoingRadiance;
}

void main()
{
    vec3 emissionColor = vec3(0, 0, 0);
    vec3 baseColor =
        texture(sampler2D(checkerBoardTexture, defaultSampler), fragTexCoord).rgb;

    vec3 normal = normalize(fragNormal);
    vec3 position = fragWorldPosition;
    vec3 view = normalize(fragCameraPosition - position);

    vec3 outgoingRadiance = vec3(0);
#if 0
    // Ambient light
    vec3 ambientLightRadiance = vec3(lightData.ambientLight.radianceR,
        lightData.ambientLight.radianceG, lightData.ambientLight.radianceB);

    // TODO: Ambient occlusion
    // FIXME: Color for this doesn't look right
    outgoingRadiance += baseColor * ambientLightRadiance;
#endif

    // Environment map lighting
    vec3 environmentMapRadiance =
        texture(samplerCube(irradianceMap, defaultSampler), normal).rgb;

    outgoingRadiance += baseColor * environmentMapRadiance;

    // Directional lighting
    outgoingRadiance += CalculateDirectionalLighting(baseColor, normal, view);

    // Sphere/point lights
    outgoingRadiance += CalculatePointLighting(baseColor, normal, view);

#if 0
    // Disk light test. From "Moving Frostbite to PBR" presentation
    for (uint i = 0; i < lightData.diskLightCount; i++)
    {
        vec3 diskNormal = vec3(lightData.diskLights[i].nx,
                lightData.diskLights[i].ny,
                lightData.diskLights[i].nz);

        vec3 diskPosition = vec3(lightData.diskLights[i].px,
                lightData.diskLights[i].py,
                lightData.diskLights[i].pz);

        vec3 diskRadiance = vec3(lightData.diskLights[i].rr,
                lightData.diskLights[i].rg,
                lightData.diskLights[i].rb);

        float diskRadius = lightData.diskLights[i].radius;

        // TODO: Horizon clipping!
        vec3 L = diskPosition - fragWorldPosition;
        float sqrDist = dot(L, L);
        L = normalize(L);
        vec3 N = normal;
        float luminance = PI * max(0.0, dot(diskNormal, -L)) *
            max(0.0, dot(N, L)) / (sqrDist / (diskRadius * diskRadius) + 1);
        outgoingRadiance += baseColor * diskRadiance * luminance;
    }
#endif
    outgoingRadiance += emissionColor;

    outputColor = vec4(outgoingRadiance, 0);
}
