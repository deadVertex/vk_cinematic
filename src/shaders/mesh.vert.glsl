#version 450
#extension GL_ARB_shader_viewport_layer_array : enable

#define CAMERA_INDEX_SHADOW_PASS 2
// NOTE: Needs to be kept in sync with src/rasterizer_data_model.h and frag shader
#define RS_MAX_DIRECTIONAL_LIGHTS 4

layout(binding = 0) uniform UniformBufferObject {
    mat4 viewMatrices[16];
    mat4 projectionMatrices[16];
    vec3 cameraPosition;
    uint showComparision;
} ubo;

struct VertexPC
{
    float px, py, pz;
    float cr, cg, cb;
};

struct VertexPNT
{
    float px, py, pz;
    float nx, ny, nz;
    float tx, ty;
};

layout(binding = 1) readonly buffer Vertices
{
    VertexPNT vertices[];
};

layout(binding = 4) readonly buffer ModelMatrices
{
    mat4 modelMatrices[];
};

layout(push_constant) uniform PushConstants
{
    uint modelMatrixIndex;
    uint vertexDataOffset;
    uint cameraIndex;
    int layer;
};


layout(location = 0) out vec3 fragNormal;
layout(location = 2) out vec3 fragLocalPosition;
layout(location = 3) out vec2 fragTexCoord;
layout(location = 4) out vec3 fragWorldPosition;
layout(location = 5) out vec3 fragCameraPosition;
layout(location = 6) out vec4 fragLightPosition[RS_MAX_DIRECTIONAL_LIGHTS];

void main()
{
    mat4 modelMatrix = modelMatrices[modelMatrixIndex];
    mat3 invModelMatrix = transpose(mat3(modelMatrix));

    vec3 inPosition = vec3(
            vertices[gl_VertexIndex + vertexDataOffset].px,
            vertices[gl_VertexIndex + vertexDataOffset].py,
            vertices[gl_VertexIndex + vertexDataOffset].pz);

    vec3 inNormal = vec3(
            vertices[gl_VertexIndex + vertexDataOffset].nx,
            vertices[gl_VertexIndex + vertexDataOffset].ny,
            vertices[gl_VertexIndex + vertexDataOffset].nz);

    vec2 inTextureCoord = vec2(
            vertices[gl_VertexIndex + vertexDataOffset].tx,
            vertices[gl_VertexIndex + vertexDataOffset].ty);

    gl_Position = ubo.projectionMatrices[cameraIndex] *
                  ubo.viewMatrices[cameraIndex] * 
                  modelMatrix *
                  vec4(inPosition, 1.0);

    // Transform normal into world space
    fragNormal = normalize(inNormal * invModelMatrix);

    fragLocalPosition = inPosition;

    fragTexCoord = inTextureCoord;

    fragWorldPosition = vec3(modelMatrix * vec4(inPosition, 1.0));

    fragCameraPosition = ubo.cameraPosition;

    for (uint i = 0; i < RS_MAX_DIRECTIONAL_LIGHTS; i++)
    {
        fragLightPosition[i] = ubo.projectionMatrices[CAMERA_INDEX_SHADOW_PASS + i] *
            ubo.viewMatrices[CAMERA_INDEX_SHADOW_PASS + i] * modelMatrix * 
            vec4(inPosition, 1.0);
    }

    gl_Layer = layer;
}
