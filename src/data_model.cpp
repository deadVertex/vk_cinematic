inline vec3 JsonArrayToVec3(yyjson_val *arr)
{
    vec3 result = {};
    Assert(yyjson_is_arr(arr));
    Assert(yyjson_arr_size(arr) == 3);

    for (u32 i = 0; i < 3; i++)
    {
        yyjson_val *v = yyjson_arr_get(arr, i);
        Assert(yyjson_is_real(v));
        result.data[i] = (f32)yyjson_get_real(v);
    }

    return result;
}

inline vec4 JsonArrayToVec4(yyjson_val *arr)
{
    vec4 result = {};
    Assert(yyjson_is_arr(arr));
    Assert(yyjson_arr_size(arr) == 4);

    for (u32 i = 0; i < 4; i++)
    {
        yyjson_val *v = yyjson_arr_get(arr, i);
        Assert(yyjson_is_real(v));
        result.data[i] = (f32)yyjson_get_real(v);
    }

    return result;
}

// Convert from axis angle to quaternion with angle in degrees
inline quat JsonArrayToQuat(yyjson_val *arr)
{
    vec4 v = JsonArrayToVec4(arr);

    vec3 axis = v.xyz;
    f32 angle = v.w; // Angle is in degrees

    return Quat(axis, Radians(angle));
}

internal b32 ParseMaterial(
    yyjson_val *material, DataModelMaterial *materialDefinition, u32 *hashedId)
{
    Assert(yyjson_is_obj(material));
    yyjson_val *id = yyjson_obj_get(material, "id");
    Assert(id != NULL);
    Assert(yyjson_is_str(id));
    *hashedId = HashStringU32(yyjson_get_str(id));

    yyjson_val *albedo =
        yyjson_obj_get(material, "albedo_texture");
    if (albedo != NULL)
    {
        materialDefinition->albedoTexture =
            HashStringU32(yyjson_get_str(albedo));
    }
    yyjson_val *emission =
        yyjson_obj_get(material, "emission_texture");
    if (emission != NULL)
    {
        materialDefinition->emissionTexture =
            HashStringU32(yyjson_get_str(emission));
    }

    yyjson_val *roughness = yyjson_obj_get(material, "roughness");
    if (roughness != NULL)
    {
        Assert(yyjson_is_real(roughness));
        materialDefinition->roughness = (f32)yyjson_get_real(roughness);
    }
    else
    {
        materialDefinition->roughness = 1.0f;
    }

    return true;
}

internal b32 DataModelFromJson(
    DataModel *dataModel, const char *jsonStr, MemoryArena *tempArena)
{
    b32 result = true;
    yyjson_doc *doc = yyjson_read(jsonStr, strlen(jsonStr), 0);
    if (doc != NULL)
    {
        yyjson_val *root = yyjson_doc_get_root(doc);
        if (root != NULL)
        {
            Assert(yyjson_is_obj(root));

            // Textures
            yyjson_val *textures = yyjson_obj_get(root, "textures");
            Assert(textures != NULL);
            Assert(yyjson_is_arr(textures));
            {
                yyjson_arr_iter iter = yyjson_arr_iter_with(textures);
                yyjson_val *texture = yyjson_arr_iter_next(&iter);
                while (texture != NULL)
                {
                    Assert(yyjson_is_obj(texture));
                    yyjson_val *id = yyjson_obj_get(texture, "id");
                    Assert(id != NULL);
                    Assert(yyjson_is_str(id));
                    u32 hashedId = HashStringU32(yyjson_get_str(id));

                    yyjson_val *path = yyjson_obj_get(texture, "path");
                    yyjson_val *generator = yyjson_obj_get(texture, "generator");
                    if (path != NULL)
                    {
                        Assert(yyjson_is_str(path));

                        const char *pathMemory =
                            CopyString(tempArena, yyjson_get_str(path));
                        RegisterTexture(dataModel, hashedId, pathMemory);
                    }
                    else if (generator != NULL)
                    {
                        Assert(yyjson_is_str(generator));

                        // NOTE: Only checkerboard texture supported for now
                        RegisterTextureCheckerboard(dataModel, hashedId);
                    }
                    else
                    {
                        yyjson_val *singleColor =
                            yyjson_obj_get(texture, "single_color");
                        if (singleColor != NULL)
                        {
                            vec4 color = JsonArrayToVec4(singleColor);
                            RegisterTexture(dataModel, hashedId, color);
                        }
                    }

                    texture = yyjson_arr_iter_next(&iter);
                }
            }

            // Materials
            yyjson_val *materials = yyjson_obj_get(root, "materials");
            Assert(materials != NULL);
            Assert(yyjson_is_arr(materials));
            {
                yyjson_arr_iter iter = yyjson_arr_iter_with(materials);
                yyjson_val *material = yyjson_arr_iter_next(&iter);
                while (material != NULL)
                {
                    DataModelMaterial materialDefinition = {};
                    u32 hashedId = 0;
                    if (ParseMaterial(material, &materialDefinition, &hashedId))
                    {
                        RegisterMaterial(dataModel, hashedId, materialDefinition);
                    }

                    material = yyjson_arr_iter_next(&iter);
                }
            }

            // Meshes
            yyjson_val *meshes = yyjson_obj_get(root, "meshes");
            Assert(meshes != NULL);
            Assert(yyjson_is_arr(meshes));
            {
                yyjson_arr_iter iter = yyjson_arr_iter_with(meshes);
                yyjson_val *mesh = yyjson_arr_iter_next(&iter);
                while (mesh != NULL)
                {
                    Assert(yyjson_is_obj(mesh));
                    yyjson_val *id = yyjson_obj_get(mesh, "id");
                    Assert(id != NULL);
                    Assert(yyjson_is_str(id));
                    u32 hashedId = HashStringU32(yyjson_get_str(id));

                    yyjson_val *path = yyjson_obj_get(mesh, "path");
                    if (path != NULL)
                    {
                        Assert(yyjson_is_str(path));

                        const char *pathMemory = CopyString(tempArena, yyjson_get_str(path));
                        RegisterMesh(dataModel, hashedId, pathMemory);
                    }
                    else
                    {
                        yyjson_val *generator = yyjson_obj_get(mesh, "generator");
                        Assert(generator != NULL);

                        u32 generatorId = GeneratedMesh_Cube;
                        if (strcmp(yyjson_get_str(generator), "cube") == 0)
                        {
                            generatorId = GeneratedMesh_Cube;
                        }
                        else if (strcmp(yyjson_get_str(generator), "triangle") == 0)
                        {
                            generatorId = GeneratedMesh_Triangle;
                        }
                        else if (strcmp(yyjson_get_str(generator), "plane") == 0)
                        {
                            generatorId = GeneratedMesh_Plane;
                        }
                        else if (strcmp(yyjson_get_str(generator), "sphere") == 0)
                        {
                            generatorId = GeneratedMesh_Sphere;
                        }
                        else
                        {
                            LogMessage("Unknown mesh generator %s", yyjson_get_str(generator));
                        }

                        RegisterMesh(dataModel, hashedId, generatorId);
                    }

                    mesh = yyjson_arr_iter_next(&iter);
                }
            }

            // Entities
            yyjson_val *entities = yyjson_obj_get(root, "entities");
            Assert(entities != NULL);
            Assert(yyjson_is_arr(entities));
            {
                yyjson_arr_iter iter = yyjson_arr_iter_with(entities);
                yyjson_val *entity = yyjson_arr_iter_next(&iter);
                while (entity != NULL)
                {
                    Assert(yyjson_is_obj(entity));
                    yyjson_val *id = yyjson_obj_get(entity, "id");
                    Assert(id != NULL);
                    Assert(yyjson_is_str(id));

                    yyjson_val *mesh = yyjson_obj_get(entity, "mesh");
                    Assert(mesh != NULL);
                    Assert(yyjson_is_str(mesh));
                    u32 meshId = HashStringU32(yyjson_get_str(mesh));

                    yyjson_val *material = yyjson_obj_get(entity, "material");
                    Assert(material != NULL);
                    Assert(yyjson_is_str(material));
                    u32 materialId = HashStringU32(yyjson_get_str(material));

                    vec3 position = JsonArrayToVec3(yyjson_obj_get(entity, "position"));
                    vec3 scale = JsonArrayToVec3(yyjson_obj_get(entity, "scale"));
                    quat rotation = Quat();
                    if (yyjson_obj_get(entity, "rotation"))
                    {
                        rotation = JsonArrayToQuat(yyjson_obj_get(entity, "rotation"));
                    }

                    MeshData meshData = {};
                    u32 hashedId = HashStringU32(yyjson_get_str(id));
                    AddEntity(dataModel, hashedId,
                            meshId, materialId, position, rotation, scale);

                    entity = yyjson_arr_iter_next(&iter);
                }
            }

            // Directional lights
            yyjson_val *directionalLights = yyjson_obj_get(root, "directional_lights");
            if (directionalLights != NULL)
            {
                Assert(yyjson_is_arr(directionalLights));
                {
                    yyjson_arr_iter iter =
                        yyjson_arr_iter_with(directionalLights);
                    yyjson_val *light = yyjson_arr_iter_next(&iter);
                    while (light != NULL)
                    {
                        Assert(yyjson_is_obj(light));
                        yyjson_val *id = yyjson_obj_get(light, "id");
                        Assert(id != NULL);
                        Assert(yyjson_is_str(id));
                        u32 hashedId = HashStringU32(yyjson_get_str(id));

                        vec3 radiance =
                            JsonArrayToVec3(yyjson_obj_get(light, "radiance"));
                        vec3 direction = Normalize(JsonArrayToVec3(
                            yyjson_obj_get(light, "direction")));

                        AddDirectionalLight(
                            dataModel, hashedId, radiance, direction);

                        light = yyjson_arr_iter_next(&iter);
                    }
                }
            }

            // Point lights
            yyjson_val *pointLights = yyjson_obj_get(root, "point_lights");
            if (pointLights != NULL)
            {
                Assert(yyjson_is_arr(pointLights));
                {
                    yyjson_arr_iter iter =
                        yyjson_arr_iter_with(pointLights);
                    yyjson_val *light = yyjson_arr_iter_next(&iter);
                    while (light != NULL)
                    {
                        Assert(yyjson_is_obj(light));
                        yyjson_val *id = yyjson_obj_get(light, "id");
                        Assert(id != NULL);
                        Assert(yyjson_is_str(id));
                        u32 hashedId = HashStringU32(yyjson_get_str(id));

                        vec3 radiance =
                            JsonArrayToVec3(yyjson_obj_get(light, "radiance"));
                        vec3 position =
                            JsonArrayToVec3(yyjson_obj_get(light, "position"));

                        AddPointLight(
                            dataModel, hashedId, radiance, position);

                        light = yyjson_arr_iter_next(&iter);
                    }
                }
            }

            // Camera
            yyjson_val *camera = yyjson_obj_get(root, "camera");
            Assert(camera != NULL);
            Assert(yyjson_is_obj(camera));

            yyjson_val *cameraPositionVal = yyjson_obj_get(camera, "position");
            vec3 cameraPosition = JsonArrayToVec3(cameraPositionVal);
            ConfigureCamera(dataModel, cameraPosition, Quat());

            // Background materialId
            yyjson_val *backgroundMaterialVal = yyjson_obj_get(root, "backgroundMaterial");
            Assert(backgroundMaterialVal != NULL);
            Assert(yyjson_is_str(backgroundMaterialVal));
            dataModel->backgroundMaterialId = HashStringU32(yyjson_get_str(backgroundMaterialVal));
        }
        yyjson_doc_free(doc);
    }
    else
    {
        LogMessage("Failed to parse JSON data");
    }

    return result;
}

