/* 
======================== RESUME 2023 ===================
Task List:
- Command line arg triggered dump images to disk

Thinking:
- Lighting
    - Directional light
            -> Raster shadow improvements
                - Shadow acne (not going to try do more at this stage, good enough)
                - Soft shadows (not unique problem to rasterizer currently)
                    - PCF (for rasterizer)
                    - PCSS (https://developer.download.nvidia.com/shaderlibrary/docs/shadow_PCSS.pdf)
                    - Something more exotic (i.e. variance map shadows)
                    - Blender uses angle on the sun light to give soft shadows
    - Point light
        [Rasterizer]
            -> Shadows?
    Seem to have introduced a bug with how we combine directional lights with
    environment lighting, getting strange dark areas in the transition area
    - Bug with normal on sphere mesh on edge of light in path tracer?
    - Area lights
        
- Materials
    - Specular reflections again! (This is hard environment maps raster)

        https://renderwonk.com/publications/s2010-shading-course/
        -> Test scene - middle gray and chrome sphere in HDRi scene with ground plane
            -> Fix irradiance map generation time (PAINFUL)

        ----- Use area light test scene? ----
        -> path tracer chance of specular bounce + 'roughness' controlled specular lobe
            -> Material parameter to control roughness
            -> Pre-compute reflection cube maps for raster using random sampling?
        -> raster - basic blinn/phong
        -----
    - Material model infra
    - Blin/Phong material model
    - Microfacet material model
    - Normal maps, roughness, metallic, etc
- Other
- Stock take and figure out where we were at (and whats working) [DONE FOR NOW]
    - Revert to simplest possible scene (geomentry, lighting, etc)
        - Multiple meshes (bunny and a plane) [x]
        - Multiple materials [x]
            - Diffuse only material model [X]
            - Albedo Single color [X] (NO TEST YET)
            - Emission
            - Multiple albedo textures [2] (NOPE)
        - Multiple meshes (instancing for path tracer)
        - Camera models are a complete mess
            - Mostly fixed by using old values manually matched between the two renderers
        - Scene
            - Non-uniform scaling (seems fine)

        - Lighting purely from environment map?
            - What lighting features do we support
            - Light only test i.e. diffuse pure white materials?
            - Emission?
            - Directional lights
            - Point lights
            - Area lights
                - Disk
                - Quad
                - Tube?
        - Rasterizer features
            - Shadow maps
            - Screen space ambient occlusion
            - Depth of Field
            - Bloom (also used for path tracer output)
        - Material models
            - PBR
    - Pretty sure code for creating the irradiance map on the CPU is going to be wrong
    - Working towards 'The Forest' like scene with:
        - Lots of geomemtry: Trees, bushes, grass, rocks
        - Lots of shadows
        - Sub surface scattering leaves
        - Materials with albedo, normal, roughness, etc textures

- Get some end to end tests going
    - Generate images from both renderers which show significant differences
    (This has opened a can of worms where the current path tracer has been
    shown to not be a good reference which makes knowing what the rasterizer
    should look like quite a bit more challenging).
    - Perhaps for now we focus on generating representative comparison images
    for each feature (for both rasterizer and path tracer) and comparing
    manually or rather for documentation purposes of what we support.
    - This would cause us to need a lot more configurability in the
    application. Being able to set scene, camera, sample counts, materials, etc
    at runtime so we can automate the generating of these comparison images as
    part of regression testing.
    - It would drive a change in testing focus from System/E2E to more focused
    component level testing to provide feedback on code changes. Comparison
    images will generate to much noise to be used for meaningful development
    feedback but rather just as a way of showing longer term progress.

- Component/unit test for path tracer diffuse material with directional light
1. Given the rendering equation
    radianceToCamera = emission + Sum( brdf(L,V,N) * incomingRadiance(L) * cosTheta)

- Shaders to respect asset dir
- Descriptor set management (fixed list of descriptor writes, hardcoded binding numbers)

- Idea: Shift perspective for CPU path tracer to focus more on mathematical
interpretation (i.e "Solving the Rendering Equation") rather than just
rendering

Problems that this test has reveiled:
    - Camera aspect and FOV is completely differerent
        -> Support specifying resolution independent of window size?
    -> FUTURE: Setting background (to cubemap + irradiance) will require updating descriptor set for each material
    - Mess of memory arenas

Problems found:
    - Memory arenas....
    - Support for shaders?
    - Sloooooow
    - Use spherical coordinates for evaluating BRDF so we can easily multiply
      contribution to integral by Cos(theta) * Sin(theta) [NOT YET - More fundamental changes needed to path tracer]
    - Something very wrong with path tracer output (waaaay to dim) - need more tests

======================== OLD ===========================
Priorities:
0. Fun to work on
1. Easy to tweak, change and experiment with (changability, configurability, modular, fast iteration times)
2. Debugability and Observability
3. Physically based (accuracy?)
4. Performant
5. Reusable

BIG BANG REFACTOR:
- (1) Get rid of bat file build system DRY (TOO HARD, build system is a big mess right now)
- (1) Define our modules and their interfaces
- (1) Module reloading without restarting application
- (1) Cycle time monitoring (build time, app start up time, module restart time)
- (1) Config system?
- (1) First class Linux support?

TODO (NEW):
- QoL improvements:
    - Texture binding mess (see FIXMEs)
    - Clean up how we are passing data to shaders (i.e. radianceR, radianceG,
    radianceB) - This is actually not bad, despite a small bit of developer
    overhead this we are not wasting any memory for alignment. Alternative
    would be to use vec4 for everywhere we use vec3 in the storage buffers but
    that would increase the amount of work we need to do on the CPU side.
    - TODO: Setup lights in the scene via material instead of raw radiance values
    - Improve output comparison system? (Swtich between CPU and Rast without losing CPU image)
    - FEAT: Better system for managing textures
- Bugs:
    - FIXME: Random vector on hemi-sphere code is generating non-uniform terrible results
    - SRGB conversion is inaccurate approximation
- FEAT: Restore Environment/HDR lighting
- FEAT: RAY support window resizing
- FETA: Proper API for scene construction
- FEAT: Proper support for multiple scenes to help with testing
- FEAT: Image channels (separate direct and indirect lighting)
- FEAT: Lights for rasterization
    - Sphere
    - Ambient term [x]
    - Disk [x]
    - Spot
    - Quad
    - Tube/Line?
- FEAT: Proper PBR material model
    - Smith GGX BRDF
    - Albedo maps
    - Normal maps
    - Metalness
    - Roughness
- FEAT: Tone mapping
    - Reinhard
    - Filmic
    - ACES
- FEAT: GPU Ray tracing (Parked for now, want to work on rast and CPU for now)
    - Basic compute shader implementation [x]
    - Camera [x]
    - Basic ray bouncing [x]
    - Basic diffuse material [x]
    - Multi sampling [x]
    - Don't run GPU ray tracing every frame! [x]
    - Texture mapping [x]
    - Triangle intersection [x]
    - Fix camera FOV to match CPU path tracer [ ]
    - Triangle mesh support [x]
    - Uploading meshes [x]
    - Split compute shader workload into smaller chunks for prevent lockup [x]
    - Update screen with partial updates from GPU ray tracing [x]
    - Include system for shaders [ ]
    - Unit test framework for compute shaders [ ]
    - Broadphase? (Top Level Acceleration Structure) [ ] (NEEDS INFRA WORK)
        - Building broadphase tree on GPU [ ]
        - Ray intersection broadphase tree [ ]
    - Midphase (Bottom Level Acceleration Structure) [ ]
    - Fix texture coordinates mismatch [ ]
    - Accumulate pixel samples over multiple frames [ ]
    - Skip drawing rasterization scene when GPU ray tracing [ ]
- FEAT: Rasterizer Screen space ambient occlusion
- FEAT: Rasterizer Screen space reflections
- FEAT: Bloom Post processing
- FEAT: Rasterizer Global Illumination
    - Light Propagation Volumes
    - Voxel cone tracing
    - SDFGI
    - Screen space GI
    - Light probes
- FEAT: Rasterizer Shadows
    - Directional 
    - Omni directional
    - Cascaded shadow maps
- FEAT: CPU Ray tracer optimizations
    - FIXME: Use semaphore for signally between worked threads rather than busy wait with sleep
    - Importance sampling
    - Next event estimation
    - Surface Area heuristic acceleration structure
    - Improve acceleration structure construction performance
    - FIXME: Avoid computing AABB from mesh vertices each time we add an object to our scene
    - TODO: Only generate acceleration structure for meshes used in the scene
*/
/* TODO:
List:
 - Code duplication for creating and uploading images/cube maps to GPU [x]
- [CPU] Don't model skybox using geometry [x]

Bugs:
 - Race condition when submitting work to queue when queue is empty but worker
   threads are still working on the tasks they've pulled from the queue.
 - IBL looks too dim, not sure what is causing it

Tech Debt:
 - Hard-coded material id to texture mapping in shader
 - [CPU] Ray tracer image resolution is hard-coded

Features:
 - Linear space rendering [x]
 - Texture mapping [x]
 - Smooth shading [x]
 - Bilinear sampling [x]
 - Image based lighting [x]
 - ACES tone mapping
 - Bloom
 - gltf importing
 - Cube map texture loading

Performance testing infrastructure
- Performance test suite
- Runtime/startup scene configuration

Functionality testing infrastructure
- Unit tests!

Visualization infrastructure

Usability
- Realtime feedback of ray tracing
- Live code reloading? +1
- Scene definition from file [ ] (Nice to have)
- Resource definition from file

Optimizations
- [CPU] Multiple triangles per tree leaf node
- [CPU] SIMD
- [CPU] Multi-core [X]
- [RAS] Sample cube maps in shaders rather than equirectangular images [x]
- [ALL] Startup time is too long! (building AABB trees for meshes most likely)
- [CPU] Profiling! (What is our current cost per ray?) - Midphase is culprit (tree is too deep/unbalanced)
- [CPU] Don't ray trace whole screen when using comparison view

Analysis
- AABB trees
- Triangle meshes
- Intersection functions
- Assembly code

Observability
- More metrics
- More logs
- More profiling
*/

#include <cstdarg>

#include "config.h"
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#ifdef PLATFORM_WINDOWS
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#elif defined(PLATFORM_LINUX)
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>
#endif

// Move to vulkan specific module?
#include <vulkan/vulkan.h>
#ifdef PLATFORM_WINDOWS
#include <vulkan/vulkan_win32.h>
#endif

#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include <yyjson.h>
#include <argtable3.h>

#include "platform.h"
#include "input.h"

struct DebugReadFileResult
{
    void *contents;
    u32 length;
};

#define DebugReadEntireFile(NAME) DebugReadFileResult NAME(const char *path)
typedef DebugReadEntireFile(DebugReadEntireFileFunction);
#define DebugFreeFileMemory(NAME) void NAME(void *memory)
typedef DebugFreeFileMemory(DebugFreeFileMemoryFunction);

internal void* AllocateMemory(u64 size, u64 baseAddress = 0);
internal void FreeMemory(void *p);
internal DebugReadEntireFile(ReadEntireFile);

#include "math_lib.h"
#include "mesh.h"
#include "asset_loader/asset_loader.h"
#include "data_model.h"
#include "hash.h"
#include "dictionary.h"
#include "aabb.h"
#include "rasterizer_data_model.h"
#include "vulkan_renderer.h"
#include "profiler.h"
#include "debug.h"
#include "intrinsics.h"
#include "work_queue.h"
#include "tile.h"
#include "memory_pool.h"
#include "bvh.h"
#include "ray_intersection.h"
#include "image.h"
#include "sp_scene.h"
#include "sp_material_system.h"
#include "sp_metrics.h"
#include "sp_light_system.h"
#include "simd_path_tracer.h"
#include "simd.h"
#include "aabb.h"
#include "cmdline.h"
#include "cubemap.h"
#include "asset_system.h"

#include "debug.cpp"
#include "ray_intersection.cpp"
#include "cubemap.cpp"
#include "vulkan_renderer.cpp"
#include "mesh.cpp"
#include "mesh_generation.cpp"

#include "image.cpp"
#include "memory_pool.cpp"
#include "bvh.cpp"
#include "sp_scene.cpp"
#include "sp_material_system.cpp"
#include "sp_light_system.cpp"
#include "simd_path_tracer.cpp"
#include "asset_system.cpp"
#include "data_model.cpp"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

struct sp_Task
{
    sp_Context *context;
    Tile tile;
};

global GLFWwindow *g_Window;
global u32 g_FramebufferWidth = 1024;
global u32 g_FramebufferHeight = 768;

internal DebugLogMessage(LogMessage_)
{
    char buffer[256];
    va_list args;
    va_start(args, fmt);
    vsnprintf(buffer, sizeof(buffer), fmt, args);
    va_end(args);
#ifdef PLATFORM_WINDOWS
    OutputDebugString(buffer);
    OutputDebugString("\n");
#endif
    puts(buffer);
}

internal void GlfwErrorCallback(int error, const char *description)
{
    LogMessage("GLFW Error (%d): %s.", error, description);
}

#ifdef PLATFORM_WINDOWS
internal void *AllocateMemory(u64 size, u64 baseAddress)
{
    void *result =
        VirtualAlloc((void*)baseAddress, size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    return result;
}

internal void FreeMemory(void *p)
{
    VirtualFree(p, 0, MEM_RELEASE);
}

DebugReadEntireFile(ReadEntireFile)
{
    DebugReadFileResult result = {};
    HANDLE file = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, 0,
                              OPEN_EXISTING, 0, 0);
    if (file != INVALID_HANDLE_VALUE)
    {
        LARGE_INTEGER tempSize;
        if (GetFileSizeEx(file, &tempSize))
        {
            result.length = SafeTruncateU64ToU32(tempSize.QuadPart);
            result.contents = AllocateMemory(result.length);

            if (result.contents)
            {
                DWORD bytesRead;
                if (!ReadFile(file, result.contents, result.length, &bytesRead,
                              0) ||
                    (result.length != bytesRead))
                {
                    LogMessage("Failed to read file %s", path);
                    FreeMemory(result.contents);
                    result.contents = NULL;
                    result.length = 0;
                }
            }
            else
            {
                LogMessage("Failed to allocate %d bytes for file %s",
                          result.length, path);
            }
        }
        else
        {
            LogMessage("Failed to read file length for file %s", path);
        }
        CloseHandle(file);
    }
    else
    {
        LogMessage("Failed to open file %s", path);
    }
    return result;
}

#elif defined(PLATFORM_LINUX)
internal void *AllocateMemory(u64 numBytes, u64 baseAddress)
{
    void *result = mmap((void *)baseAddress, numBytes, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    Assert(result != MAP_FAILED);

    return result;
}

internal void FreeMemory(void *p)
{
    munmap(p, 0);
}

// NOTE: bytesToRead must equal the size of the file, if great we will enter an
// infinite loop.
internal bool ReadFile(int file, void *buf, int bytesToRead)
{
    while (bytesToRead)
    {
        int bytesRead = read(file, buf, bytesToRead);
        if (bytesRead == -1)
        {
            return false;
        }
        bytesToRead -= bytesRead;
        buf = (u8 *)buf + bytesRead;
    }
    return true;
}

internal DebugReadEntireFile(ReadEntireFile)
{
    DebugReadFileResult result = {};
    int file = open(path, O_RDONLY);
    if (file != -1)
    {
        struct stat fileStatus;
        if (fstat(file, &fileStatus) != -1)
        {
            result.length = SafeTruncateU64ToU32(fileStatus.st_size);
            result.contents = AllocateMemory(result.length);
            if (result.contents)
            {
                if (!ReadFile(file, result.contents, result.length))
                {
                    LogMessage("Failed to read file %s", path);
                    FreeMemory(result.contents);
                    result.contents = nullptr;
                    result.length = 0;
                }
            }
            else
            {
                LogMessage("Failed to allocate %d bytes for file %s",
                          result.length, path);
                result.length = 0;
            }
        }
        else
        {
            LogMessage("Failed to read file size for file %s", path);
        }
        close(file);
    }
    else
    {
        LogMessage("Failed to open file %s", path);
    }
    return result;
}
#endif

#define KEY_HELPER(NAME)                                                       \
    case GLFW_KEY_##NAME:                                                      \
        return KEY_##NAME;
internal i32 ConvertKey(int key)
{
    if (key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT)
    {
        return key;
    }
    switch (key)
    {
        KEY_HELPER(BACKSPACE);
        KEY_HELPER(TAB);
        KEY_HELPER(INSERT);
        KEY_HELPER(HOME);
        KEY_HELPER(PAGE_UP);
    // Can't use KEY_HELPER( DELETE ) as windows has a #define for DELETE
    case GLFW_KEY_DELETE:
        return KEY_DELETE;
        KEY_HELPER(END);
        KEY_HELPER(PAGE_DOWN);
        KEY_HELPER(ENTER);

        KEY_HELPER(LEFT_SHIFT);
    case GLFW_KEY_LEFT_CONTROL:
        return KEY_LEFT_CTRL;
        KEY_HELPER(LEFT_ALT);
        KEY_HELPER(RIGHT_SHIFT);
    case GLFW_KEY_RIGHT_CONTROL:
        return KEY_RIGHT_CTRL;
        KEY_HELPER(RIGHT_ALT);

        KEY_HELPER(LEFT);
        KEY_HELPER(RIGHT);
        KEY_HELPER(UP);
        KEY_HELPER(DOWN);

        KEY_HELPER(ESCAPE);

        KEY_HELPER(F1);
        KEY_HELPER(F2);
        KEY_HELPER(F3);
        KEY_HELPER(F4);
        KEY_HELPER(F5);
        KEY_HELPER(F6);
        KEY_HELPER(F7);
        KEY_HELPER(F8);
        KEY_HELPER(F9);
        KEY_HELPER(F10);
        KEY_HELPER(F11);
        KEY_HELPER(F12);
    case GLFW_KEY_KP_0:
        return KEY_NUM0;
    case GLFW_KEY_KP_1:
        return KEY_NUM1;
    case GLFW_KEY_KP_2:
        return KEY_NUM2;
    case GLFW_KEY_KP_3:
        return KEY_NUM3;
    case GLFW_KEY_KP_4:
        return KEY_NUM4;
    case GLFW_KEY_KP_5:
        return KEY_NUM5;
    case GLFW_KEY_KP_6:
        return KEY_NUM6;
    case GLFW_KEY_KP_7:
        return KEY_NUM7;
    case GLFW_KEY_KP_8:
        return KEY_NUM8;
    case GLFW_KEY_KP_9:
        return KEY_NUM9;
    case GLFW_KEY_KP_DECIMAL:
        return KEY_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return KEY_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return KEY_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return KEY_NUM_MINUS;
    case GLFW_KEY_KP_ADD:
        return KEY_NUM_PLUS;
    case GLFW_KEY_KP_ENTER:
        return KEY_NUM_ENTER;
    }
    return KEY_UNKNOWN;
}


void KeyCallback(GLFWwindow *window, int glfwKey, int scancode, int action, int mods)
{
    GameInput *input =
            (GameInput *)glfwGetWindowUserPointer(window);

    i32 key = ConvertKey(glfwKey);
    if (key != KEY_UNKNOWN)
    {
        Assert(key < MAX_KEYS);
        if (action == GLFW_PRESS)
        {
            input->buttonStates[key].isDown = true;
        }
        else if (action == GLFW_RELEASE)
        {
            input->buttonStates[key].isDown = false;
        }
        // else: ignore key repeat messages
    }
}

internal void MouseButtonCallback(
    GLFWwindow *window, int button, int action, int mods)
{
    GameInput *input =
            (GameInput *)glfwGetWindowUserPointer(window);

    if (button == GLFW_MOUSE_BUTTON_LEFT)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_LEFT].isDown = (action == GLFW_PRESS);
    }
    if (button == GLFW_MOUSE_BUTTON_MIDDLE)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_MIDDLE].isDown = (action == GLFW_PRESS);
    }
    if (button == GLFW_MOUSE_BUTTON_RIGHT)
    {
        input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown = (action == GLFW_PRESS);
    }
}

global f64 g_PrevMousePosX;
global f64 g_PrevMousePosY;

internal void CursorPositionCallback(GLFWwindow *window, f64 xPos, f64 yPos)
{
    GameInput *input = (GameInput *)glfwGetWindowUserPointer(window);

    // Unfortunately GLFW doesn't seem to provide a way to get mouse motion
    // rather than cursor position so we have to compute the relative motion
    // ourselves.
    f64 relX = xPos - g_PrevMousePosX;
    f64 relY = yPos - g_PrevMousePosY;
    g_PrevMousePosX = xPos;
    g_PrevMousePosY = yPos;

    input->mouseRelPosX = (i32)Floor((f32)relX);
    input->mouseRelPosY = (i32)Floor((f32)relY);
    input->mousePosX = (i32)Floor((f32)xPos);
    input->mousePosY = (i32)Floor((f32)yPos);
}

internal void ShowMouseCursor(b32 isVisible)
{
    glfwSetInputMode(g_Window, GLFW_CURSOR,
        isVisible ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
}

struct FreeRoamCamera
{
    vec3 position;
    vec3 velocity;
    vec3 rotation;
};

global FreeRoamCamera g_camera;

internal void UpdateFreeRoamCamera(
    FreeRoamCamera *camera, GameInput *input, f32 dt)
{
    vec3 position = camera->position;
    vec3 velocity = camera->velocity;
    vec3 rotation = camera->rotation;

    f32 speed = 20.0f; //200.0f;
    f32 friction = 18.0f;

    f32 sens = 0.005f;
    f32 mouseX = -input->mouseRelPosY * sens;
    f32 mouseY = -input->mouseRelPosX * sens;

    vec3 newRotation = Vec3(mouseX, mouseY, 0.0f);

    if (input->buttonStates[KEY_MOUSE_BUTTON_RIGHT].isDown)
    {
        rotation += newRotation;
        ShowMouseCursor(false);
    }
    else
    {
        ShowMouseCursor(true);
    }

    rotation.x = Clamp(rotation.x, -PI * 0.5f, PI * 0.5f);

    if (rotation.y > 2.0f * PI)
    {
        rotation.y -= 2.0f * PI;
    }
    if (rotation.y < 2.0f * -PI)
    {
        rotation.y += 2.0f * PI;
    }

    f32 forwardMove = 0.0f;
    f32 rightMove = 0.0f;
    if (input->buttonStates[KEY_W].isDown)
    {
        forwardMove = -1.0f;
    }

    if (input->buttonStates[KEY_S].isDown)
    {
        forwardMove = 1.0f;
    }

    if (input->buttonStates[KEY_A].isDown)
    {
        rightMove = -1.0f;
    }
    if (input->buttonStates[KEY_D].isDown)
    {
        rightMove = 1.0f;
    }

    if (input->buttonStates[KEY_LEFT_SHIFT].isDown)
    {
        speed *= 10.0f;
    }

    mat4 rotationMatrix = RotateY(rotation.y) * RotateX(rotation.x);

    vec3 forward = (rotationMatrix * Vec4(0, 0, 1, 0)).xyz;
    vec3 right = (rotationMatrix * Vec4(1, 0, 0, 0)).xyz;

    vec3 targetDir = Normalize(forward * forwardMove + right * rightMove);

    velocity -= velocity * friction * dt;
    velocity += targetDir * speed * dt;

    position = position + velocity * dt;

    camera->position = position;
    camera->velocity = velocity;
    camera->rotation = rotation;
}

internal void Update(DataModel *dataModel, GameInput *input, f32 dt)
{
    UpdateFreeRoamCamera(&g_camera, input, dt);
    ConfigureCamera(dataModel, g_camera.position,
        Quat(Vec3(0, 1, 0), g_camera.rotation.y) *
            Quat(Vec3(1, 0, 0), g_camera.rotation.x));
}

internal void DrawEntityAabbs(Scene scene, DebugDrawingBuffer *debugDrawBuffer)
{
    for (u32 entityIndex = 0; entityIndex < scene.count; ++entityIndex)
    {
        Entity *entity = scene.entities + entityIndex;
        DrawBox(debugDrawBuffer, entity->aabbMin, entity->aabbMax,
            Vec3(0.8, 0.4, 0.2));
    }
}

internal void DrawSceneAabb(Scene scene, DebugDrawingBuffer *debugDrawBuffer)
{
    DrawBox(
        debugDrawBuffer, scene.aabb.min, scene.aabb.max, Vec3(0.2, 0.4, 0.8));
}

global sp_Metrics g_metricsBuffer[MAX_TILES];
global volatile i32 g_metricsBufferLength;

internal void WorkerThread(WorkQueue *queue)
{
    while (1)
    {
        // If not empty
        if (queue->head != queue->tail)
        {
            RandomNumberGenerator rng = {};
            rng.state = 0xF51C0E49;

            // Work to do
            sp_Metrics metrics = {};
            sp_Task *task = (sp_Task *)WorkQueuePop(queue, sizeof(sp_Task));
            sp_PathTraceTile(task->context, task->tile, &rng, &metrics);

            u32 index = AtomicExchangeAdd(&g_metricsBufferLength, 1);
            g_metricsBuffer[index] = metrics;
        }
        else
        {
            // FIXME: Use a semaphore for signalling
#ifdef PLATFORM_WINDOWS
            Sleep(10);
#elif defined(PLATFORM_LINUX)
            usleep(1000);
#endif
        }
    }
}

#ifdef PLATFORM_WINDOWS
internal DWORD WinWorkerThreadProc(LPVOID lpParam) 
{
    WorkerThread((WorkQueue *)lpParam);
    return 0;
}
#elif defined(PLATFORM_LINUX)
internal void* LinuxWorkerThreadProc(void *arg)
{
    WorkerThread((WorkQueue *)arg);
    return NULL;
}
#endif

struct ThreadMetaData
{
#ifdef PLATFORM_WINDOWS
    HANDLE handle;
    DWORD id;
#elif defined(PLATFORM_LINUX)
    pthread_t handle;
#endif
};

struct ThreadPool
{
    ThreadMetaData threads[MAX_THREADS];
};

internal ThreadPool CreateThreadPool(WorkQueue *queue)
{
    ThreadPool pool = {};

#ifdef PLATFORM_WINDOWS
    for (u32 threadIndex = 0; threadIndex < MAX_THREADS; ++threadIndex)
    {
        ThreadMetaData metaData = {};
        metaData.handle =
            CreateThread(NULL, 0, WinWorkerThreadProc, queue, 0, &metaData.id);
        Assert(metaData.handle != INVALID_HANDLE_VALUE);
        LogMessage("ThreadId is %u", metaData.id);

        pool.threads[threadIndex] = metaData;
    }
#elif defined(PLATFORM_LINUX)
    for (u32 threadIndex = 0; threadIndex < MAX_THREADS; ++threadIndex)
    {
        ThreadMetaData metaData = {};
        int ret = pthread_create(&metaData.handle, NULL, LinuxWorkerThreadProc, queue);
        Assert(ret == 0);
        LogMessage("Thread handle is %u", metaData.handle);

        pool.threads[threadIndex] = metaData;
    }
#endif
    return pool;
}

internal void AddRayTracingWorkQueue(WorkQueue *workQueue, sp_Context *ctx)
{
    Assert(workQueue->head == workQueue->tail);

    HdrImage *outputImage = ctx->camera->outputImage;

    // TODO: Don't need to compute and store and array for this, could just
    // store a queue of tile indices that the worker threads read from. They
    // can then construct the tile data for each index from just the image
    // dimensions and tile dimensions.
    Tile tiles[MAX_TILES];
    u32 tileCount = ComputeTiles(outputImage->width, outputImage->height,
        TILE_WIDTH, TILE_HEIGHT, tiles, ArrayCount(tiles));

    workQueue->tail = 0; // oof
    for (u32 i = 0; i < tileCount; ++i)
    {
        sp_Task task = {};
        task.context = ctx;
        task.tile = tiles[i];
        g_metricsBufferLength = 0;
        WorkQueuePush(workQueue, &task, sizeof(task));
    }

    workQueue->head = 0; // ooof
}

internal void DrawMeshDataNormals(
    DebugDrawingBuffer *debugDrawBuffer, MeshData meshData)
{
    f32 normalLength = 0.1f;
    u32 triangleCount = meshData.indexCount / 3;
    for (u32 triangleIndex = 0; triangleIndex < triangleCount; ++triangleIndex)
    {
        u32 i = triangleIndex * 3;
        u32 j = triangleIndex * 3 + 1;
        u32 k = triangleIndex * 3 + 2;

        VertexPNT vertices[3];
        vertices[0] = meshData.vertices[i];
        vertices[1] = meshData.vertices[j];
        vertices[2] = meshData.vertices[k];

        DrawTriangle(debugDrawBuffer, vertices[0].position,
            vertices[1].position, vertices[2].position, Vec3(0, 0.8, 1));

        // Draw normals
        DrawLine(debugDrawBuffer, vertices[0].position,
            vertices[0].position + vertices[0].normal * normalLength,
            Vec3(1, 0, 1));
        DrawLine(debugDrawBuffer, vertices[1].position,
            vertices[1].position + vertices[1].normal * normalLength,
            Vec3(1, 0, 1));
        DrawLine(debugDrawBuffer, vertices[2].position,
            vertices[2].position + vertices[2].normal * normalLength,
            Vec3(1, 0, 1));
    }
}

internal HdrImage LoadImage(
    const char *relativePath, MemoryArena *imageDataArena, const char *assetDir)
{
    HdrImage result = {};

    char fullPath[256];
    snprintf(fullPath, sizeof(fullPath), "%s/%s", assetDir, relativePath);

    HdrImageData tempImage = {};
    if(LoadHdrImage(&tempImage, fullPath) == 0)
    {
        result =
            AllocateImage(tempImage.width, tempImage.height, imageDataArena);
        CopyMemory(result.pixels, tempImage.pixels,
            result.width * result.height * sizeof(vec4));
        FreeHdrImage(tempImage);
    }
    else
    {
        LogMessage("Failed to load image %s", fullPath);
    }

    return result;
}

internal sp_Mesh sp_CreateMeshFromMeshData(
    MeshData meshData, MemoryArena *arena, b32 useSmoothShading)
{
    sp_Mesh mesh = sp_CreateMesh(meshData.vertices, meshData.vertexCount,
        meshData.indices, meshData.indexCount, useSmoothShading);

    return mesh;
}

internal void DebugDrawBvh(bvh_Node *node, DebugDrawingBuffer *debugDrawBuffer)
{
    for (u32 i = 0; i < 4; i++)
    {
        bvh_Node *child = node->children[i];
        if (child != NULL)
        {
            DebugDrawBvh(child, debugDrawBuffer);
            //DrawBox(debugDrawBuffer, child->min, child->max, Vec3(1, 1, 0));
        }
    }

    DrawBox(debugDrawBuffer, node->min, node->max, Vec3(0, 1, 0));
}

internal void AddEntity(Scene *scene, vec3 position, quat rotation, vec3 scale,
    u32 mesh, u32 material, Aabb meshAabb)
{
    // TODO: Support non-uniform scaling in the ray tracer
    //Assert(scale.x == scale.y && scale.x == scale.z);
    if (scene->count < scene->max)
    {
        Entity *entity = scene->entities + scene->count++;
        entity->position = position;
        entity->rotation = rotation;
        entity->scale = scale;
        entity->mesh = mesh;
        entity->material = material;

        Aabb transformedAabb = TransformAabb(
            meshAabb.min, meshAabb.max, position, rotation, scale);

        entity->aabbMin = transformedAabb.min;
        entity->aabbMax = transformedAabb.max;
    }
}

internal void ComputeSceneAabb(Scene *scene)
{
    if (scene->count > 0)
    {
        Entity firstEntity = scene->entities[0];
        scene->aabb = CreateAabb(firstEntity.aabbMin, firstEntity.aabbMax);

        for (u32 i = 1; i < scene->count; i++)
        {
            Entity *entity = scene->entities + i;
            scene->aabb = AabbUnion(
                    scene->aabb, CreateAabb(entity->aabbMin, entity->aabbMax));
        }
    }
    else
    {
        scene->aabb = {};
    }
}

internal void PopulateAssetSystem(AssetSystem *assetSystem, DataModel *dataModel)
{
    for (u32 i = 0; i < dataModel->textureCount; ++i)
    {
        DataModelTexture texture = dataModel->textures[i];

        if (!HasTexture(assetSystem, texture.id))
        {
            switch (texture.source)
            {
                case TextureSource_Disk:
                    RegisterTextureFromDisk(assetSystem, texture.id, texture.path);
                    break;
                case TextureSource_SingleColor:
                    RegisterSingleColorTexture(
                            assetSystem, texture.id, texture.singleColor);
                    break;
                case TextureSource_Checkerboard:
                    RegisterCheckerboardTexture(assetSystem, texture.id);
                    break;
                default:
                    InvalidCodePath();
                    break;
            }
        }
    }

    for (u32 i = 0; i < dataModel->meshCount; i++)
    {
        DataModelMesh mesh = dataModel->meshes[i];

        if (!HasMesh(assetSystem, mesh.id))
        {
            if (mesh.path != NULL)
            {
                RegisterMeshFromDisk(assetSystem, mesh.id, mesh.path);
            }
            else
            {
                RegisterGeneratedMesh(assetSystem, mesh.id, mesh.generator);
            }
        }
    }
}

internal void SyncPathTracerWithDataModel(DataModel *dataModel,
    MemoryArena *meshDataArena, MemoryArena *accelerationStructureMemoryArena,
    MemoryArena *tempArena, Dictionary *meshStorage, sp_Context *context,
    HdrImage *outputImage, AssetSystem *assetSystem)
{
    sp_MaterialSystem *materialSystem = context->materialSystem;
    sp_Scene *scene = context->scene;
    sp_Camera *camera = context->camera;
    sp_LightSystem *lightSystem = context->lightSystem;

    // TODO: Find any textures that data model has which path tracer doesn't know about
    for (u32 i = 0; i < dataModel->textureCount; ++i)
    {
        DataModelTexture texture = dataModel->textures[i];
        if (sp_FindTexture(materialSystem, texture.id) == NULL)
        {
            HdrImage *image = GetTexture(assetSystem, texture.id);
            Assert(image != NULL);
            sp_RegisterTexture(materialSystem, *image, texture.id);
        }
    }

    for (u32 i = 0; i < dataModel->materialCount; i++)
    {
        DataModelMaterial materialDefinition = dataModel->materials[i];

        if (sp_FindMaterialById(materialSystem, materialDefinition.id) == NULL)
        {
            // Map data model material to sp_Material
            sp_Material material = {};
            material.albedoTexture = materialDefinition.albedoTexture;
            material.emissionTexture = materialDefinition.emissionTexture;
            material.roughness = materialDefinition.roughness;
            sp_RegisterMaterial(materialSystem, material, materialDefinition.id);
        }
    }

    for (u32 i = 0; i < dataModel->meshCount; i++)
    {
        DataModelMesh meshDefinition = dataModel->meshes[i];

        if (Dict_FindItem(meshStorage, sp_Mesh, meshDefinition.id) == NULL)
        {
            MeshData *meshData = GetMesh(assetSystem, meshDefinition.id);
            Assert(meshData != NULL);

            sp_Mesh mesh =
                sp_CreateMeshFromMeshData(*meshData, meshDataArena, true);
            sp_BuildMeshMidphase(
                &mesh, accelerationStructureMemoryArena, tempArena);

            sp_Mesh *meshDst =
                Dict_AddItem(meshStorage, sp_Mesh, meshDefinition.id);
            *meshDst = mesh;
        }
    }

    for (u32 i = 0; i < dataModel->entityCount; i++)
    {
        DataModelEntity entityDefinition = dataModel->entities[i];

        sp_Mesh *mesh =
            Dict_FindItem(meshStorage, sp_Mesh, entityDefinition.meshId);
        Assert(mesh != NULL);

        sp_AddObjectToScene(scene, *mesh,
            entityDefinition.materialId, entityDefinition.position,
            entityDefinition.rotation, entityDefinition.scale);
    }

    for (u32 i = 0; i < dataModel->directionalLightCount; i++)
    {
        DataModelDirectionalLight light = dataModel->directionalLights[i];

        sp_AddDirectionalLight(
            lightSystem, light.radiance, light.direction, light.id);
    }

    for (u32 i = 0; i < dataModel->pointLightCount; i++)
    {
        DataModelPointLight light = dataModel->pointLights[i];

        sp_AddPointLight(
            lightSystem, light.radiance, light.position, light.id);
    }

    // TODO: Cheat for now
    materialSystem->backgroundMaterialId = HashStringU32("materials/background");

    sp_ConfigureCamera(camera, outputImage, dataModel->camera.position,
        dataModel->camera.rotation, 0.8f);
}

internal void DrawShadowProjectionMatrix(VulkanRenderer *renderer, Scene scene,
        DebugDrawingBuffer *debugDrawBuffer)
{
    if (scene.lightData->directionalLightCount == 0)
    {
        return;
    }
    vec3 lightDirection = scene.lightData->directionalLights[0].direction;
    mat4 directionalLightMatrix = ChangeOfBasisZ(-lightDirection);
    mat4 lightToWorld = directionalLightMatrix;// * Translate(Vec3(0, 0, 5));
#if 0

    Aabb aabb = scene.aabb;
    // Compute left, right, top, bottom, near, far
    Aabb lightSpaceAabb = TransformAabb(aabb.min, aabb.max, worldToLightSpace);

    // Assume we're sticking to an aspect ratio of 1
    f32 k = Max(Abs(Min(lightSpaceAabb.min.x, lightSpaceAabb.min.y)),
        Max(lightSpaceAabb.max.x, lightSpaceAabb.max.y));
#endif

    // Compute view to world matrix
    UniformBufferObject *ubo =
        (UniformBufferObject *)renderer->uniformBuffer.data;
    mat4 viewMatrix = ubo->viewMatrices[CameraIndex_ShadowPass];

#if 0
    // Shitty inverse
    mat4 inv = Transpose(viewMatrix);
    inv.columns[0].w = 0.0f;
    inv.columns[1].w = 0.0f;
    inv.columns[2].w = 0.0f;
    //inv.columns[3] = -viewMatrix.columns[3];
    inv.columns[3].w = 1.0f;

    mat4 ident = viewMatrix * inv;
    LogMessage("[%g, %g, %g, %g]", ident.data[0], ident.data[4], ident.data[8],
        ident.data[12]);
    LogMessage("[%g, %g, %g, %g]", ident.data[1], ident.data[5], ident.data[9],
        ident.data[13]);
    LogMessage("[%g, %g, %g, %g]", ident.data[2], ident.data[6], ident.data[10],
        ident.data[14]);
    LogMessage("[%g, %g, %g, %g]", ident.data[3], ident.data[7], ident.data[11],
        ident.data[15]);
#endif

    f32 k = 1.0f;
    f32 zNear = -10.0f;
    f32 zFar = 10.0f;
    vec3 vertices[] = {
        Vec3(-k, -k, zNear),
        Vec3(-k, k, zNear),
        Vec3(k, k, zNear),
        Vec3(k, -k, zNear),
        Vec3(-k, -k, zFar),
        Vec3(-k, k, zFar),
        Vec3(k, k, zFar),
        Vec3(k, -k, zFar),
    };

    for (u32 i = 0; i < ArrayCount(vertices); i++)
    {
        vertices[i] = TransformPoint(vertices[i], lightToWorld);
    }

    vec3 color = Vec3(0, 1, 0);
    DrawLine(debugDrawBuffer, vertices[0], vertices[1], color);
    DrawLine(debugDrawBuffer, vertices[1], vertices[2], color);
    DrawLine(debugDrawBuffer, vertices[2], vertices[3], color);
    DrawLine(debugDrawBuffer, vertices[3], vertices[0], color);

    DrawLine(debugDrawBuffer, vertices[4], vertices[5], color);
    DrawLine(debugDrawBuffer, vertices[5], vertices[6], color);
    DrawLine(debugDrawBuffer, vertices[6], vertices[7], color);
    DrawLine(debugDrawBuffer, vertices[7], vertices[4], color);

    DrawLine(debugDrawBuffer, vertices[0], vertices[4], color);
    DrawLine(debugDrawBuffer, vertices[1], vertices[5], color);
    DrawLine(debugDrawBuffer, vertices[2], vertices[6], color);
    DrawLine(debugDrawBuffer, vertices[3], vertices[7], color);
}

// TODO: Should this rather use the data model as input?
internal void DrawPointLightVisualizations(
    DataModel *dataModel, DebugDrawingBuffer *debugDrawBuffer)
{
    for (u32 i = 0; i < dataModel->pointLightCount; i++)
    {
        DataModelPointLight light = dataModel->pointLights[i];

        DrawSphere(debugDrawBuffer, light.position, light.radiance,
            0.1f, 15);
    }
}

// TODO: Should this rather use the data model as input?
internal void DrawDirectionalLightVisualization(
    DataModel *dataModel, DebugDrawingBuffer *debugDrawBuffer)
{
    for (u32 i = 0; i < dataModel->directionalLightCount; i++)
    {
        DataModelDirectionalLight light = dataModel->directionalLights[i];

        f32 distanceFromOrigin = 10.0f;
        f32 lineLength = 8.0f;
        vec3 position = light.direction * -distanceFromOrigin;

        vec3 right = Vec3(1, 0, 0);
        if (Dot(right, light.direction) <= EPSILON)
        {
            right = Vec3(0, 0, 1);
        }

        vec3 color = light.radiance;
        DrawCircle(
            debugDrawBuffer, position, right, light.direction, color, 0.5f, 15);
        DrawLine(debugDrawBuffer, position,
            position + light.direction * lineLength, color);
    }
}

internal VulkanImage CreateOutputCubeMap(VulkanRenderer *renderer, u32 width)
{
    VkFormat format = VK_FORMAT_R32G32B32A32_SFLOAT;
    VulkanImage outputImage = VulkanCreateImage(renderer->device,
        renderer->physicalDevice, width, width, format,
        VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_COLOR_BIT, true);

    VulkanTransitionImageLayout(outputImage.handle,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_GENERAL, renderer->device,
        renderer->commandPool, renderer->graphicsQueue, true);

    return outputImage;
}

internal void WriteDescriptorSet(VulkanRenderer *renderer,
    VkDescriptorSet descriptorSet, VulkanImage outputCubeMap,
    VulkanImage inputImage)
{
    VkDescriptorImageInfo outputCubeMapInfo = {};
    outputCubeMapInfo.imageView = outputCubeMap.view;
    outputCubeMapInfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;

    VkDescriptorImageInfo inputImageInfo = {};
    inputImageInfo.imageView = inputImage.view;
    inputImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    inputImageInfo.sampler = renderer->defaultSampler;

    VkWriteDescriptorSet descriptorWrites[2] = {};
    {
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = descriptorSet;
        descriptorWrites[0].dstBinding = 0;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pImageInfo = &outputCubeMapInfo;
    }
    {
        descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].dstSet = descriptorSet;
        descriptorWrites[1].dstBinding = 1;
        descriptorWrites[1].descriptorType =
            VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pImageInfo = &inputImageInfo;
    }

    vkUpdateDescriptorSets(renderer->device, ArrayCount(descriptorWrites),
        descriptorWrites, 0, NULL);
}

// TODO: Remove the default descriptor set so this is no longer needed
internal void UpdateDefaultDescriptorSet(
    VulkanRenderer *renderer, VulkanImage outputCubeMap, u32 dstBinding)
{
    for (u32 i = 0; i < renderer->swapchain.imageCount; ++i)
    {
        VkDescriptorImageInfo imageInfo = {};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = outputCubeMap.view;

        VkWriteDescriptorSet descriptorWrites[1] = {};
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = renderer->descriptorSets[i];
        descriptorWrites[0].dstBinding = dstBinding;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pImageInfo = &imageInfo;
        vkUpdateDescriptorSets(renderer->device, ArrayCount(descriptorWrites),
            descriptorWrites, 0, NULL);
    }
}

internal void ConvertEquirectangularToCubeMap(VulkanRenderer *renderer,
    u32 inputTextureId, u32 cubeMapId, u32 dstBinding, u32 width, u32 height)
{
    // NOTE: Assuming pipeline has already been created
    VulkanImage outputCubeMap = CreateOutputCubeMap(renderer, width);
    StoreVulkanTexture(renderer, cubeMapId, outputCubeMap);

    VulkanImage *inputImage = VulkanFindTextureById(renderer, inputTextureId);
    Assert(inputImage != NULL);
    // NOTE: Assuming input texture is already in layout optimized for reading?

    WriteDescriptorSet(renderer, renderer->convertEquirectangular.descriptorSet,
        outputCubeMap, *inputImage);

    DispatchComputeShader(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->convertEquirectangular.pipeline,
        renderer->convertEquirectangular.pipelineLayout,
        renderer->convertEquirectangular.descriptorSet, width,
        height, 6);

    VulkanTransitionImageLayout(outputCubeMap.handle,
        VK_IMAGE_LAYOUT_GENERAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, renderer->device,
        renderer->commandPool, renderer->graphicsQueue, true);

    UpdateDefaultDescriptorSet(renderer, outputCubeMap, dstBinding);
}

internal void GenerateIrradianceCubeMap(VulkanRenderer *renderer,
        u32 inputTextureId, u32 cubeMapId, u32 dstBinding, u32 width, u32 height)
{
    // NOTE: Assuming pipeline has already been created
    VulkanImage outputCubeMap = CreateOutputCubeMap(renderer, width);
    StoreVulkanTexture(renderer, cubeMapId, outputCubeMap);

    VulkanImage *inputImage = VulkanFindTextureById(renderer, inputTextureId);
    Assert(inputImage != NULL);

    WriteDescriptorSet(renderer, renderer->generateIrradiance.descriptorSet,
        outputCubeMap, *inputImage);

    DispatchComputeShader(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->generateIrradiance.pipeline,
        renderer->generateIrradiance.pipelineLayout,
        renderer->generateIrradiance.descriptorSet, width,
        height, 6);

    VulkanTransitionImageLayout(outputCubeMap.handle,
        VK_IMAGE_LAYOUT_GENERAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, renderer->device,
        renderer->commandPool, renderer->graphicsQueue, true);

    UpdateDefaultDescriptorSet(renderer, outputCubeMap, dstBinding);
}

internal void SyncRasterizerWithDataModel(VulkanRenderer *renderer,
    DataModel *dataModel, MemoryArena *imageDataArena, Scene *scene,
    AssetSystem *assetSystem)
{
    u32 cubeMapId = HashStringU32("textures/cube_map");
    u32 irradianceMapId = HashStringU32("textures/irradiance_map");

    u32 backgroundTextureId = 0;
    for (u32 i = 0; i < dataModel->materialCount; i++)
    {
        DataModelMaterial material = dataModel->materials[i];
        if (material.id == dataModel->backgroundMaterialId)
        {
            backgroundTextureId = material.emissionTexture;
        }
    }

    for (u32 i = 0; i < dataModel->textureCount; ++i)
    {
        DataModelTexture texture = dataModel->textures[i];
        if (VulkanFindTextureById(renderer, texture.id) == NULL)
        {
            HdrImage *image = GetTexture(assetSystem, texture.id);
            Assert(image != NULL);

            // FIXME: Don't need the dstBinding anymore :)
            UploadHdrImageToGPU(renderer, *image, texture.id, 8);

            if (texture.id == backgroundTextureId)
            {
                // Guess resolutions
                u32 cubeMapWidth = Max(1, image->width / 4);
                u32 irradianceMapWidth = cubeMapWidth > 1 ? 32 : 1;

                f64 cubeMapStart = glfwGetTime();
                //HdrCubeMap cubeMap = CreateCubeMap(
                    //*image, imageDataArena, cubeMapWidth, cubeMapWidth);
                //UploadCubeMapToGPU(renderer, cubeMap, cubeMapId, 6,
                    //cubeMapWidth, cubeMapWidth);
                ConvertEquirectangularToCubeMap(
                    renderer, texture.id, cubeMapId, 6, cubeMapWidth, cubeMapWidth);
                LogMessage("CreateCubeMap - %gs", glfwGetTime() - cubeMapStart);

                f64 irradianceStart = glfwGetTime();
                //HdrCubeMap irradianceCubeMap =
                    //CreateIrradianceCubeMap(*image, imageDataArena,
                        //irradianceMapWidth, irradianceMapWidth);
                //UploadCubeMapToGPU(renderer, irradianceCubeMap, irradianceMapId,
                    //7, irradianceMapWidth, irradianceMapWidth);
                GenerateIrradianceCubeMap(renderer, cubeMapId, irradianceMapId,
                    7, irradianceMapWidth, irradianceMapWidth);
                LogMessage("CreateIrradianceCubeMap - %gs", glfwGetTime() - irradianceStart);
            }
        }
    }

    for (u32 i = 0; i < dataModel->materialCount; i++)
    {
        DataModelMaterial material = dataModel->materials[i];

        if (VulkanFindMaterialById(renderer, material.id) == NULL)
        {
            // TODO: Is 0 actually a null value when its a hashed string?
            if (material.albedoTexture != 0)
            {
                VulkanCreateMaterial(renderer, material.id,
                    material.albedoTexture, cubeMapId, irradianceMapId);
            }
        }
    }

    for (u32 i = 0; i < dataModel->meshCount; i++)
    {
        DataModelMesh meshDefinition = dataModel->meshes[i];

        if (VulkanFindMeshById(renderer, meshDefinition.id) == NULL)
        {
            MeshData *meshData = GetMesh(assetSystem, meshDefinition.id);
            Assert(meshData != NULL);

            CopyMeshDataToUploadBuffer(renderer, *meshData, meshDefinition.id);
        }
    }
    VulkanCopyMeshDataToGpu(renderer);

    // HACK: Keep reseting the scene because we don't have a way of checking if
    // an entity already exists in the scene
    scene->count = 0;
    for (u32 i = 0; i < dataModel->entityCount; i++)
    {
        DataModelEntity entityDefinition = dataModel->entities[i];

        Aabb *meshAabb = GetMeshAabb(assetSystem, entityDefinition.meshId);
        Assert(meshAabb != NULL);

        AddEntity(scene, entityDefinition.position, entityDefinition.rotation,
            entityDefinition.scale, entityDefinition.meshId,
            entityDefinition.materialId, *meshAabb);
    }

    ComputeSceneAabb(scene);

    // TODO: Hard-code for now
    scene->backgroundMaterial = HashStringU32("materials/background");

    UniformBufferObject *ubo =
        (UniformBufferObject *)renderer->uniformBuffer.data;
    LightData *lightData = scene->lightData;

    mat4 directionalLightMatrix = Identity();
    Assert(dataModel->directionalLightCount <= RS_MAX_DIRECTIONAL_LIGHTS);
    Assert(ArrayCount(ubo->viewMatrices) >
           RS_MAX_DIRECTIONAL_LIGHTS + CameraIndex_ShadowPass);
    Assert(ArrayCount(ubo->projectionMatrices) >
           RS_MAX_DIRECTIONAL_LIGHTS + CameraIndex_ShadowPass);
    for (u32 i = 0; i < dataModel->directionalLightCount; i++)
    {
        DataModelDirectionalLight light = dataModel->directionalLights[i];
        lightData->directionalLights[i].direction = light.direction;
        lightData->directionalLights[i].radiance = light.radiance;

        // NOTE: Only support first directional light with shadow for now
        // NOTE: We negate light direction here because -Z is actually the
        // forward direction for the camera
        ubo->viewMatrices[CameraIndex_ShadowPass + i] =
            Transpose(ChangeOfBasisZ(-light.direction));
        ubo->projectionMatrices[CameraIndex_ShadowPass + i] =
            ComputeOrthographicProjectionForBoundingBox(
                ubo->viewMatrices[CameraIndex_ShadowPass + i], scene->aabb);
    }
    lightData->directionalLightCount = dataModel->directionalLightCount;

    Assert(dataModel->pointLightCount <= RS_MAX_SPHERE_LIGHTS);
    for (u32 i = 0; i < dataModel->pointLightCount; i++)
    {
        DataModelPointLight light = dataModel->pointLights[i];
        lightData->sphereLights[i].position = light.position;
        lightData->sphereLights[i].radiance = light.radiance;
        lightData->sphereLights[i].radius = 0.0f;
    }
    lightData->sphereLightCount = dataModel->pointLightCount;

    // Set rasterizer camera
    {
        vec3 cameraPosition = dataModel->camera.position;
        quat cameraRotation = dataModel->camera.rotation;

        ubo->cameraPosition = cameraPosition;
        ubo->viewMatrices[CameraIndex_Default] =
            Rotate(Conjugate(cameraRotation)) * Translate(-cameraPosition);
        ubo->viewMatrices[CameraIndex_Skybox] =
            Rotate(Conjugate(cameraRotation));

        f32 aspect =
            (f32)renderer->swapchain.width / (f32)renderer->swapchain.height;

        // Vulkan specific correction matrix
        mat4 correctionMatrix = {};
        correctionMatrix.columns[0] = Vec4(1, 0, 0, 0);
        correctionMatrix.columns[1] = Vec4(0, -1, 0, 0);
        correctionMatrix.columns[2] = Vec4(0, 0, 0.5f, 0);
        correctionMatrix.columns[3] = Vec4(0, 0, 0.5f, 1);
        ubo->projectionMatrices[CameraIndex_Default] =
            correctionMatrix *
            Perspective(CAMERA_FOV, aspect, CAMERA_NEAR_CLIP, 100.0f);
        ubo->projectionMatrices[CameraIndex_Skybox] =
            ubo->projectionMatrices[0];

#if 0
        ubo->projectionMatrices[CameraIndex_Default] = ubo->projectionMatrices[CameraIndex_ShadowPass];
        ubo->projectionMatrices[CameraIndex_Skybox] = ubo->projectionMatrices[CameraIndex_ShadowPass];
        ubo->viewMatrices[CameraIndex_Default] = ubo->viewMatrices[CameraIndex_ShadowPass];
        ubo->viewMatrices[CameraIndex_Skybox] = ubo->viewMatrices[CameraIndex_ShadowPass];
#endif
    }
}

internal b32 GenerateSingleColorTexture(
    HdrImage *image, MemoryArena *imageDataArena, vec4 color, void *userData)
{
    *image = CreateSingleColorImage(color, 1, 1, imageDataArena);
    return true;
}

internal b32 GenerateMesh(MeshData *meshData, Aabb *meshAabb, u32 generatorId,
    MemoryArena *arena, void *userData)
{
    b32 result = true;

    switch (generatorId)
    {
        case GeneratedMesh_Cube:
            *meshData = CreateCubeMesh(arena);
            break;
        case GeneratedMesh_Triangle:
            *meshData = CreateTriangleMeshData(arena);
            break;
        case GeneratedMesh_Plane:
            *meshData = CreatePlaneMesh(arena);
            break;
        case GeneratedMesh_Sphere:
            *meshData = CreateIcosahedronMesh(3, arena);
            break;
        default:
            InvalidCodePath();
            break;
    }

    *meshAabb = ComputeAabb(meshData->vertices, meshData->vertexCount);

    return result;
}

internal void TestVec4(f32 epsilon, vec4 expected, vec4 actual, const char *msg)
{
    if (Length(expected - actual) > epsilon)
    {
        LogMessage("=== TEST FAILED ===");
        LogMessage("\tExpected [%g %g %g %g] but was [%g, %g, %g, %g] - %s",
            expected.r, expected.g, expected.b, expected.a,
            actual.r, actual.g, actual.b, actual.a, msg);
    }
}

internal void TestMultipleMaterials(MemoryArena *imageDataArena,
        MemoryArena *meshDataArena,
        MemoryArena *accelerationStructureMemoryArena,
        MemoryArena *tempArena,
        MemoryArena *entityMemoryArena,
        MemoryArena *applicationMemoryArena,
        VulkanRenderer *renderer)
{
    DataModel dataModel = {};
    AssetSystem assetSystem = {};
    AssetSystemConfig assetSystemConfig = {};
    assetSystemConfig.maxTextures = 4;
    assetSystemConfig.maxMeshes = 2;
    assetSystemConfig.imageDataArenaSize = Kilobytes(1);
    assetSystemConfig.meshDataArenaSize = Kilobytes(1);
    InitializeAssetSystem(
        &assetSystem, applicationMemoryArena, assetSystemConfig);

    //1. Create 2 single color textures, create material for each
    u32 textureId1 = HashStringU32("textures/texture1");
    u32 textureId2 = HashStringU32("textures/texture2");
    u32 backgroundTextureId = HashStringU32("textures/background");

    RegisterTexture(&dataModel, textureId1, Vec4(1, 0, 0, 1));
    RegisterTexture(&dataModel, textureId2, Vec4(0, 0, 1, 1));
    RegisterTexture(&dataModel, backgroundTextureId, Vec4(1));

    u32 materialId1 = HashStringU32("materials/material1");
    u32 materialId2 = HashStringU32("materials/material2");
    u32 backgroundMaterialId = HashStringU32("materials/background");

    DataModelMaterial material1 = {};
    material1.albedoTexture = textureId1;
    RegisterMaterial(&dataModel, materialId1, material1);

    DataModelMaterial material2 = {};
    material2.albedoTexture = textureId2;
    RegisterMaterial(&dataModel, materialId2, material2);

    DataModelMaterial backgroundMaterial = {};
    backgroundMaterial.emissionTexture = backgroundTextureId;
    RegisterMaterial(&dataModel, backgroundMaterialId, backgroundMaterial);
    dataModel.backgroundMaterialId = backgroundMaterialId;

    //2. Create 2 meshes, assign material to each
    u32 cubeMeshId = Mesh_Cube;
    RegisterMesh(&dataModel, cubeMeshId, GeneratedMesh_Cube);

    AddEntity(&dataModel, HashStringU32("scene/cube1"), cubeMeshId, materialId1,
        Vec3(-0.5, 0, 0), Quat(), Vec3(1));
    AddEntity(&dataModel, HashStringU32("scene/cube2"), cubeMeshId, materialId2,
        Vec3(0.5, 0, 0), Quat(), Vec3(1));

    //3. Position camera to see both meshes
    ConfigureCamera(&dataModel, Vec3(0, 0, 1), Quat());

    // 3.5 Populate asset system from data model and generate asset data
    PopulateAssetSystem(&assetSystem, &dataModel);

    AssetSystemGenerator generator = {};
    generator.singleColor = GenerateSingleColorTexture;
    generator.mesh = GenerateMesh;
    GenerateAssetData(&assetSystem, generator, imageDataArena);

    // Build path tracer scene
    sp_Scene pathTracerScene = {};
    sp_InitializeScene(&pathTracerScene, applicationMemoryArena);

    sp_MaterialSystem materialSystem = {};
    sp_LightSystem lightSystem = {};
    sp_Camera camera = {};
    sp_Context context = {};
    context.materialSystem = &materialSystem;
    context.lightSystem = &lightSystem;
    context.camera = &camera;
    context.scene = &pathTracerScene;
    context.sampleCount = 32;

    HdrImage pathTracerOutput = AllocateImage(1024 / 16, 1024 / 16, tempArena);


    Dictionary pathTracerMeshStorage = Dict_CreateFromArena(meshDataArena, sp_Mesh, MAX_MESHES);
    SyncPathTracerWithDataModel(&dataModel, meshDataArena,
        accelerationStructureMemoryArena, tempArena, &pathTracerMeshStorage,
        &context, &pathTracerOutput, &assetSystem);

    // Build rasterizer scene struct
    Scene scene = {};
    // TODO: Mesh aabbs
    scene.entities = AllocateArray(entityMemoryArena, Entity, MAX_ENTITIES);
    scene.max = MAX_ENTITIES;
    scene.lightData = (LightData *)renderer->lightBuffer.data;
    SyncRasterizerWithDataModel(renderer, &dataModel, imageDataArena, &scene, &assetSystem);

    //4. Render (just path tracer for now)
    sp_BuildSceneBroadphase(&pathTracerScene);

    Tile tile = {0, 0, pathTracerOutput.width, pathTracerOutput.height};
    RandomNumberGenerator rng = {};
    rng.state = 1;
    sp_Metrics metrics = {};

    sp_PathTraceTile(&context, tile, &rng, &metrics);

    //5. Check that path tracer has both colors
    if (!stbi_write_hdr("path_tracer.hdr", pathTracerOutput.width,
            pathTracerOutput.height, 4, pathTracerOutput.pixels[0].data))
    {
        LogMessage("Failed to write out path_tracer.hdr");
    }

    //6. Check that rasterizer has both colors
    u32 outputFlags = Output_None;
    HdrImage rasterizerOutput = AllocateImage(
        renderer->swapchain.width, renderer->swapchain.height, tempArena);
    VulkanRender(renderer, outputFlags, scene, &rasterizerOutput);

    if (!stbi_write_hdr("rasterizer.hdr", rasterizerOutput.width,
            rasterizerOutput.height, 4, rasterizerOutput.pixels[0].data))
    {
        LogMessage("Failed to write out rasterizer.hdr");
    }

    // 7. Assert
    vec2 p0 = Vec2(0.25, 0.5);
    vec2 p1 = Vec2(0.75, 0.5);
    f32 epsilon = 0.1f;

    vec4 c0 = SampleImageNearest(pathTracerOutput, p0);
    TestVec4(epsilon, Vec4(1, 0, 0, 1), c0,
        "Left cube in path tracer output should be red");

    vec4 c1 = SampleImageNearest(pathTracerOutput, p1);
    TestVec4(epsilon, Vec4(0, 0, 1, 1), c1,
        "Right cube in path tracer output should be blue");

    vec4 c2 = SampleImageNearest(rasterizerOutput, p0);
    c2.a = 1.0f;
    TestVec4(epsilon, Vec4(1, 0, 0, 1), c2,
            "Left cube in rasterizer output should be red");

    vec4 c3 = SampleImageNearest(rasterizerOutput, p1);
    c3.a = 1.0f;
    TestVec4(epsilon, Vec4(0, 0, 1, 1), c3,
            "Right cube in rasterizer output should be red");
}

internal void AssertVec4Within(f32 epsilon, vec4 expected, vec4 actual)
{
    vec4 v = expected - actual;
    Assert(v.x >= -epsilon && v.x <= epsilon);
    Assert(v.y >= -epsilon && v.y <= epsilon);
    Assert(v.z >= -epsilon && v.z <= epsilon);
    Assert(v.w >= -epsilon && v.w <= epsilon);
}

internal void TestSingleMaterial(MemoryArena *imageDataArena,
        MemoryArena *meshDataArena,
        MemoryArena *accelerationStructureMemoryArena,
        MemoryArena *tempArena,
        MemoryArena *entityMemoryArena,
        MemoryArena *applicationMemoryArena,
        VulkanRenderer *renderer)
{
    DataModel dataModel = {};
    AssetSystem assetSystem = {};
    AssetSystemConfig assetSystemConfig = {};
    assetSystemConfig.maxTextures = 4;
    assetSystemConfig.maxMeshes = 2;
    assetSystemConfig.imageDataArenaSize = Kilobytes(1);
    assetSystemConfig.meshDataArenaSize = Kilobytes(1);
    InitializeAssetSystem(
        &assetSystem, applicationMemoryArena, assetSystemConfig);

    //1. Create single color texture and material (magenta)
    u32 textureId1 = HashStringU32("textures/texture_magenta");
    RegisterTexture(&dataModel, textureId1, Vec4(1, 0, 1, 1));

    u32 backgroundTextureId = HashStringU32("textures/background");
    RegisterTexture(&dataModel, backgroundTextureId, Vec4(1));

    u32 materialId1 = HashStringU32("materials/material_magenta");
    DataModelMaterial material1 = {};
    material1.albedoTexture = textureId1;
    RegisterMaterial(&dataModel, materialId1, material1);

    //2. Create pure white background material
    u32 backgroundMaterialId = HashStringU32("materials/background");
    DataModelMaterial backgroundMaterial = {};
    backgroundMaterial.emissionTexture = backgroundTextureId;
    RegisterMaterial(&dataModel, backgroundMaterialId, backgroundMaterial);
    dataModel.backgroundMaterialId = backgroundMaterialId;

    //3. Create simplest possible mesh (triangle?)
    u32 triangleMeshId = HashStringU32("meshes/triangle");
    RegisterMesh(&dataModel, triangleMeshId, GeneratedMesh_Triangle);
    // NOTE: Needed for rasterizer skybox
    RegisterMesh(&dataModel, HashStringU32("meshes/cube"), GeneratedMesh_Cube);

    //4. Single entity at origin with triangle mesh and material
    AddEntity(&dataModel, HashStringU32("scene/triangle"), triangleMeshId, materialId1,
        Vec3(0, 0, 0), Quat(), Vec3(1));

    //5. Position camera so that center of screen is on triangle
    ConfigureCamera(&dataModel, Vec3(0, 0, 1), Quat());

    // 5.5 Populate asset system from data model and generate asset data
    PopulateAssetSystem(&assetSystem, &dataModel);
    AssetSystemGenerator generator = {};
    generator.singleColor = GenerateSingleColorTexture;
    generator.mesh = GenerateMesh;
    GenerateAssetData(&assetSystem, generator, imageDataArena);

    // 6. Copy-Pasta
    // Build path tracer scene
    sp_Scene pathTracerScene = {};
    sp_InitializeScene(&pathTracerScene, applicationMemoryArena);
    sp_MaterialSystem materialSystem = {};
    sp_LightSystem lightSystem = {};
    sp_Camera camera = {};
    sp_Context context = {};
    context.materialSystem = &materialSystem;
    context.camera = &camera;
    context.scene = &pathTracerScene;
    context.sampleCount = 32;

    HdrImage pathTracerOutput = AllocateImage(1024 / 16, 1024 / 16, tempArena);

    Dictionary pathTracerMeshStorage =
        Dict_CreateFromArena(meshDataArena, sp_Mesh, MAX_MESHES);
    SyncPathTracerWithDataModel(&dataModel, meshDataArena,
        accelerationStructureMemoryArena, tempArena, &pathTracerMeshStorage,
        &context, &pathTracerOutput, &assetSystem);

    // Build rasterizer scene struct
    Scene scene = {};
    // TODO: Mesh aabbs
    scene.entities = AllocateArray(entityMemoryArena, Entity, MAX_ENTITIES);
    scene.max = MAX_ENTITIES;
    scene.lightData = (LightData *)renderer->lightBuffer.data;
    SyncRasterizerWithDataModel(renderer, &dataModel, imageDataArena, &scene, &assetSystem);

    //6. Render with path tracer
    sp_BuildSceneBroadphase(&pathTracerScene);

    Tile tile = {0, 0, pathTracerOutput.width, pathTracerOutput.height};
    RandomNumberGenerator rng = {};
    rng.state = 1;
    sp_Metrics metrics = {};

    sp_PathTraceTile(&context, tile, &rng, &metrics);

    if (!stbi_write_hdr("path_tracer_test1.hdr", pathTracerOutput.width,
            pathTracerOutput.height, 4, pathTracerOutput.pixels[0].data))
    {
        LogMessage("Failed to write out path_tracer_test1.hdr");
    }

    //6. Render scene with rasterizer
    u32 outputFlags = Output_None;
    HdrImage rasterizerOutput = AllocateImage(
        renderer->swapchain.width, renderer->swapchain.height, tempArena);
    VulkanRender(renderer, outputFlags, scene, &rasterizerOutput);

    if (!stbi_write_hdr("rasterizer_test1.hdr", rasterizerOutput.width,
            rasterizerOutput.height, 4, rasterizerOutput.pixels[0].data))
    {
        LogMessage("Failed to write out rasterizer_test1.hdr");
    }

    vec2 p = Vec2(0.5, 0.5);
    f32 epsilon = 0.05f;
    vec4 expected = Vec4(1, 0, 1, 1);
    //7. Check that path tracer has full magenta color
    vec4 c0 = SampleImageNearest(pathTracerOutput, p);

    /* NOTE: Accepting that path tracer output is too dim for now.
     * This is caused our uniform sampling and not correctly weighting the
     * contributions of those samples so we appear to be losing energy when
     * we generate sample vectors that do not closely align with the
     * surface normal (i.e. cosineTheta is close to 0).
     * To properly resolve this we need to rethink some core parts of the
     * path tracer with a better understanding of Monte Carlo integration
     * so we can apply importance sampling (i.e. correctly weighting our
     * samples and generating samples that are more likely to contribute to
     * the final radiance value).
     */
    TestVec4(epsilon, expected, c0,
        "Color from path tracer output should be pure magenta");

    //8. Check that rasterizer has full magenta color
    vec4 c1 = SampleImageNearest(rasterizerOutput, p);
    c1.a = 1.0f; // TODO: For some reason alpha coming back as 0
    TestVec4(epsilon, expected, c1,
        "Color from rasterizer output should be pure magenta");
}

internal void TestIntegrationOverHemisphere()
{
    RandomNumberGenerator rng = {};
    rng.state = 0xF51C0E49;

    vec3 n = Vec3(0, 1, 0);

    for (u32 k = 1; k < 100000; k*=2)
    {
        f32 randomIntegral = 0.0f;
        u32 maxSamples = k;
        for (u32 i = 0; i < maxSamples; i++)
        {
            // Generate random vector on hemisphere
            vec3 dir = RandomDirectionOnHemisphere(n, &rng);
            vec2 s = ToSphericalCoordinates(dir);

            f32 cosineTheta = Dot(dir, n);
            // FIXME: Pretty sure we do need to scale by Sin(theta)
            randomIntegral += cosineTheta * Sin(s.y);// * Sin(theta);
        }

        randomIntegral = PI * randomIntegral * (1.0f / (f32)maxSamples);
        LogMessage("Random integration over hemisphere: %g %u samples", randomIntegral, maxSamples);
    }

    f32 integral = 0.0f;
    f32 sampleDelta = 0.025f;
    u32 sampleCount = 0;
    for (f32 phi = -PI; phi < PI; phi += sampleDelta)
    {
        for (f32 theta = 0.0f; theta < 0.5f * PI; theta += sampleDelta)
        {
            vec3 tangentDir =
                MapSphericalToCartesianCoordinates(Vec2(phi, theta));

            f32 cosineTheta = Dot(tangentDir, n);

            f32 contrib = cosineTheta * Sin(theta);
            integral += contrib;
            sampleCount++;
        }
    }

    integral  = PI * integral * (1.0f / (f32)sampleCount);

    LogMessage("Integration over hemisphere: %g %u samples", integral, sampleCount);
    Assert(Abs(integral - 1.0f) <= 0.05f);
}

struct AssetSystemLoaderUserData
{
    const char *assetDir;
};

internal b32 LoadTextureFromDisk(HdrImage *image, MemoryArena *imageDataArena,
    const char *path, void *userDataIn)
{
    AssetSystemLoaderUserData *userData =
        (AssetSystemLoaderUserData *)userDataIn;

    *image = LoadImage(path, imageDataArena, userData->assetDir);
    return true;
}

internal b32 LoadMeshFromDisk(MeshData *meshData, Aabb *meshAabb,
    MemoryArena *meshDataArena, const char *path, void *userDataIn)
{
    AssetSystemLoaderUserData *userData =
        (AssetSystemLoaderUserData *)userDataIn;

    *meshData = LoadMesh(path, meshDataArena, userData->assetDir);
    *meshAabb = ComputeAabb(meshData->vertices, meshData->vertexCount);
    return true;
}

internal b32 GenerateCheckerboardTexture(
    HdrImage *image, MemoryArena *imageDataArena, void *userData)
{
    *image = CreateCheckerBoardImage(imageDataArena);
    return true;
}


internal void TestLoadingDataModelFromJson(MemoryArena *applicationMemoryArena,
        const char *assetDir,
        MemoryArena *tempArena,
        MemoryArena *meshDataArena,
        MemoryArena *accelerationStructureMemoryArena,
        MemoryArena *entityMemoryArena,
        VulkanRenderer *renderer)
{
    // Given some json string
    const char *jsonStr = R"FOO(
{
    "textures": [
        { 
            "id": "textures/hdri",
            "path": "kiara_4_mid-morning_4k.exr"
        },
        {
            "id": "textures/white",
            "single_color": [1.0, 1.0, 1.0, 1.0]
        }
    ],
    "materials": [
        {
            "id": "materials/material1",
            "albedo_texture": "textures/white"
        },
        {
            "id": "materials/background",
            "emission_texture": "textures/hdri"
        }
    ],
    "meshes": [
        {
            "id": "meshes/bunny",
            "path": "bunny.obj"
        },
        {
            "id": "meshes/cube",
            "generator": "cube"
        }
    ],
    "entities": [
        {
            "id": "scene/cube1",
            "mesh": "meshes/bunny",
            "material": "materials/material1",
            "position": [0.0, -0.5, 0.0],
            "scale": [5.0, 5.0, 5.0]
        }
    ],
    "camera": {
        "position": [0.0, 0.0, 1.0]
    },
    "backgroundMaterial": "materials/background"
}
    )FOO";
    // When we decode it with our cool function
    DataModel dataModel = {};
    b32 result = DataModelFromJson(&dataModel, jsonStr, tempArena);
    Assert(result);

    // Then we have a data model with
    // - 1 texture
    // - 2 materials
    // - 1 mesh
    // - 1 entity
    // - camera at 1, 2, 3
    // - backgroundMaterialId set to HashStringU32("materials/background")
    Assert(dataModel.textureCount == 2);
    Assert(strcmp(dataModel.textures[0].path, "kiara_4_mid-morning_4k.exr") == 0);
    AssertVec4Within(EPSILON, Vec4(1), dataModel.textures[1].singleColor);
    Assert(dataModel.materialCount == 2);
    Assert(dataModel.meshCount == 2);
    Assert(strcmp(dataModel.meshes[0].path, "bunny.obj") == 0);
    Assert(dataModel.meshes[1].generator == GeneratedMesh_Cube);
    Assert(dataModel.entityCount == 1);
    Assert(Length(Vec3(0, 0, 1) - dataModel.camera.position) <= EPSILON);
    Assert(dataModel.backgroundMaterialId == HashStringU32("materials/background"));

    // Render it!
    AssetSystem assetSystem = {};
    AssetSystemConfig assetSystemConfig = {};
    assetSystemConfig.maxTextures = 4;
    assetSystemConfig.maxMeshes = 2;
    assetSystemConfig.imageDataArenaSize = Megabytes(256);
    assetSystemConfig.meshDataArenaSize = Megabytes(64);
    InitializeAssetSystem(
        &assetSystem, applicationMemoryArena, assetSystemConfig);

    PopulateAssetSystem(&assetSystem, &dataModel);

    PopulateAssetSystem(&assetSystem, &dataModel);
    AssetSystemGenerator generator = {};
    generator.singleColor = GenerateSingleColorTexture;
    generator.checkerboard = GenerateCheckerboardTexture;
    generator.mesh = GenerateMesh;
    GenerateAssetData(&assetSystem, generator, NULL);

    AssetSystemLoader assetLoader = {};
    assetLoader.readTextureFromDisk = LoadTextureFromDisk;
    assetLoader.readMeshFromDisk = LoadMeshFromDisk;

    AssetSystemLoaderUserData assetLoaderUserData = {};
    assetLoaderUserData.assetDir = assetDir;
    LoadAssetsFromDisk(&assetSystem, assetLoader, &assetLoaderUserData);

    // Build path tracer scene
    sp_Scene pathTracerScene = {};
    sp_InitializeScene(&pathTracerScene, applicationMemoryArena);
    sp_MaterialSystem materialSystem = {};
    sp_LightSystem lightSystem = {};
    sp_Camera camera = {};
    sp_Context context = {};
    context.materialSystem = &materialSystem;
    context.lightSystem = &lightSystem;
    context.camera = &camera;
    context.scene = &pathTracerScene;
    context.sampleCount = 32;

    HdrImage pathTracerOutput = AllocateImage(1024 / 16, 1024 / 16, tempArena);

    Dictionary pathTracerMeshStorage = Dict_CreateFromArena(meshDataArena, sp_Mesh, MAX_MESHES);
    SyncPathTracerWithDataModel(&dataModel, meshDataArena,
        accelerationStructureMemoryArena, tempArena, &pathTracerMeshStorage, &context,
        &pathTracerOutput, &assetSystem);

    // Build rasterizer scene struct
    Scene scene = {};
    // TODO: Mesh aabbs
    scene.entities = AllocateArray(entityMemoryArena, Entity, MAX_ENTITIES);
    scene.max = MAX_ENTITIES;
    scene.lightData = (LightData *)renderer->lightBuffer.data;
    SyncRasterizerWithDataModel(renderer, &dataModel,
        &assetSystem.imageDataArena, &scene, &assetSystem);

    //4. Render (just path tracer for now)
    sp_BuildSceneBroadphase(&pathTracerScene);

    Tile tile = {0, 0, pathTracerOutput.width, pathTracerOutput.height};
    RandomNumberGenerator rng = {};
    rng.state = 2304580;
    sp_Metrics metrics = {};

    sp_PathTraceTile(&context, tile, &rng, &metrics);

    //5. Check that path tracer has both colors
    if (!stbi_write_hdr("json_path_tracer.hdr", pathTracerOutput.width,
            pathTracerOutput.height, 4, pathTracerOutput.pixels[0].data))
    {
        LogMessage("Failed to write out json_path_tracer.hdr");
    }

    //6. Check that rasterizer has both colors
    u32 outputFlags = Output_None;
    HdrImage rasterizerOutput = AllocateImage(
        renderer->swapchain.width, renderer->swapchain.height, tempArena);
    VulkanRender(renderer, outputFlags, scene, &rasterizerOutput);

    if (!stbi_write_hdr("json_rasterizer.hdr", rasterizerOutput.width,
            rasterizerOutput.height, 4, rasterizerOutput.pixels[0].data))
    {
        LogMessage("Failed to write out json_rasterizer.hdr");
    }
}

inline AssetSystem CreateAssetSystemForIntegrationTests(MemoryArena *memoryArena)
{
    AssetSystemConfig assetSystemConfig = {};
    assetSystemConfig.maxTextures = 4;
    assetSystemConfig.maxMeshes = 2;
    assetSystemConfig.imageDataArenaSize = Kilobytes(1);
    assetSystemConfig.meshDataArenaSize = Kilobytes(512);

    AssetSystem assetSystem = {};
    InitializeAssetSystem(
        &assetSystem, memoryArena, assetSystemConfig);

    return assetSystem;
}

inline void WriteHdrImageToDisk(const char *path, HdrImage image)
{
    if (!stbi_write_hdr(
            path, image.width, image.height, 4, image.pixels[0].data))
    {
        LogMessage("Failed to write out %s", path);
    }
}

inline f32 Luminance(vec3 linearColor)
{
    f32 result = 0.2126f * linearColor.r + 0.7152f * linearColor.g +
                 0.0722f * linearColor.b;
    return result;
}

internal void TestDirectionalLight(MemoryArena *imageDataArena,
        MemoryArena *meshDataArena,
        MemoryArena *accelerationStructureMemoryArena,
        MemoryArena *tempArena,
        MemoryArena *entityMemoryArena,
        MemoryArena *applicationMemoryArena,
        VulkanRenderer *renderer)
{
    DataModel dataModel = {};
    AssetSystem assetSystem =
        CreateAssetSystemForIntegrationTests(applicationMemoryArena);

    // 1. Create single color texture
    u32 textureId = HashStringU32("textures/singlecolor");
    RegisterTexture(&dataModel, textureId, Vec4(1));

    u32 backgroundTextureId = HashStringU32("textures/background");
    RegisterTexture(&dataModel, backgroundTextureId, Vec4(0, 0, 0, 1));

    u32 materialId = HashStringU32("materials/singlecolor");
    DataModelMaterial material = {};
    material.albedoTexture = textureId;
    RegisterMaterial(&dataModel, materialId, material);

    u32 backgroundMaterialId = HashStringU32("materials/background");
    DataModelMaterial backgroundMaterial = {};
    backgroundMaterial.emissionTexture = backgroundTextureId;
    RegisterMaterial(&dataModel, backgroundMaterialId, backgroundMaterial);
    dataModel.backgroundMaterialId = backgroundMaterialId;

    // 2. Create sphere mesh
    u32 cubeMeshId = Mesh_Cube;
    RegisterMesh(&dataModel, cubeMeshId, GeneratedMesh_Cube);

    u32 sphereMeshId = HashStringU32("meshes/sphere");
    RegisterMesh(&dataModel, sphereMeshId, GeneratedMesh_Sphere);

    AddEntity(&dataModel, HashStringU32("scene/sphere"), sphereMeshId,
        materialId, Vec3(0, 0, 0), Quat(), Vec3(1));

    // 4. Create directionl light pointing left
    AddDirectionalLight(
        &dataModel, HashStringU32("scene/light"), Vec3(1), Vec3(-1, 0, 0));

    // 4. Position camera to see both meshes
    ConfigureCamera(&dataModel, Vec3(0, 0, 2), Quat());

    // 4.5 Populate asset system from data model and generate asset data
    PopulateAssetSystem(&assetSystem, &dataModel);

    AssetSystemGenerator generator = {};
    generator.singleColor = GenerateSingleColorTexture;
    generator.mesh = GenerateMesh;
    GenerateAssetData(&assetSystem, generator, imageDataArena);

    // 5. Render scene with directional light to get shadow?
    // Build path tracer scene
    sp_Scene pathTracerScene = {};
    sp_InitializeScene(&pathTracerScene, applicationMemoryArena);

    sp_MaterialSystem materialSystem = {};
    sp_LightSystem lightSystem = {};
    sp_Camera camera = {};
    sp_Context context = {};
    context.materialSystem = &materialSystem;
    context.lightSystem = &lightSystem;
    context.camera = &camera;
    context.scene = &pathTracerScene;
    context.sampleCount = 32;
    context.enableDirectionalLights = true;

    HdrImage pathTracerOutput = AllocateImage(1024 / 16, 1024 / 16, tempArena);

    // TODO: This should be part of context?
    Dictionary pathTracerMeshStorage =
        Dict_CreateFromArena(meshDataArena, sp_Mesh, MAX_MESHES);
    SyncPathTracerWithDataModel(&dataModel, meshDataArena,
        accelerationStructureMemoryArena, tempArena, &pathTracerMeshStorage,
        &context, &pathTracerOutput, &assetSystem);

    // Build rasterizer scene struct
    Scene scene = {};
    // TODO: Mesh aabbs
    scene.entities = AllocateArray(entityMemoryArena, Entity, MAX_ENTITIES);
    scene.max = MAX_ENTITIES;
    scene.lightData = (LightData *)renderer->lightBuffer.data;
    SyncRasterizerWithDataModel(renderer, &dataModel, imageDataArena, &scene, &assetSystem);

    //4. Render
    sp_BuildSceneBroadphase(&pathTracerScene);

    Tile tile = {0, 0, pathTracerOutput.width, pathTracerOutput.height};
    RandomNumberGenerator rng = {};
    rng.state = 1;
    sp_Metrics metrics = {};

    sp_PathTraceTile(&context, tile, &rng, &metrics);

    //5. Check that path tracer has both colors
    WriteHdrImageToDisk(
        "directional_light_path_tracer.hdr", pathTracerOutput);

    //6. Check that rasterizer has both colors
    u32 outputFlags = Output_None;
    HdrImage rasterizerOutput = AllocateImage(
        renderer->swapchain.width, renderer->swapchain.height, tempArena);
    VulkanRender(renderer, outputFlags, scene, &rasterizerOutput);

    WriteHdrImageToDisk("directional_light_rasterizer.hdr", rasterizerOutput);

    // 7. Check that pixels on right side of sphere are white and pixels on
    // left side are black
    vec2 p0 = Vec2(0.9, 0.5);
    vec2 p1 = Vec2(0.1, 0.5);

    vec4 c0 = SampleImageNearest(pathTracerOutput, p0);
    vec4 c1 = SampleImageNearest(pathTracerOutput, p1);

    f32 l0 = Luminance(c0.xyz);
    f32 l1 = Luminance(c1.xyz);

    if (l0 < 0.1f)
    {
        LogMessage("=== PATH TRACER TEST FAILED ===");
        LogMessage("\tExpected right side of sphere to have luminance greater "
                   "than 0.1 but was %g. - Color was [%g, %g, %g, %g].",
            l0, c0.r, c0.g, c0.b, c0.a);
    }

    if (l1 > 0.01f)
    {
        LogMessage("=== PATH TRACER TEST FAILED ===");
        LogMessage("\tExpected left side of sphere to have luminance less "
                   "than 0.01 but was %g. - Color was [%g, %g, %g, %g].",
            l1, c1.r, c1.g, c1.b, c1.a);
    }

    vec4 c2 = SampleImageNearest(rasterizerOutput, p0);
    vec4 c3 = SampleImageNearest(rasterizerOutput, p1);

    f32 l2 = Luminance(c2.xyz);
    f32 l3 = Luminance(c3.xyz);

    if (l2 < 0.1f)
    {
        LogMessage("=== RASTERIZER TEST FAILED ===");
        LogMessage("\tExpected right side of sphere to have luminance greater "
                   "than 0.1 but was %g. - Color was [%g, %g, %g, %g].",
            l2, c2.r, c2.g, c2.b, c2.a);
    }

    if (l3 > 0.01f)
    {
        LogMessage("=== RASTERIZER TEST FAILED ===");
        LogMessage("\tExpected left side of sphere to have luminance less "
                   "than 0.01 but was %g. - Color was [%g, %g, %g, %g].",
            l3, c3.r, c3.g, c3.b, c3.a);
    }
}

inline void ExpectFloatEqual(f32 epsilon, f32 expected, f32 actual)
{
    if (Abs(expected - actual) > epsilon)
    {
        LogMessage("=== TEST FAILED ===");
        LogMessage("Expected %g but was %g", expected, actual);
    }
}

internal b32 ParseCommandLineArgs(
    int argc, char **argv, CommandLineArgs *args)
{
    arg_file_t *assetDir = arg_filen(NULL, "asset-dir", "FILE", 0, 1, NULL);
    arg_file_t *inputJsonFile = arg_filen(NULL, "input-json-file", "FILE", 0, 1, NULL);
    arg_lit_t *singleFrameMode = arg_litn(NULL, "single-frame-mode", 0, 1, "Run tests and output result to file");
    arg_lit_t *runIntegrationTests = arg_litn(NULL, "run-integration-tests", 0, 1, "Run integration tests");
    arg_int_t *sampleCount = arg_intn("s", "sample-count", "<n>", 0, 1, "Sample count to use for CPU Path tracer");
    arg_end_t *end = arg_end(20);
    void *argtable[] = {
        assetDir,
        singleFrameMode,
        runIntegrationTests,
        inputJsonFile,
        sampleCount,
        end,
    };

    i32 errorCount = arg_parse(argc, argv, argtable);
    if (errorCount > 0)
    {
        arg_print_errors(stdout, end, "vk_cinematic");
        arg_freetable(argtable, ArrayCount(argtable));
        return false;
    }

    if (singleFrameMode->count > 0)
    {
        args->singleFrameMode = true;
    }

    args->runIntegrationTests = runIntegrationTests->count > 0;

    if (assetDir->count > 0)
    {
        args->assetDir = assetDir->filename[0];
    }

    if (inputJsonFile->count > 0)
    {
        args->inputJsonFile = inputJsonFile->filename[0];
    }

    if (sampleCount->count > 0)
    {
        args->sampleCount = sampleCount->ival[0];
    }

    arg_freetable(argtable, ArrayCount(argtable));

    return true;
}

internal void UseDefaultScene(DataModel *dataModel)
{
    // Load background HDRI
    u32 backgroundTextureId = HashStringU32("textures/hdri");
    RegisterTexture(
        dataModel, backgroundTextureId, "kiara_4_mid-morning_4k.exr");
    //RegisterTexture(dataModel, backgroundTextureId, Vec4(0, 0, 0, 1));

    // Create checkerboard image
    u32 checkerboardTextureId = HashStringU32("textures/checkerboard");
    RegisterTextureCheckerboard(dataModel, checkerboardTextureId);

    u32 whiteTextureId = HashStringU32("textures/white");
    RegisterTexture(dataModel, whiteTextureId, Vec4(1));

    u32 whiteMaterialId = HashStringU32("materials/white");
    DataModelMaterial whiteMaterial = {};
    whiteMaterial.albedoTexture = whiteTextureId;
    RegisterMaterial(dataModel, whiteMaterialId, whiteMaterial);

    u32 checkerBoardMaterialId = HashStringU32("materials/checkerboard");
    DataModelMaterial checkerBoardMaterial = {};
    checkerBoardMaterial.albedoTexture = checkerboardTextureId;
    RegisterMaterial(dataModel, checkerBoardMaterialId, checkerBoardMaterial);

    u32 backgroundMaterialId = HashStringU32("materials/background");
    DataModelMaterial backgroundMaterial = {};
    backgroundMaterial.emissionTexture = backgroundTextureId;
    RegisterMaterial(dataModel, backgroundMaterialId, backgroundMaterial);
    dataModel->backgroundMaterialId = backgroundMaterialId;

    // Create meshes
    u32 cubeMeshId = Mesh_Cube;
    RegisterMesh(dataModel, cubeMeshId, GeneratedMesh_Cube);

    u32 bunnyMeshId = HashStringU32("meshes/bunny");
    RegisterMesh(dataModel, bunnyMeshId, "bunny.obj");

    u32 planeMeshId = HashStringU32("meshes/plane");
    RegisterMesh(dataModel, planeMeshId, GeneratedMesh_Plane);

    // Create scene
    AddEntity(dataModel, HashStringU32("scene/bunny"), bunnyMeshId,
        whiteMaterialId, Vec3(0, -0.02, 0), Quat(), Vec3(1));
    AddEntity(dataModel, HashStringU32("scene/plane"), planeMeshId,
        checkerBoardMaterialId, Vec3(0, 0.02, 0),
        Quat(Vec3(1, 0, 0), PI * -0.5f), Vec3(1));

    ConfigureCamera(dataModel, Vec3(0, 0.1, 0.3), Quat());

    AddDirectionalLight(dataModel, HashStringU32("scene/sun"), Vec3(15, 12, 12),
        Normalize(Vec3(1, -3, 1)));
}

internal void SetCommandLineArgsToDefaultValues(CommandLineArgs *args)
{
    args->assetDir = DEFAULT_ASSET_DIR;
    args->singleFrameMode = false;
    args->runIntegrationTests = false;
    args->sampleCount = DEFAULT_SAMPLE_COUNT;
}

int main(int argc, char **argv)
{
    LogMessage = &LogMessage_;

    // Parse command line args
    CommandLineArgs args = {};
    SetCommandLineArgsToDefaultValues(&args);
    if (!ParseCommandLineArgs(argc, argv, &args))
    {
        LogMessage("Failed to parse command line arguments");
        return 1;
    }
    LogMessage("Asset directoy set to %s", args.assetDir);
    LogMessage("Sample count: %u", args.sampleCount);
    LogMessage("Running in single frame mode: %s",
        args.singleFrameMode ? "true" : "false");

    LogMessage("Compiled agist GLFW %i.%i.%i", GLFW_VERSION_MAJOR,
           GLFW_VERSION_MINOR, GLFW_VERSION_REVISION);

    i32 major, minor, revision;
    glfwGetVersion(&major, &minor, &revision);
    LogMessage("Running against GLFW %i.%i.%i", major, minor, revision);
    LogMessage("%s", glfwGetVersionString());

    glfwSetErrorCallback(GlfwErrorCallback);
    if (!glfwInit())
    {
        LogMessage("Failed to initialize GLFW!");
        return -1;
    }

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    g_Window = glfwCreateWindow(g_FramebufferWidth,
        g_FramebufferHeight, "vk_cinematic", NULL, NULL);
    Assert(g_Window != NULL);
    GameInput input = {};
    glfwSetWindowUserPointer(g_Window, &input);
    glfwSetKeyCallback(g_Window, KeyCallback);
    glfwSetMouseButtonCallback(g_Window, MouseButtonCallback);
    glfwSetCursorPosCallback(g_Window, CursorPositionCallback);

    // Create memory arenas
    u32 applicationMemorySize = APPLICATION_MEMORY_LIMIT;
    MemoryArena applicationMemoryArena = {};
    InitializeMemoryArena(&applicationMemoryArena,
        AllocateMemory(applicationMemorySize), applicationMemorySize);

    u32 workQueueArenaSize = Kilobytes(32);
    MemoryArena workQueueArena =
        SubAllocateArena(&applicationMemoryArena, workQueueArenaSize);

    u32 tempMemorySize = Megabytes(64); // TODO: Config option
    MemoryArena tempArena =
        SubAllocateArena(&applicationMemoryArena, tempMemorySize);

    u32 meshDataMemorySize = Megabytes(4); // TODO: Config option
    MemoryArena meshDataArena =
        SubAllocateArena(&applicationMemoryArena, meshDataMemorySize);

    u32 accelerationStructureMemorySize = Megabytes(64); // TODO: Config option
    MemoryArena accelerationStructureMemoryArena = SubAllocateArena(
        &applicationMemoryArena, accelerationStructureMemorySize);

    u32 entityMemorySize = Megabytes(4); // TODO: Config option
    MemoryArena entityMemoryArena =
        SubAllocateArena(&applicationMemoryArena, entityMemorySize);

    u32 imageDataMemorySize = Megabytes(256); // TODO: Config option
    MemoryArena imageDataArena =
        SubAllocateArena(&applicationMemoryArena, imageDataMemorySize);

    LogMessage("Application memory usage: %uk / %uk",
        applicationMemoryArena.size / 1024,
        applicationMemoryArena.capacity / 1024);

    // Create Vulkan Renderer
    VulkanRenderer renderer = {};
    VulkanInit(&renderer, g_Window);

    if (args.runIntegrationTests)
    {
        TestDirectionalLight(&imageDataArena, &meshDataArena,
            &accelerationStructureMemoryArena, &tempArena, &entityMemoryArena,
            &applicationMemoryArena, &renderer);
        VulkanClearResources(&renderer);

        TestLoadingDataModelFromJson(&applicationMemoryArena, args.assetDir,
            &tempArena, &meshDataArena, &accelerationStructureMemoryArena,
            &entityMemoryArena, &renderer);
        VulkanClearResources(&renderer);

        TestIntegrationOverHemisphere();

        TestMultipleMaterials(&imageDataArena, &meshDataArena,
            &accelerationStructureMemoryArena, &tempArena, &entityMemoryArena,
            &applicationMemoryArena, &renderer);
        VulkanClearResources(&renderer);

        TestSingleMaterial(&imageDataArena, &meshDataArena,
            &accelerationStructureMemoryArena, &tempArena, &entityMemoryArena,
            &applicationMemoryArena, &renderer);
        return 0;
    }

    DataModel dataModel = {};
    AssetSystem assetSystem = {};
    AssetSystemConfig assetSystemConfig = {};
    assetSystemConfig.maxTextures = 8;
    assetSystemConfig.maxMeshes = 8;
    assetSystemConfig.imageDataArenaSize = Megabytes(512);
    assetSystemConfig.meshDataArenaSize = Megabytes(64);
    InitializeAssetSystem(
        &assetSystem, &applicationMemoryArena, assetSystemConfig);

    if (args.inputJsonFile != NULL)
    {
        // Load json file from disk
        DebugReadFileResult result = ReadEntireFile(args.inputJsonFile);
        if (!DataModelFromJson(&dataModel, (const char*)result.contents, &tempArena))
        {
            LogMessage("Error while parsing JSON");
        }

        // Parse it
        FreeMemory(result.contents);
    }
    else
    {
        UseDefaultScene(&dataModel);
    }

    // Create SIMD Path tracer
    sp_Context context = {};
    sp_MaterialSystem materialSystem = {};
    sp_LightSystem lightSystem = {};
    context.materialSystem = &materialSystem;
    context.lightSystem = &lightSystem;
    context.sampleCount = args.sampleCount;
    context.enableDirectionalLights = true;
    context.enablePointLights = true;

    g_camera.position = dataModel.camera.position;

    Scene scene = {};
    // TODO: Mesh aabbs
    scene.entities = AllocateArray(&entityMemoryArena, Entity, MAX_ENTITIES);
    scene.max = MAX_ENTITIES;
    scene.lightData = (LightData *)renderer.lightBuffer.data;

    g_Profiler.samples =
        (ProfilerSample *)AllocateMemory(PROFILER_SAMPLE_BUFFER_SIZE);
    ProfilerResults profilerResults = {};

    DebugDrawingBuffer debugDrawBuffer = {};
    debugDrawBuffer.vertices = (VertexPC *)renderer.debugVertexDataBuffer.data;
    debugDrawBuffer.max = DEBUG_VERTEX_BUFFER_SIZE / sizeof(VertexPC);

    HdrImage pathTracerOutput = {};
    pathTracerOutput.pixels = (vec4 *)renderer.imageUploadBuffer.data;
    pathTracerOutput.width = RAY_TRACER_WIDTH;
    pathTracerOutput.height = RAY_TRACER_HEIGHT;

    sp_Camera camera = {};
    sp_ConfigureCamera(&camera, &pathTracerOutput, Vec3(0, 0, 1), Quat(), 0.3f);
    context.camera = &camera;

    sp_Scene pathTracerScene = {};
    sp_InitializeScene(&pathTracerScene, &applicationMemoryArena);
    context.scene = &pathTracerScene;

    // FIXME: This should be part of sp_Scene!
    Dictionary pathTracerMeshStorage =
        Dict_CreateFromArena(&meshDataArena, sp_Mesh, MAX_MESHES);

    materialSystem.backgroundMaterialId = HashStringU32("materials/hdri");

    // FIXME: Too scard to run this in the while loop
    PopulateAssetSystem(&assetSystem, &dataModel);
    AssetSystemGenerator generator = {};
    generator.singleColor = GenerateSingleColorTexture;
    generator.checkerboard = GenerateCheckerboardTexture;
    generator.mesh = GenerateMesh;
    GenerateAssetData(&assetSystem, generator, &imageDataArena);

    AssetSystemLoader assetLoader = {};
    assetLoader.readTextureFromDisk = LoadTextureFromDisk;
    assetLoader.readMeshFromDisk = LoadMeshFromDisk;

    AssetSystemLoaderUserData assetLoaderUserData = {};
    assetLoaderUserData.assetDir = args.assetDir;
    LoadAssetsFromDisk(&assetSystem, assetLoader, &assetLoaderUserData);

    // For image dumping
    HdrImage rasterizerOutput = AllocateImage(
        renderer.swapchain.width, renderer.swapchain.height, &tempArena);

    WorkQueue workQueue =
        CreateWorkQueue(&workQueueArena, sizeof(sp_Task), 1024);
    ThreadPool threadPool = CreateThreadPool(&workQueue);

    LogMessage("Start up time: %gs", glfwGetTime());

    vec3 lastCameraPosition = g_camera.position;
    vec3 lastCameraRotation = g_camera.rotation;
    f32 prevFrameTime = 0.0f;
    u32 maxDepth = 1;
    b32 drawTests = false;
    b32 isRayTracing = false;
    b32 showComparision = false;
    b32 showDebugDrawing = true;
    f32 t = 0.0f;
    f64 rayTracingStartTime = 0.0;
    f64 nextStatPrintTime = 0.0;
    b32 showComputeShaderOutput = false;
    b32 runPathTracingComputeShader = false;
    while (!glfwWindowShouldClose(g_Window))
    {
        f32 dt = prevFrameTime;
        dt = Min(dt, 0.25f);
        if (isRayTracing)
        {
            dt = 0.016f;
        }

        debugDrawBuffer.count = 0;


        f64 frameStart = glfwGetTime();
        InputBeginFrame(&input);
        glfwPollEvents();

        // Handle window resize
        i32 framebufferWidth, framebufferHeight;
        glfwGetFramebufferSize(g_Window, &framebufferWidth, &framebufferHeight);
        if ((u32)framebufferWidth != g_FramebufferWidth ||
            (u32)framebufferHeight != g_FramebufferHeight)
        {
            LogMessage("Framebuffer resized to %d x %d", framebufferWidth,
                framebufferHeight);

            // Recreate vulkan swapchain
            VulkanFramebufferResize(
                &renderer, framebufferWidth, framebufferHeight);

            g_FramebufferWidth = framebufferWidth;
            g_FramebufferHeight = framebufferHeight;
        }


        if (isRayTracing)
        {
            // Only print metrics while we have path tracing work remaining
            if (workQueue.head != workQueue.tail)
            {
                if (glfwGetTime() > nextStatPrintTime)
                {
                    // Not sure how scalable this approach is of adding metrics
                    // for each tile to a queue. Decided to go with this
                    // approach rather than create a new global metric
                    // structure that stored atomic counters for each metric
                    // value which then each thread accumulates its local
                    // metric values onto.
                    sp_Metrics total = {};
                    for (i32 i = 0; i < g_metricsBufferLength; i++)
                    {
                        for (u32 j = 0; j < SP_MAX_METRICS; ++j)
                        {
                            total.values[j] += g_metricsBuffer[i].values[j];
                        }
                    }

                    u32 tilesProcessed = g_metricsBufferLength;
                    u32 totalNumberOfFiles = workQueue.tail;

                    LogMessage("Processed %u/%u tiles - %g%% complete",
                        tilesProcessed, totalNumberOfFiles,
                        ((f32)tilesProcessed / (f32)totalNumberOfFiles) *
                            100.0f);
                    LogMessage("Cycles elapsed: %llu",
                        total.values[sp_Metric_CyclesElapsed]);
                    LogMessage("Paths traced: %llu",
                        total.values[sp_Metric_PathsTraced]);
                    LogMessage("Rays traced: %llu",
                        total.values[sp_Metric_RaysTraced]);
                    LogMessage(
                        "Ray hits: %llu", total.values[sp_Metric_RayHitCount]);
                    LogMessage("Ray misses: %llu",
                        total.values[sp_Metric_RayMissCount]);
                    LogMessage("RayIntersectScene cycles elapsed: %llu",
                        total
                            .values[sp_Metric_CyclesElapsed_RayIntersectScene]);
                    LogMessage("RayIntersectBroadphase cycles elapsed: %llu",
                        total.values
                            [sp_Metric_CyclesElapsed_RayIntersectBroadphase]);
                    LogMessage("RayIntersectMesh cycles elapsed: %llu",
                        total.values[sp_Metric_CyclesElapsed_RayIntersectMesh]);
                    LogMessage("RayIntersectMesh Midphase cycles elapsed: %llu",
                        total.values
                            [sp_Metric_CyclesElapsed_RayIntersectMeshMidphase]);
                    LogMessage("RayIntersectTriangle cycles elapsed: %llu",
                        total.values
                            [sp_Metric_CyclesElapsed_RayIntersectTriangle]);
                    LogMessage(
                        "RayIntersectMesh Midphase AABB tests performed: %llu",
                        total.values
                            [sp_Metric_RayIntersectMesh_MidphaseAabbTestCount]);
                    LogMessage("RayIntersectMesh tests performed: %llu",
                        total
                            .values[sp_Metric_RayIntersectMesh_TestsPerformed]);

                    f64 secondsElapsed = glfwGetTime() - rayTracingStartTime;
                    LogMessage("Seconds elapsed: %g", secondsElapsed);
                    LogMessage("Paths traced per second: %g",
                        (f64)total.values[sp_Metric_PathsTraced] /
                            secondsElapsed);
                    LogMessage("Rays per second: %g",
                        (f64)total.values[sp_Metric_RaysTraced] /
                            secondsElapsed);
                    LogMessage("Average cycles per ray: %g",
                        (f64)total.values[sp_Metric_CyclesElapsed] /
                            (f64)total.values[sp_Metric_RaysTraced]);

                    // TODO: Expose constant, currently prints stats 1 per
                    // second
                    nextStatPrintTime = glfwGetTime() + 1.0;
                }
            }
        }

        if (WasPressed(input.buttonStates[KEY_SPACE]))
        {
            if (!isRayTracing)
            {
                // Only allow submitting new work to the queue if it is empty
                if (workQueue.head == workQueue.tail)
                {
                    isRayTracing = true;
                    ClearImage(&pathTracerOutput);

                    // TODO: Switch to immediate mode API style
                    pathTracerScene.objectCount = 0;
                    ResetMemoryArena(&pathTracerScene.memoryArena);

                    SyncPathTracerWithDataModel(&dataModel,
                        &meshDataArena, &accelerationStructureMemoryArena,
                        &tempArena, &pathTracerMeshStorage, &context,
                        &pathTracerOutput, &assetSystem);

                    sp_BuildSceneBroadphase(&pathTracerScene);

                    AddRayTracingWorkQueue(&workQueue, &context);
                    rayTracingStartTime = glfwGetTime();
                }
            }
            else
            {
                isRayTracing = false;
            }
        }

        if (LengthSq(g_camera.position - lastCameraPosition) > 0.0001f ||
            LengthSq(g_camera.rotation - lastCameraRotation) > 0.0001f)
        {
            lastCameraPosition = g_camera.position;
            lastCameraRotation = g_camera.rotation;
        }

        if (WasPressed(input.buttonStates[KEY_UP]))
        {
            maxDepth++;
        }
        if (WasPressed(input.buttonStates[KEY_DOWN]))
        {
            if (maxDepth > 0)
            {
                maxDepth--;
            }
        }

        if (WasPressed(input.buttonStates[KEY_F1]))
        {
            showComparision = !showComparision;
        }

        if (WasPressed(input.buttonStates[KEY_F2]))
        {
            showDebugDrawing = !showDebugDrawing;
        }

#if FEAT_ENABLE_GPU_PATH_TRACING
        if (WasPressed(input.buttonStates[KEY_F3]))
        {
            showComputeShaderOutput = !showComputeShaderOutput;
        }

        if (WasPressed(input.buttonStates[KEY_P]))
        {
            runPathTracingComputeShader = true;
            showComputeShaderOutput = true;
            VulkanUploadComputeTileQueue(&renderer);
        }
#endif

        VulkanCopyImageFromCPU(&renderer);
        
#if DRAW_ENTITY_AABBS
        DrawEntityAabbs(scene, &debugDrawBuffer);
        DrawSceneAabb(scene, &debugDrawBuffer);
        DrawShadowProjectionMatrix(&renderer, scene, &debugDrawBuffer);
        DrawPointLightVisualizations(&dataModel, &debugDrawBuffer);
        DrawDirectionalLightVisualization(&dataModel, &debugDrawBuffer);
#endif

        // Move camera around
        Update(&dataModel, &input, dt);

        // FIXME: This won't work
        SyncRasterizerWithDataModel(
            &renderer, &dataModel, &imageDataArena, &scene, &assetSystem);

        // TODO: Don't do this here
        UniformBufferObject *ubo =
            (UniformBufferObject *)renderer.uniformBuffer.data;

        if (isRayTracing)
        {
            ubo->showComparision = 1;
            if (showComparision)
                ubo->showComparision = 2;
        }
        else
        {
            ubo->showComparision = 0;
        }

        if (showComputeShaderOutput)
        {
            ubo->showComparision = 3;
        }

        renderer.debugDrawVertexCount = debugDrawBuffer.count;

        u32 outputFlags = Output_None;
        if (showDebugDrawing)
        {
            outputFlags |= Output_ShowDebugDrawing;
        }

#if FEAT_ENABLE_GPU_PATH_TRACING
        if (runPathTracingComputeShader)
        {
            outputFlags |= Output_RunPathTracingComputeShader;
        }
#endif
        if (WasPressed(input.buttonStates[KEY_F5]) || args.singleFrameMode)
        {
            if (!stbi_write_hdr("cont_path_tracer.hdr", pathTracerOutput.width,
                    pathTracerOutput.height, 4,
                    pathTracerOutput.pixels[0].data))
            {
                LogMessage("Failed to write out cont_path_tracer.hdr");
            }

            VulkanRender(&renderer, outputFlags, scene, &rasterizerOutput);

            if (!stbi_write_hdr("cont_rasterizer.hdr", rasterizerOutput.width,
                    rasterizerOutput.height, 4,
                    rasterizerOutput.pixels[0].data))
            {
                LogMessage("Failed to write out cont_rasterizer.hdr");
            }

            if (args.singleFrameMode)
            {
                return 0; // TODO: Should rather just break out of loop
            }
        }
        else
        {
            VulkanRender(&renderer, outputFlags, scene);
        }

        prevFrameTime = (f32)(glfwGetTime() - frameStart);
    }
    return 0;
}
