#pragma once

struct sp_DirectionalLight
{
    vec3 radiance;
    vec3 direction;
    u32 id;
};

struct sp_PointLight
{
    vec3 radiance;
    vec3 position;
    u32 id;
};

#define SP_MAX_DIRECTIONAL_LIGHTS 8
#define SP_MAX_POINT_LIGHTS 8

struct sp_LightSystem
{
    sp_DirectionalLight directionalLights[SP_MAX_DIRECTIONAL_LIGHTS];
    u32 directionalLightCount;

    sp_PointLight pointLights[SP_MAX_POINT_LIGHTS];
    u32 pointLightCount;
};
