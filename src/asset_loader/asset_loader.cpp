#define TINYEXR_IMPLEMENTATION
#define TINYEXR_USE_OPENMP 0
#pragma warning(push)
#pragma warning(disable : 4018)
#pragma warning(disable : 4389)
#pragma warning(disable : 4706)
#include "tinyexr.h"
#pragma warning(pop)

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "asset_loader.h"

static int LoadExrImage(HdrImageData *image, const char *path)
{
    int result = 0;

    float *out;
    int width;
    int height;
    const char *err = NULL;

    int ret = LoadEXR(&out, &width, &height, path, &err);

    if (ret != TINYEXR_SUCCESS)
    {
        if (err)
        {
            // TODO: Return error message
            fprintf(stderr, "ERR : %s\n", err);
            FreeEXRErrorMessage(err); // release memory of error message.
        }
        result = 1;
    }
    else
    {
        image->pixels = out;
        image->width = width;
        image->height = height;
        result = 0;
    }

    return result;
}

static int LoadImageWithSTB(HdrImageData *image, const char *path)
{
    int result = 1;
    int x, y, n;
    float *data = stbi_loadf(path, &x, &y, &n, 4);
    if (data != NULL)
    {
        image->width = (uint32_t)x;
        image->height = (uint32_t)y;
        image->pixels = (float*)malloc(x * y * sizeof(float) * 4);
        memcpy(image->pixels, data, x * y * sizeof(float) * 4);

        stbi_image_free(data);
        result = 0;
    }

    return result;
}

int LoadHdrImage(HdrImageData *image, const char *path)
{
    int result = 0;

    // Guess which loader to use based on file extension for now
    uint32_t pathLength = (uint32_t)strlen(path);
    const char *ext = path + (pathLength - 4);

    if (strcmp(ext, ".exr") == 0)
    {
        result = LoadExrImage(image, path);
    }
    else
    {
        // Try stb
        result = LoadImageWithSTB(image, path);
    }

    return result;
}

void FreeHdrImage(HdrImageData image)
{
    free(image.pixels);
}
