#include "vulkan_renderer.h"

#include "vulkan_utils.cpp"

#define VALIDATION_LAYER_NAME "VK_LAYER_KHRONOS_validation"

internal VulkanImage *VulkanFindTextureById(VulkanRenderer *renderer, u32 id)
{
    VulkanImage *texture = Dict_FindItem(&renderer->textures, VulkanImage, id);
    return texture;
}

internal b32 StoreVulkanTexture(VulkanRenderer *renderer,
        u32 id, VulkanImage texture)
{
    VulkanImage *dst = Dict_AddItem(&renderer->textures, VulkanImage, id);
    b32 result = false;
    if (dst != NULL)
    {
        *dst = texture;
        result = true;
    }

    return result;
}

internal VulkanMaterial *VulkanFindMaterialById(VulkanRenderer *renderer, u32 id)
{
    VulkanMaterial *material = Dict_FindItem(&renderer->materials, VulkanMaterial, id);
    return material;
}

internal b32 StoreVulkanMaterial(VulkanRenderer *renderer,
        u32 id, VulkanMaterial material)
{
    b32 result = false;
    VulkanMaterial *dst = Dict_AddItem(&renderer->materials, VulkanMaterial, id);
    if (dst != NULL)
    {
        *dst = material;
        result = true;
    }

    return result;
}

internal Mesh *VulkanFindMeshById(VulkanRenderer *renderer, u32 id)
{
    Mesh *mesh = Dict_FindItem(&renderer->meshes, Mesh, id);
    return mesh;
}

internal b32 StoreVulkanMesh(VulkanRenderer *renderer, u32 id, Mesh mesh)
{
    b32 result = false;
    Mesh *dst = Dict_AddItem(&renderer->meshes, Mesh, id);
    if (dst != NULL)
    {
        *dst = mesh;
        result = true;
    }

    return result;
}

internal b32 HasValidationLayerSupport()
{
    b32 found = false;

    VkLayerProperties layers[256];
    u32 layerCount = ArrayCount(layers);
    VK_CHECK(vkEnumerateInstanceLayerProperties(&layerCount, layers));

    for (u32 layerIndex = 0; layerIndex < layerCount; ++layerIndex)
    {
        if (strcmp(VALIDATION_LAYER_NAME,
                layers[layerIndex].layerName) == 0)
        {
            found = true;
            break;
        }
    }

    return found;
}

internal VkInstance VulkanCreateInstance()
{
    const char *validationLayers[] = {VALIDATION_LAYER_NAME};
    const char *requestedExtensions[] = {VK_EXT_DEBUG_UTILS_EXTENSION_NAME};

    u32 glfwExtensionsCount = 0;
    const char **glfwExtensions =
        glfwGetRequiredInstanceExtensions(&glfwExtensionsCount);

    const char *extensions[16];
    u32 extensionCount = 0;
    for (u32 i = 0; i < ArrayCount(requestedExtensions); ++i)
    {
        Assert(extensionCount < ArrayCount(extensions));
        extensions[extensionCount++] = requestedExtensions[i];
    }

    for (u32 i = 0; i < glfwExtensionsCount; ++i)
    {
        Assert(extensionCount < ArrayCount(extensions));
        extensions[extensionCount++] = glfwExtensions[i];
    }

    VkApplicationInfo appInfo = { VK_STRUCTURE_TYPE_APPLICATION_INFO };
    appInfo.apiVersion = VK_API_VERSION_1_2;

    VkInstanceCreateInfo instanceCreateInfo = {
        VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO};
    instanceCreateInfo.pApplicationInfo = &appInfo;
    instanceCreateInfo.enabledExtensionCount = extensionCount;
    instanceCreateInfo.ppEnabledExtensionNames = extensions;
#ifdef ENABLE_VALIDATION_LAYERS
    LogMessage("Enabling Vulkan validation layers");
    if (HasValidationLayerSupport())
    {
        instanceCreateInfo.enabledLayerCount = ArrayCount(validationLayers);
        instanceCreateInfo.ppEnabledLayerNames = validationLayers;
    }
    else
    {
        LogMessage("Vulkan instance does not support validation layers");
    }
#endif

    VkInstance instance;
    VK_CHECK(vkCreateInstance(&instanceCreateInfo, NULL, &instance));

    return instance;
}

internal VulkanQueueFamilyIndices VulkanGetQueueFamilies(
    VkPhysicalDevice physicalDevice, VkSurfaceKHR surface)
{
    VulkanQueueFamilyIndices result = {};
    result.graphicsQueue = U32_MAX;
    result.presentQueue = U32_MAX;

    VkQueueFamilyProperties queueFamilies[64];
    u32 queueFamilyCount = ArrayCount(queueFamilies);
    vkGetPhysicalDeviceQueueFamilyProperties(
        physicalDevice, &queueFamilyCount, queueFamilies);

    for (u32 i = 0; i < queueFamilyCount; ++i)
    {
        if (result.graphicsQueue != U32_MAX &&
            result.presentQueue != U32_MAX)
        {
            break;
        }

        if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            result.graphicsQueue = i;
        }

        b32 supported = false;
        VK_CHECK(vkGetPhysicalDeviceSurfaceSupportKHR(
                    physicalDevice, i, surface, &supported));
        if (supported)
        {
            result.presentQueue = i;
        }
    }
    Assert(result.graphicsQueue != U32_MAX);
    Assert(result.presentQueue != U32_MAX);

    return result;
}

internal VkDevice VulkanCreateDevice(VkInstance instance,
    VkPhysicalDevice physicalDevice,
    VulkanQueueFamilyIndices queueFamilyIndices)
{
    float queuePriorities[] = { 1.0f };

    u32 queueCreateInfoCount = 1;
    VkDeviceQueueCreateInfo queueCreateInfos[2] = {};
    queueCreateInfos[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queueCreateInfos[0].queueFamilyIndex = queueFamilyIndices.graphicsQueue;
    queueCreateInfos[0].queueCount = 1;
    queueCreateInfos[0].pQueuePriorities = queuePriorities;

    if (queueFamilyIndices.presentQueue != queueFamilyIndices.graphicsQueue)
    {
        queueCreateInfos[1].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queueCreateInfos[1].queueFamilyIndex = queueFamilyIndices.presentQueue;
        queueCreateInfos[1].queueCount = 1;
        queueCreateInfos[1].pQueuePriorities = queuePriorities;
        queueCreateInfoCount = 2;
    }

    const char *extensions[] = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME, VK_EXT_SHADER_VIEWPORT_INDEX_LAYER_EXTENSION_NAME};

    VkPhysicalDeviceVulkan12Features enabledFeaturesVulkan12 = {
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES};
    enabledFeaturesVulkan12.shaderOutputViewportIndex = VK_TRUE;
    enabledFeaturesVulkan12.shaderOutputLayer = VK_TRUE;

    // TODO: Not sure if we need to test if the feature is supported?
    VkPhysicalDeviceFeatures2 enabledFeatures = {
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2};
    enabledFeatures.pNext = &enabledFeaturesVulkan12;
    enabledFeatures.features.fillModeNonSolid = VK_TRUE;
    enabledFeatures.features.samplerAnisotropy = VK_TRUE;
    enabledFeatures.features.shaderSampledImageArrayDynamicIndexing = VK_TRUE;

    VkDeviceCreateInfo deviceCreateInfo = {VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO};
    deviceCreateInfo.pNext = &enabledFeatures;
    deviceCreateInfo.ppEnabledExtensionNames = extensions;
    deviceCreateInfo.enabledExtensionCount = ArrayCount(extensions);
    deviceCreateInfo.queueCreateInfoCount = queueCreateInfoCount;
    deviceCreateInfo.pQueueCreateInfos = queueCreateInfos;
    //deviceCreateInfo.pEnabledFeatures = &enabledFeatures;

    VkDevice device;
    VK_CHECK(vkCreateDevice(physicalDevice, &deviceCreateInfo, NULL, &device));

    return device;
}


internal VkSwapchainKHR VulkanCreateSwapchain(VkDevice device,
    VkSurfaceKHR surface, u32 imageCount, u32 framebufferWidth,
    u32 framebufferHeight, VulkanQueueFamilyIndices queueFamilyIndices)
{
    u32 queueFamilyIndicesArray[2] = {
        queueFamilyIndices.graphicsQueue, queueFamilyIndices.presentQueue};

    VkSwapchainCreateInfoKHR createInfo = {VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR};
    createInfo.surface = surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = VK_FORMAT_B8G8R8A8_UNORM;
    createInfo.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR; // FIXME: Supported?
    createInfo.imageExtent.width = framebufferWidth;
    createInfo.imageExtent.height = framebufferHeight;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    if (queueFamilyIndices.graphicsQueue != queueFamilyIndices.presentQueue)
    {
        // VK_SHARING_MODE_EXCLUSIVE is defined as 0, so we don't need to
        // handle it
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = ArrayCount(queueFamilyIndicesArray);
        createInfo.pQueueFamilyIndices = queueFamilyIndicesArray;
    }
    createInfo.presentMode = VK_PRESENT_MODE_FIFO_KHR; //VK_PRESENT_MODE_IMMEDIATE_KHR; // FIXME: Always supported?
    createInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    VkSwapchainKHR swapchain;
    VK_CHECK(vkCreateSwapchainKHR(device, &createInfo, NULL, &swapchain));
    return swapchain;
}

internal VulkanSwapchain VulkanSetupSwapchain(VkDevice device,
    VkPhysicalDevice physicalDevice, VkSurfaceKHR surface,
    VulkanQueueFamilyIndices queueFamilyIndices, VkRenderPass renderPass,
    u32 imageCount, u32 width, u32 height)
{
    VulkanSwapchain swapchain = {};
    swapchain.width = width;
    swapchain.height = height;
    swapchain.imageCount = imageCount;

    Assert(imageCount <= ArrayCount(swapchain.images));

    swapchain.handle = VulkanCreateSwapchain(device, surface,
        imageCount, width, height, queueFamilyIndices);

    VK_CHECK(vkGetSwapchainImagesKHR(
        device, swapchain.handle, &swapchain.imageCount, NULL));

    // TODO: Probably don't need to assert this, rather just check that we can
    // store the returned imageCount
    Assert(swapchain.imageCount == imageCount);
    VK_CHECK(vkGetSwapchainImagesKHR(
        device, swapchain.handle, &swapchain.imageCount, swapchain.images));

    swapchain.depthImage =
        VulkanCreateImage(device, physicalDevice, width, height,
            VK_FORMAT_D32_SFLOAT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_DEPTH_BIT);

    for (u32 i = 0; i < swapchain.imageCount; ++i)
    {
        // TODO: Get supported image formats for the surface?
        swapchain.imageViews[i] =
            VulkanCreateImageView(device, swapchain.images[i],
                VK_FORMAT_B8G8R8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
        swapchain.framebuffers[i] = VulkanCreateFramebuffer(device, renderPass,
            swapchain.imageViews[i], swapchain.depthImage.view, width, height);
    }

    return swapchain;
}

internal void VulkanDestroySwapchain(VulkanSwapchain swapchain, VkDevice device)
{
    for (u32 i = 0; i < swapchain.imageCount; ++i)
    {
        vkDestroyFramebuffer(device, swapchain.framebuffers[i], NULL);
        vkDestroyImageView(device, swapchain.imageViews[i], NULL);
    }
    VulkanDestroyImage(device, swapchain.depthImage);

    vkDestroySwapchainKHR(device, swapchain.handle, NULL);
}

// TODO: Define constants for this
internal VkDescriptorPool VulkanCreateDescriptorPool(VkDevice device)
{
    VkDescriptorPoolSize poolSizes[6];
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSizes[0].descriptorCount = 64;
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    poolSizes[1].descriptorCount = 64;
    poolSizes[2].type = VK_DESCRIPTOR_TYPE_SAMPLER;
    poolSizes[2].descriptorCount = 64;
    poolSizes[3].type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    poolSizes[3].descriptorCount = 256;
    poolSizes[4].type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    poolSizes[4].descriptorCount = 64;
    poolSizes[5].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[5].descriptorCount = 64;

    VkDescriptorPoolCreateInfo createInfo = {VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO};
    createInfo.poolSizeCount = ArrayCount(poolSizes);
    createInfo.pPoolSizes = poolSizes;
    createInfo.maxSets = 64;
    // TODO: Spec doesn't make this sound particularly efficient. It seems like
    // we should rather have a separate pool for descriptor sets which we want
    // to free and then just do a bulk reset for them.
    createInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;

    VkDescriptorPool descriptorPool;
    VK_CHECK(vkCreateDescriptorPool(device, &createInfo, NULL, &descriptorPool));
    return descriptorPool;
}

internal VkDescriptorSetLayout VulkanCreateDescriptorSetLayout(
    VkDevice device, VkSampler sampler)
{
    VkDescriptorSetLayoutBinding layoutBindings[11] = {};
    layoutBindings[0].binding = 0;
    layoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    layoutBindings[0].descriptorCount = 1;
    layoutBindings[0].stageFlags =
        VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
    layoutBindings[1].binding = 1;
    layoutBindings[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    layoutBindings[1].descriptorCount = 1;
    layoutBindings[1].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    layoutBindings[2].binding = 2;
    layoutBindings[2].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
    layoutBindings[2].descriptorCount = 1;
    layoutBindings[2].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    layoutBindings[2].pImmutableSamplers = &sampler;
    layoutBindings[3].binding = 3;
    layoutBindings[3].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    layoutBindings[3].descriptorCount = 1;
    layoutBindings[3].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    layoutBindings[4].binding = 4;
    layoutBindings[4].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    layoutBindings[4].descriptorCount = 1;
    layoutBindings[4].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    layoutBindings[5].binding = 6;
    layoutBindings[5].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    layoutBindings[5].descriptorCount = 1;
    layoutBindings[5].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    layoutBindings[6].binding = 7;
    layoutBindings[6].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    layoutBindings[6].descriptorCount = 1;
    layoutBindings[6].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    layoutBindings[7].binding = 8;
    layoutBindings[7].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    layoutBindings[7].descriptorCount = 1;
    layoutBindings[7].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    layoutBindings[8].binding = 9;
    layoutBindings[8].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    layoutBindings[8].descriptorCount = 1;
    layoutBindings[8].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    layoutBindings[9].binding = 10;
    layoutBindings[9].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    layoutBindings[9].descriptorCount = 1;
    layoutBindings[9].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    layoutBindings[10].binding = 11;
    layoutBindings[10].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    layoutBindings[10].descriptorCount = 1;
    layoutBindings[10].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    VkDescriptorSetLayoutCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO};
    createInfo.bindingCount = ArrayCount(layoutBindings);
    createInfo.pBindings = layoutBindings;

    VkDescriptorSetLayout descriptorSetLayout;

    VK_CHECK(vkCreateDescriptorSetLayout(
        device, &createInfo, NULL, &descriptorSetLayout));

    return descriptorSetLayout;
}

internal VkDescriptorSetLayout VulkanCreateDebugDrawDescriptorSetLayout(
    VkDevice device)
{
    VkDescriptorSetLayoutBinding layoutBindings[2] = {};
    layoutBindings[0].binding = 0;
    layoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    layoutBindings[0].descriptorCount = 1;
    layoutBindings[0].stageFlags =
        VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
    layoutBindings[1].binding = 1;
    layoutBindings[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    layoutBindings[1].descriptorCount = 1;
    layoutBindings[1].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    VkDescriptorSetLayoutCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO};
    createInfo.bindingCount = ArrayCount(layoutBindings);
    createInfo.pBindings = layoutBindings;

    VkDescriptorSetLayout descriptorSetLayout;

    VK_CHECK(vkCreateDescriptorSetLayout(
        device, &createInfo, NULL, &descriptorSetLayout));

    return descriptorSetLayout;
}

internal VkDescriptorSetLayout VulkanCreateShadowDescriptorSetLayout(
    VkDevice device)
{
    VkDescriptorSetLayoutBinding layoutBindings[3] = {};
    layoutBindings[0].binding = 0;
    layoutBindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    layoutBindings[0].descriptorCount = 1;
    layoutBindings[0].stageFlags =
        VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;

    layoutBindings[1].binding = 1;
    layoutBindings[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    layoutBindings[1].descriptorCount = 1;
    layoutBindings[1].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    layoutBindings[2].binding = 4;
    layoutBindings[2].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    layoutBindings[2].descriptorCount = 1;
    layoutBindings[2].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    VkDescriptorSetLayoutCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO};
    createInfo.bindingCount = ArrayCount(layoutBindings);
    createInfo.pBindings = layoutBindings;

    VkDescriptorSetLayout descriptorSetLayout;

    VK_CHECK(vkCreateDescriptorSetLayout(
        device, &createInfo, NULL, &descriptorSetLayout));

    return descriptorSetLayout;
}

internal VkPipelineLayout VulkanCreatePipelineLayout(VkDevice device,
    VkDescriptorSetLayout descriptorSetLayout, b32 usePushConstants)
{
    VkPushConstantRange pushConstantRange = {};
    pushConstantRange.offset = 0;
    pushConstantRange.size = sizeof(MeshPushConstants);
    pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

    VkPipelineLayoutCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO};
    createInfo.setLayoutCount = 1;
    createInfo.pSetLayouts = &descriptorSetLayout;
    if (usePushConstants)
    {
        createInfo.pPushConstantRanges = &pushConstantRange;
        createInfo.pushConstantRangeCount = 1;
    }

    VkPipelineLayout layout;
    VK_CHECK(vkCreatePipelineLayout(device, &createInfo, 0, &layout));
    return layout;
}

internal void VulkanCopyImageFromCPU(VulkanRenderer *renderer)
{
    u32 width = RAY_TRACER_WIDTH;
    u32 height = RAY_TRACER_HEIGHT;

    VulkanImage *image = &renderer->cpuPathTracerImage;

    VulkanTransitionImageLayout(image->handle,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, renderer->device,
        renderer->commandPool, renderer->graphicsQueue);

    VulkanCopyBufferToImage(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->imageUploadBuffer.handle,
        image->handle, width, height, 0);

    VulkanTransitionImageLayout(image->handle,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, renderer->device,
        renderer->commandPool, renderer->graphicsQueue);
}

internal void VulkanCopyMeshDataToGpu(VulkanRenderer *renderer)
{
    VulkanCopyBuffer(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->vertexDataUploadBuffer.handle,
        renderer->vertexDataBuffer.handle,
        renderer->vertexDataUploadBufferSize);

    VulkanCopyBuffer(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->indexUploadBuffer.handle,
        renderer->indexBuffer.handle, 
        renderer->indexUploadBufferSize);
}

internal void UploadSceneDataToGpu(VulkanRenderer *renderer, Scene scene)
{
    mat4 *modelMatrices = (mat4 *)renderer->modelMatricesBuffer.data;
    modelMatrices[0] = Identity(); // 0 slot reserved for skybox

    for (u32 i = 0; i < scene.count; ++i)
    {
        Entity *entity = scene.entities + i;
        modelMatrices[i + 1] = Translate(entity->position) *
                               Rotate(entity->rotation) * Scale(entity->scale);
    }
}

// FIXME: Why does this share the same descriptor sets as the mesh shader!
// Because we use the mesh.vert.glsl shader
internal void UpdatePostProcessingDescriptorSets(VulkanRenderer *renderer)
{
    // Post processing descriptor sets
    for (u32 i = 0; i < renderer->swapchain.imageCount; ++i)
    {
        VkDescriptorBufferInfo uniformBufferInfo = {};
        uniformBufferInfo.buffer = renderer->uniformBuffer.handle;
        uniformBufferInfo.range = VK_WHOLE_SIZE;

        VkDescriptorBufferInfo vertexDataBufferInfo = {};
        vertexDataBufferInfo.buffer =
            renderer->vertexDataBuffer.handle;
        vertexDataBufferInfo.range = VK_WHOLE_SIZE;

        VkDescriptorBufferInfo modelMatrixBufferInfo = {};
        modelMatrixBufferInfo.buffer = renderer->modelMatricesBuffer.handle;
        modelMatrixBufferInfo.range = VK_WHOLE_SIZE;

        VkDescriptorImageInfo vulkanImageInfo = {};
        vulkanImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        vulkanImageInfo.imageView = renderer->hdrSwapchain.framebuffers[i].color.view;

        VulkanImage *cpuRayTracerImage = &renderer->cpuPathTracerImage;
        Assert(cpuRayTracerImage != NULL);
        VkDescriptorImageInfo rayTracerImageInfo = {};
        rayTracerImageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        rayTracerImageInfo.imageView = cpuRayTracerImage->view;

        VkWriteDescriptorSet descriptorWrites[5] = {};
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = renderer->postProcessDescriptorSets[i];
        descriptorWrites[0].dstBinding = 0;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pBufferInfo = &uniformBufferInfo;
        descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].dstSet = renderer->postProcessDescriptorSets[i];
        descriptorWrites[1].dstBinding = 1;
        descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pBufferInfo = &vertexDataBufferInfo;
        // Binding 2 is for the defaultSampler
        descriptorWrites[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[2].dstSet = renderer->postProcessDescriptorSets[i];
        descriptorWrites[2].dstBinding = 3;
        descriptorWrites[2].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
        descriptorWrites[2].descriptorCount = 1;
        descriptorWrites[2].pImageInfo = &vulkanImageInfo;
        descriptorWrites[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[3].dstSet = renderer->postProcessDescriptorSets[i];
        descriptorWrites[3].dstBinding = 4;
        descriptorWrites[3].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorWrites[3].descriptorCount = 1;
        descriptorWrites[3].pBufferInfo = &modelMatrixBufferInfo;
        // Binding 5 is for the material data
        // Binding 6 is for the test cube map
        descriptorWrites[4].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[4].dstSet = renderer->postProcessDescriptorSets[i];
        descriptorWrites[4].dstBinding = 9;
        descriptorWrites[4].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
        descriptorWrites[4].descriptorCount = 1;
        descriptorWrites[4].pImageInfo = &rayTracerImageInfo;
        // Binding 10 is the lighting data
        vkUpdateDescriptorSets(renderer->device, ArrayCount(descriptorWrites),
            descriptorWrites, 0, NULL);
    }

}

internal void UpdateMeshDescriptorSets(VulkanRenderer *renderer)
{
    Assert(renderer->swapchain.imageCount == 2);
    for (u32 i = 0; i < renderer->swapchain.imageCount; ++i)
    {
        VkDescriptorBufferInfo uniformBufferInfo = {};
        uniformBufferInfo.buffer = renderer->uniformBuffer.handle;
        uniformBufferInfo.range = VK_WHOLE_SIZE;

        VkDescriptorBufferInfo vertexDataBufferInfo = {};
        vertexDataBufferInfo.buffer =
            renderer->vertexDataBuffer.handle;
        vertexDataBufferInfo.range = VK_WHOLE_SIZE;

        VkDescriptorBufferInfo modelMatrixBufferInfo = {};
        modelMatrixBufferInfo.buffer = renderer->modelMatricesBuffer.handle;
        modelMatrixBufferInfo.range = VK_WHOLE_SIZE;

        VkDescriptorBufferInfo lightBufferInfo = {};
        lightBufferInfo.buffer = renderer->lightBuffer.handle;
        lightBufferInfo.range = VK_WHOLE_SIZE;

        VulkanImage *cpuRayTracerImage = &renderer->cpuPathTracerImage;
        Assert(cpuRayTracerImage != NULL);

        VkDescriptorImageInfo imageInfo = {};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = cpuRayTracerImage->view;

        VkWriteDescriptorSet descriptorWrites[5] = {};
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = renderer->descriptorSets[i];
        descriptorWrites[0].dstBinding = 0;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pBufferInfo = &uniformBufferInfo;
        descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].dstSet = renderer->descriptorSets[i];
        descriptorWrites[1].dstBinding = 1;
        descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pBufferInfo = &vertexDataBufferInfo;
        // Binding 2 is for the defaultSampler
        descriptorWrites[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[2].dstSet = renderer->descriptorSets[i];
        descriptorWrites[2].dstBinding = 3;
        descriptorWrites[2].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
        descriptorWrites[2].descriptorCount = 1;
        descriptorWrites[2].pImageInfo = &imageInfo;
        descriptorWrites[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[3].dstSet = renderer->descriptorSets[i];
        descriptorWrites[3].dstBinding = 4;
        descriptorWrites[3].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorWrites[3].descriptorCount = 1;
        descriptorWrites[3].pBufferInfo = &modelMatrixBufferInfo;
        descriptorWrites[4].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[4].dstSet = renderer->descriptorSets[i];
        descriptorWrites[4].dstBinding = 10;
        descriptorWrites[4].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorWrites[4].descriptorCount = 1;
        descriptorWrites[4].pBufferInfo = &lightBufferInfo;

        vkUpdateDescriptorSets(renderer->device, ArrayCount(descriptorWrites),
            descriptorWrites, 0, NULL);
    }
}

internal void UpdateDebugDrawDescriptorSets(VulkanRenderer *renderer)
{
    for (u32 i = 0; i < renderer->swapchain.imageCount; ++i)
    {
        VkDescriptorBufferInfo uniformBufferInfo = {};
        uniformBufferInfo.buffer = renderer->uniformBuffer.handle;
        uniformBufferInfo.range = VK_WHOLE_SIZE;

        VkDescriptorBufferInfo vertexDataBufferInfo = {};
        vertexDataBufferInfo.buffer =
            renderer->debugVertexDataBuffer.handle;
        vertexDataBufferInfo.range = VK_WHOLE_SIZE;

        VkWriteDescriptorSet descriptorWrites[2] = {};
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = renderer->debugDrawDescriptorSets[i];
        descriptorWrites[0].dstBinding = 0;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pBufferInfo = &uniformBufferInfo;
        descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].dstSet = renderer->debugDrawDescriptorSets[i];
        descriptorWrites[1].dstBinding = 1;
        descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pBufferInfo = &vertexDataBufferInfo;
        vkUpdateDescriptorSets(renderer->device, ArrayCount(descriptorWrites),
            descriptorWrites, 0, NULL);
    }
}

internal VkRenderPass CreateRenderPassForShadowPass(VkDevice device)
{
    VkAttachmentDescription attachment = {};
    attachment.format = VK_FORMAT_D32_SFLOAT;
    attachment.samples = VK_SAMPLE_COUNT_1_BIT;
    attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachment.finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkAttachmentReference depthAttachmentRef = {};
    depthAttachmentRef.attachment = 0;
    depthAttachmentRef.layout =
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subpass = {};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;

    VkRenderPassCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO};
    createInfo.attachmentCount = 1;
    createInfo.pAttachments = &attachment;
    createInfo.subpassCount = 1;
    createInfo.pSubpasses = &subpass;

    VkRenderPass renderPass = VK_NULL_HANDLE;
    VK_CHECK(vkCreateRenderPass(device, &createInfo, NULL, &renderPass));
    return renderPass;
}

internal VulkanImage VulkanCreateShadowPassDepthTexture(VkDevice device,
        VkPhysicalDevice physicalDevice, u32 width, u32 height)
{
    u32 layerCount = SHADOW_PASS_LAYER_COUNT;
    VkFormat format = VK_FORMAT_D32_SFLOAT;
    VkImageUsageFlags usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT |
                              VK_IMAGE_USAGE_SAMPLED_BIT;
    VkMemoryPropertyFlags propertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    VkImageAspectFlags aspectFlags = VK_IMAGE_ASPECT_DEPTH_BIT;
    u32 mipLevelCount = 1;

    VulkanImage image = {};

    VkImageCreateInfo createInfo = {VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
    createInfo.imageType = VK_IMAGE_TYPE_2D;
    createInfo.extent.width = width;
    createInfo.extent.height = height;
    createInfo.extent.depth = 1;
    createInfo.mipLevels = mipLevelCount;
    createInfo.arrayLayers = layerCount;
    createInfo.format = format;
    createInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    createInfo.usage = usage;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    createInfo.flags = 0;

    VK_CHECK(vkCreateImage(device, &createInfo, NULL, &image.handle));

    VkMemoryRequirements memoryRequirements;
    vkGetImageMemoryRequirements(device, image.handle, &memoryRequirements);

    VkMemoryAllocateInfo allocInfo = {VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO};
    allocInfo.allocationSize = memoryRequirements.size;
    allocInfo.memoryTypeIndex = VulkanFindMemoryType(
        physicalDevice, memoryRequirements.memoryTypeBits, propertyFlags);

    VK_CHECK(vkAllocateMemory(device, &allocInfo, NULL, &image.memory));

    vkBindImageMemory(device, image.handle, image.memory, 0);

    VkImageViewCreateInfo viewInfo = {VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO};
    viewInfo.image = image.handle;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
    viewInfo.format = format;
    viewInfo.subresourceRange.aspectMask = aspectFlags;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = mipLevelCount;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = layerCount;

    VK_CHECK(vkCreateImageView(device, &viewInfo, NULL, &image.view));

    return image;
}

internal VulkanShadowBuffer CreateShadowPassBuffer(VkDevice device,
    VkPhysicalDevice physicalDevice, u32 width, u32 height,
    VkRenderPass renderPass)
{
    VulkanShadowBuffer buffer = {};
    buffer.depth = VulkanCreateShadowPassDepthTexture(
        device, physicalDevice, width, height);

    VkImageView attachment = buffer.depth.view;

    VkFramebufferCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO};
    createInfo.renderPass = renderPass;
    createInfo.attachmentCount = 1;
    createInfo.pAttachments = &attachment;
    createInfo.width = width;
    createInfo.height = height;
    createInfo.layers = SHADOW_PASS_LAYER_COUNT;

    VK_CHECK(vkCreateFramebuffer(device, &createInfo, NULL, &buffer.handle));
    return buffer;
}

internal VulkanShadowPass CreateShadowPass(VkDevice device,
    VkPhysicalDevice physicalDevice, u32 width, u32 height, u32 bufferCount)
{
    // TODO: Support arbitrary number of buffers
    Assert(bufferCount == 2);

    VulkanShadowPass shadowPass = {};
    shadowPass.renderPass = CreateRenderPassForShadowPass(device);
    shadowPass.bufferCount = bufferCount;
    shadowPass.width = width;
    shadowPass.height = height;

    for (u32 i = 0; i < bufferCount; ++i)
    {
        shadowPass.buffers[i] = CreateShadowPassBuffer(
            device, physicalDevice, width, height, shadowPass.renderPass);
    }

    return shadowPass;
}

internal VkDescriptorSetLayout CreateDescriptorSetLayout(VkDevice device,
        VkDescriptorSetLayoutBinding *bindings, u32 count)
{
    VkDescriptorSetLayoutCreateInfo createInfo = {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO};
    createInfo.bindingCount = count;
    createInfo.pBindings = bindings;

    VkDescriptorSetLayout descriptorSetLayout;

    VK_CHECK(vkCreateDescriptorSetLayout(
        device, &createInfo, NULL, &descriptorSetLayout));

    return descriptorSetLayout;
}

internal VulkanComputeShaderStuff CreateComputeShaderPipeline(
    VulkanRenderer *renderer, const char *path)
{
    // clang-format off
    VkDescriptorSetLayoutBinding bindings[] = {
        // binding, descriptorType, descriptorCount, stageFlags
        {0, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1, VK_SHADER_STAGE_COMPUTE_BIT},
        {1, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1, VK_SHADER_STAGE_COMPUTE_BIT},
    };
    // clang-format on

    VkDescriptorSetLayout descriptorSetLayout = CreateDescriptorSetLayout(
        renderer->device, bindings, ArrayCount(bindings));

    VkDescriptorSet descriptorSet = VK_NULL_HANDLE;
    VulkanAllocateDescriptorSets(renderer->device, renderer->descriptorPool,
        &descriptorSetLayout, 1, &descriptorSet);

    VkPipelineLayout pipelineLayout = VulkanCreatePipelineLayout(
        renderer->device, descriptorSetLayout, false);

    VkShaderModule shader = LoadShader(renderer->device, path);

    VkComputePipelineCreateInfo computePipelineCreateInfo = {
        VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO};

    computePipelineCreateInfo.stage.sType =
        VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    computePipelineCreateInfo.stage.stage = VK_SHADER_STAGE_COMPUTE_BIT;
    computePipelineCreateInfo.stage.module = shader;
    computePipelineCreateInfo.stage.pName = "main";
    computePipelineCreateInfo.layout = pipelineLayout;

    VkPipeline pipeline = VK_NULL_HANDLE;
    VK_CHECK(vkCreateComputePipelines(renderer->device, renderer->pipelineCache,
        1, &computePipelineCreateInfo, NULL, &pipeline));

    VulkanComputeShaderStuff result = {};
    result.shader = shader;
    result.descriptorSetLayout = descriptorSetLayout;
    result.descriptorSet = descriptorSet;
    result.pipelineLayout = pipelineLayout;
    result.pipeline = pipeline;

    return result;
}

// TODO: Handle errors gracefully?
internal void VulkanInit(VulkanRenderer *renderer, GLFWwindow *window)
{
    // Setup resource storage
    renderer->meshes = Dict_CreateFromArrays(renderer->meshKeys,
            renderer->meshValues);
    renderer->textures = Dict_CreateFromArrays(renderer->textureKeys,
            renderer->textureValues);
    renderer->materials = Dict_CreateFromArrays(renderer->materialKeys,
            renderer->materialValues);

    // Create instance, debug callback
    renderer->instance = VulkanCreateInstance();
    renderer->messenger = VulkanRegisterDebugCallback(renderer->instance);

    // Select physical device
    renderer->physicalDevice = VulkanGetPhysicalDevice(renderer->instance);

    // Create surface
    VK_CHECK(glfwCreateWindowSurface(
        renderer->instance, window, NULL, &renderer->surface));

    // Create logical device
    renderer->queueFamilyIndices =
        VulkanGetQueueFamilies(renderer->physicalDevice, renderer->surface);
    renderer->device = VulkanCreateDevice(renderer->instance,
        renderer->physicalDevice, renderer->queueFamilyIndices);

    // Retrieve graphics and present queue
    vkGetDeviceQueue(renderer->device,
        renderer->queueFamilyIndices.graphicsQueue, 0,
        &renderer->graphicsQueue);
    vkGetDeviceQueue(renderer->device,
        renderer->queueFamilyIndices.presentQueue, 0, &renderer->presentQueue);

    // FIXME: Query surface formats and present modes
    VkSurfaceCapabilitiesKHR surfaceCapabilities = {
        VK_STRUCTURE_TYPE_SURFACE_CAPABILITIES_2_KHR};
    VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
        renderer->physicalDevice, renderer->surface, &surfaceCapabilities));
    Assert(surfaceCapabilities.currentTransform &
           VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR);

    VkSurfaceFormatKHR surfaceFormats[8];
    u32 surfaceFormatCount = ArrayCount(surfaceFormats);
    VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(renderer->physicalDevice,
        renderer->surface, &surfaceFormatCount, surfaceFormats));
    Assert(surfaceFormats[0].format == VK_FORMAT_B8G8R8A8_UNORM);
    Assert(surfaceFormats[0].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR);

    // Create render pass
    VulkanRenderPassSpec displayRenderPassSpec = {};
    displayRenderPassSpec.colorAttachmentFormat = surfaceFormats[0].format;
    displayRenderPassSpec.colorAttachmentFinalLayout =
        VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    displayRenderPassSpec.useDepth = true;
    renderer->renderPass =
        VulkanCreateRenderPass(renderer->device, displayRenderPassSpec);

    // TODO: Config option to support 16-bit floats?
    VkFormat linearFormat = VK_FORMAT_R32G32B32A32_SFLOAT;

    VulkanRenderPassSpec hdrRenderPassSpec = {};
    hdrRenderPassSpec.colorAttachmentFormat = linearFormat;
    hdrRenderPassSpec.colorAttachmentFinalLayout =
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    hdrRenderPassSpec.useDepth = true;
    renderer->hdrRenderPass =
        VulkanCreateRenderPass(renderer->device, hdrRenderPassSpec);

    renderer->hdrSwapchain = CreateHdrSwapchain(renderer->device,
        renderer->physicalDevice, linearFormat,
        surfaceCapabilities.currentExtent.width,
        surfaceCapabilities.currentExtent.height, 2, renderer->hdrRenderPass);

    u32 shadowBufferWidth = SHADOW_MAP_SIZE;
    u32 shadowBufferHeight = SHADOW_MAP_SIZE;
    renderer->shadowPass = CreateShadowPass(renderer->device,
        renderer->physicalDevice, shadowBufferWidth, shadowBufferHeight, 2);

    // Create swapchain
    renderer->swapchain =
        VulkanSetupSwapchain(renderer->device, renderer->physicalDevice,
            renderer->surface, renderer->queueFamilyIndices,
            renderer->renderPass, 2, surfaceCapabilities.currentExtent.width,
            surfaceCapabilities.currentExtent.height);

    renderer->acquireSemaphore = VulkanCreateSemaphore(renderer->device);
    renderer->releaseSemaphore = VulkanCreateSemaphore(renderer->device);

    // Create command pool
    renderer->commandPool = VulkanCreateCommandPool(
        renderer->device, renderer->queueFamilyIndices.graphicsQueue);

    // Create descriptor pool
    renderer->descriptorPool = VulkanCreateDescriptorPool(renderer->device);

    // Create command buffer
    renderer->commandBuffer =
        VulkanAllocateCommandBuffer(renderer->device, renderer->commandPool);

    // Create vertex data upload buffer
    renderer->vertexDataUploadBuffer =
        VulkanCreateBuffer(renderer->device, renderer->physicalDevice,
            VERTEX_BUFFER_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    // Create vertex data buffer
    renderer->vertexDataBuffer = VulkanCreateBuffer(renderer->device,
        renderer->physicalDevice, VERTEX_BUFFER_SIZE,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    renderer->uniformBuffer =
        VulkanCreateBuffer(renderer->device, renderer->physicalDevice,
            UNIFORM_BUFFER_SIZE, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    renderer->indexUploadBuffer =
        VulkanCreateBuffer(renderer->device, renderer->physicalDevice,
            INDEX_BUFFER_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    renderer->indexBuffer = VulkanCreateBuffer(renderer->device,
        renderer->physicalDevice, INDEX_BUFFER_SIZE,
        VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT |
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    renderer->imageUploadBuffer =
        VulkanCreateBuffer(renderer->device, renderer->physicalDevice,
            IMAGE_BUFFER_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    renderer->debugVertexDataBuffer =
        VulkanCreateBuffer(renderer->device, renderer->physicalDevice,
            DEBUG_VERTEX_BUFFER_SIZE, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    renderer->modelMatricesBuffer =
        VulkanCreateBuffer(renderer->device, renderer->physicalDevice,
            MODEL_MATRICES_BUFFER_SIZE, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    // TODO: Combine this with materialBuffer?
    renderer->lightBuffer =
        VulkanCreateBuffer(renderer->device, renderer->physicalDevice,
            LIGHT_BUFFER_SIZE, VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    renderer->textureUploadBuffer =
        VulkanCreateBuffer(renderer->device, renderer->physicalDevice,
            TEXTURE_UPLOAD_BUFFER_SIZE, VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

    // Load shaders
    renderer->testVertexShader = LoadShader(renderer->device, "mesh.vert.spv");
    renderer->testFragmentShader = LoadShader(renderer->device, "mesh.frag.spv");

    // Load fullscreen quad shaders
    renderer->fullscreenQuadVertexShader = LoadShader(renderer->device, "fullscreen_quad.vert.spv");

    // Debug draw shaders
    renderer->debugDrawVertexShader = LoadShader(renderer->device, "debug_draw.vert.spv");
    renderer->debugDrawFragmentShader = LoadShader(renderer->device, "debug_draw.frag.spv");

    // Post processing shader
    renderer->postProcessingFragmentShader = LoadShader(renderer->device, "post_processing.frag.spv");

    // Skybox shader
    renderer->skyboxFragmentShader = LoadShader(renderer->device, "skybox.frag.spv");

    // Shadow shader
    renderer->shadowFragmentShader = LoadShader(renderer->device, "shadow.frag.spv");

    // TODO: CLAMP_TO_EDGE sampler 
    VkSamplerCreateInfo samplerCreateInfo = {VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO};
    samplerCreateInfo.magFilter = VK_FILTER_LINEAR;
    samplerCreateInfo.minFilter = VK_FILTER_LINEAR;
    samplerCreateInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerCreateInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerCreateInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerCreateInfo.anisotropyEnable = VK_TRUE;
    samplerCreateInfo.maxAnisotropy = 16.0f;
    samplerCreateInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerCreateInfo.unnormalizedCoordinates = VK_FALSE;
    samplerCreateInfo.compareEnable = VK_FALSE;
    samplerCreateInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerCreateInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerCreateInfo.mipLodBias = 0.0f;
    samplerCreateInfo.minLod = 0.0f;
    samplerCreateInfo.maxLod = 1000.0f;
    VK_CHECK(vkCreateSampler(
        renderer->device, &samplerCreateInfo, NULL, &renderer->defaultSampler));

    // Create descriptor set layout
    renderer->descriptorSetLayout = VulkanCreateDescriptorSetLayout(
        renderer->device, renderer->defaultSampler);

    renderer->shadowDescriptorSetLayout =
        VulkanCreateShadowDescriptorSetLayout(renderer->device);

    renderer->debugDrawDescriptorSetLayout =
        VulkanCreateDebugDrawDescriptorSetLayout(renderer->device);

    // Create pipeline layout
    renderer->pipelineLayout = VulkanCreatePipelineLayout(
        renderer->device, renderer->descriptorSetLayout, true);

    renderer->shadowPipelineLayout = VulkanCreatePipelineLayout(
        renderer->device, renderer->shadowDescriptorSetLayout, true);

    renderer->debugDrawPipelineLayout = VulkanCreatePipelineLayout(
        renderer->device, renderer->debugDrawDescriptorSetLayout, false);

    // Create pipeline
    VulkanPipelineDefinition pipelineDefinition = {};
    pipelineDefinition.vertexStride = sizeof(VertexPNT);
    pipelineDefinition.primitive = Primitive_Triangle;
    pipelineDefinition.polygonMode = PolygonMode_Fill;
    pipelineDefinition.cullMode = CullMode_Back;
    pipelineDefinition.depthTestEnabled = true;
    pipelineDefinition.depthWriteEnabled = true;

    renderer->pipeline = VulkanCreatePipeline(renderer->device,
        renderer->pipelineCache, renderer->hdrRenderPass,
        renderer->pipelineLayout, renderer->testVertexShader,
        renderer->testFragmentShader, &pipelineDefinition);

    VulkanPipelineDefinition postProcessingPipelineDefinition = {};
    postProcessingPipelineDefinition.vertexStride = sizeof(VertexPNT);
    postProcessingPipelineDefinition.primitive = Primitive_Triangle;
    postProcessingPipelineDefinition.polygonMode = PolygonMode_Fill;
    postProcessingPipelineDefinition.cullMode = CullMode_None;
    postProcessingPipelineDefinition.depthTestEnabled = false;
    postProcessingPipelineDefinition.depthWriteEnabled = false;

    renderer->postProcessPipeline = VulkanCreatePipeline(renderer->device,
        renderer->pipelineCache, renderer->renderPass, renderer->pipelineLayout,
        renderer->fullscreenQuadVertexShader,
        renderer->postProcessingFragmentShader,
        &postProcessingPipelineDefinition);

    // Create debug draw pipeline
    VulkanPipelineDefinition debugDrawPipelineDefinition = {};
    debugDrawPipelineDefinition.vertexStride = sizeof(VertexPC);
    debugDrawPipelineDefinition.primitive = Primitive_Line;
    debugDrawPipelineDefinition.polygonMode = PolygonMode_Fill;
    debugDrawPipelineDefinition.cullMode = CullMode_None;
    debugDrawPipelineDefinition.depthTestEnabled = false;
    debugDrawPipelineDefinition.depthWriteEnabled = false;

    renderer->debugDrawPipeline = VulkanCreatePipeline(renderer->device,
        renderer->pipelineCache, renderer->hdrRenderPass,
        renderer->debugDrawPipelineLayout, renderer->debugDrawVertexShader,
        renderer->debugDrawFragmentShader, &debugDrawPipelineDefinition);


    // Skybox pipeline
    VulkanPipelineDefinition skyboxPipelineDefinition = {};
    skyboxPipelineDefinition.vertexStride = sizeof(VertexPNT);
    skyboxPipelineDefinition.primitive = Primitive_Triangle;
    skyboxPipelineDefinition.polygonMode = PolygonMode_Fill;
    skyboxPipelineDefinition.cullMode = CullMode_Front;
    skyboxPipelineDefinition.depthTestEnabled = false;
    skyboxPipelineDefinition.depthWriteEnabled = false;

    renderer->skyboxPipeline = VulkanCreatePipeline(renderer->device,
        renderer->pipelineCache, renderer->hdrRenderPass,
        renderer->pipelineLayout, renderer->testVertexShader,
        renderer->skyboxFragmentShader, &skyboxPipelineDefinition);

    renderer->convertEquirectangular = CreateComputeShaderPipeline(
        renderer, "convert_equirectangular.comp.spv");
    renderer->generateIrradiance = CreateComputeShaderPipeline(
            renderer, "irradiance_cubemap.comp.spv");

    // Allocate descriptor sets
    {
        VkDescriptorSetLayout layouts[2] = {
            renderer->descriptorSetLayout, renderer->descriptorSetLayout};
        Assert(ArrayCount(layouts) == ArrayCount(renderer->descriptorSets));
        VulkanAllocateDescriptorSets(renderer->device, renderer->descriptorPool,
            layouts, ArrayCount(layouts), renderer->descriptorSets);
    }

    {
        VkDescriptorSetLayout layouts[2] = {
            renderer->descriptorSetLayout, renderer->descriptorSetLayout};
        Assert(ArrayCount(layouts) ==
               ArrayCount(renderer->postProcessDescriptorSets));
        VulkanAllocateDescriptorSets(renderer->device, renderer->descriptorPool,
            layouts, ArrayCount(layouts), renderer->postProcessDescriptorSets);
    }

    // Allocate debug draw descriptor sets
    {
        VkDescriptorSetLayout layouts[2] = {
            renderer->debugDrawDescriptorSetLayout,
            renderer->debugDrawDescriptorSetLayout};
        Assert(ArrayCount(layouts) == ArrayCount(renderer->debugDrawDescriptorSets));
        VulkanAllocateDescriptorSets(renderer->device, renderer->descriptorPool,
            layouts, ArrayCount(layouts), renderer->debugDrawDescriptorSets);
    }

    // Populate vertex data buffer
    VertexPNT *vertices = (VertexPNT *)renderer->vertexDataUploadBuffer.data;
    vertices[0].position = Vec3(-0.5, -0.5, 0.0);
    vertices[0].normal = Vec3(0.0, 0.0, 1.0);
    vertices[0].textureCoord = Vec2(0.0, 0.0);
    vertices[1].position = Vec3(0.5, -0.5, 0.0);
    vertices[1].normal = Vec3(0.0, 0.0, 1.0);
    vertices[1].textureCoord = Vec2(1.0, 0.0);
    vertices[2].position = Vec3(0.0, 0.5, 0.0);
    vertices[2].normal = Vec3(0.0, 0.0, 1.0);
    vertices[2].textureCoord = Vec2(0.0, 1.0);

    VulkanCopyBuffer(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->vertexDataUploadBuffer.handle,
        renderer->vertexDataBuffer.handle, sizeof(VertexPNT) * 3);

    // Populate index buffer
    u32 *indices = (u32 *)renderer->indexUploadBuffer.data;
    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;

    VulkanCopyBuffer(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->indexUploadBuffer.handle,
        renderer->indexBuffer.handle, sizeof(u32) * 3);

    // Create image from CPU ray tracer
    {
        u32 width = RAY_TRACER_WIDTH;
        u32 height = RAY_TRACER_HEIGHT;
        VkFormat format = VK_FORMAT_R32G32B32A32_SFLOAT;
        VulkanImage image = VulkanCreateImage(
            renderer->device, renderer->physicalDevice, width, height, format,
            VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
            VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_COLOR_BIT);
        renderer->cpuPathTracerImage = image;

        VulkanTransitionImageLayout(image.handle,
            VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            renderer->device, renderer->commandPool, renderer->graphicsQueue);
    }

    UpdateMeshDescriptorSets(renderer);
    UpdatePostProcessingDescriptorSets(renderer);
    UpdateDebugDrawDescriptorSets(renderer);
}

internal void WriteMaterialDescriptorSet(VulkanRenderer *renderer,
    VkDescriptorSet dstSet, VulkanImage *albedoTexture, VulkanImage *cubeMap,
    VulkanImage *irradianceMap, VulkanImage shadowMap)
{
    VkDescriptorBufferInfo uniformBufferInfo = {};
    uniformBufferInfo.buffer = renderer->uniformBuffer.handle;
    uniformBufferInfo.range = VK_WHOLE_SIZE;

    VkDescriptorBufferInfo vertexDataBufferInfo = {};
    vertexDataBufferInfo.buffer = renderer->vertexDataBuffer.handle;
    vertexDataBufferInfo.range = VK_WHOLE_SIZE;

    VkDescriptorBufferInfo modelMatrixBufferInfo = {};
    modelMatrixBufferInfo.buffer = renderer->modelMatricesBuffer.handle;
    modelMatrixBufferInfo.range = VK_WHOLE_SIZE;

    VkDescriptorBufferInfo lightBufferInfo = {};
    lightBufferInfo.buffer = renderer->lightBuffer.handle;
    lightBufferInfo.range = VK_WHOLE_SIZE;

    // TODO: Don't think we need this one
    VkDescriptorImageInfo imageInfo = {};
    imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    imageInfo.imageView = albedoTexture->view;

    VkDescriptorImageInfo albedoTextureInfo = {};
    albedoTextureInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    albedoTextureInfo.imageView = albedoTexture->view;

    VkDescriptorImageInfo cubeMapInfo = {};
    cubeMapInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    cubeMapInfo.imageView = cubeMap->view;

    VkDescriptorImageInfo irradianceMapInfo = {};
    irradianceMapInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    irradianceMapInfo.imageView = irradianceMap->view;

    VkDescriptorImageInfo shadowMapInfo = {};
    shadowMapInfo.sampler = renderer->defaultSampler; // TODO: Shadow specific sampler?
    shadowMapInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    shadowMapInfo.imageView = shadowMap.view;

    VkWriteDescriptorSet descriptorWrites[9] = {};
    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = dstSet;
    descriptorWrites[0].dstBinding = 0;
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pBufferInfo = &uniformBufferInfo;
    descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[1].dstSet = dstSet;
    descriptorWrites[1].dstBinding = 1;
    descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorWrites[1].descriptorCount = 1;
    descriptorWrites[1].pBufferInfo = &vertexDataBufferInfo;
    // Binding 2 is for the defaultSampler
    descriptorWrites[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[2].dstSet = dstSet;
    descriptorWrites[2].dstBinding = 3;
    descriptorWrites[2].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    descriptorWrites[2].descriptorCount = 1;
    descriptorWrites[2].pImageInfo = &imageInfo;
    descriptorWrites[3].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[3].dstSet = dstSet;
    descriptorWrites[3].dstBinding = 4;
    descriptorWrites[3].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorWrites[3].descriptorCount = 1;
    descriptorWrites[3].pBufferInfo = &modelMatrixBufferInfo;
    descriptorWrites[4].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[4].dstSet = dstSet;
    descriptorWrites[4].dstBinding = 10;
    descriptorWrites[4].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorWrites[4].descriptorCount = 1;
    descriptorWrites[4].pBufferInfo = &lightBufferInfo;
    descriptorWrites[5].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[5].dstSet = dstSet;
    descriptorWrites[5].dstBinding = 8;
    descriptorWrites[5].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    descriptorWrites[5].descriptorCount = 1;
    descriptorWrites[5].pImageInfo = &albedoTextureInfo;
    descriptorWrites[6].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[6].dstSet = dstSet;
    descriptorWrites[6].dstBinding = 6;
    descriptorWrites[6].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    descriptorWrites[6].descriptorCount = 1;
    descriptorWrites[6].pImageInfo = &cubeMapInfo;
    descriptorWrites[7].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[7].dstSet = dstSet;
    descriptorWrites[7].dstBinding = 7;
    descriptorWrites[7].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    descriptorWrites[7].descriptorCount = 1;
    descriptorWrites[7].pImageInfo = &irradianceMapInfo;
    descriptorWrites[8].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[8].dstSet = dstSet;
    descriptorWrites[8].dstBinding = 11;
    descriptorWrites[8].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptorWrites[8].descriptorCount = 1;
    descriptorWrites[8].pImageInfo = &shadowMapInfo;

    vkUpdateDescriptorSets(renderer->device, ArrayCount(descriptorWrites),
        descriptorWrites, 0, NULL);
}

internal void WriteShadowDescriptorSet(
    VulkanRenderer *renderer, VkDescriptorSet dstSet)
{
    VkDescriptorBufferInfo uniformBufferInfo = {};
    uniformBufferInfo.buffer = renderer->uniformBuffer.handle;
    uniformBufferInfo.range = VK_WHOLE_SIZE;

    VkDescriptorBufferInfo vertexDataBufferInfo = {};
    vertexDataBufferInfo.buffer = renderer->vertexDataBuffer.handle;
    vertexDataBufferInfo.range = VK_WHOLE_SIZE;

    VkDescriptorBufferInfo modelMatrixBufferInfo = {};
    modelMatrixBufferInfo.buffer = renderer->modelMatricesBuffer.handle;
    modelMatrixBufferInfo.range = VK_WHOLE_SIZE;

    VkWriteDescriptorSet descriptorWrites[3] = {};
    descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[0].dstSet = dstSet;
    descriptorWrites[0].dstBinding = 0;
    descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptorWrites[0].descriptorCount = 1;
    descriptorWrites[0].pBufferInfo = &uniformBufferInfo;

    descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[1].dstSet = dstSet;
    descriptorWrites[1].dstBinding = 1;
    descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorWrites[1].descriptorCount = 1;
    descriptorWrites[1].pBufferInfo = &vertexDataBufferInfo;

    descriptorWrites[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrites[2].dstSet = dstSet;
    descriptorWrites[2].dstBinding = 4;
    descriptorWrites[2].descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    descriptorWrites[2].descriptorCount = 1;
    descriptorWrites[2].pBufferInfo = &modelMatrixBufferInfo;

    vkUpdateDescriptorSets(renderer->device, ArrayCount(descriptorWrites),
        descriptorWrites, 0, NULL);
}

// TODO: Eventually want to do 'pull-model' where the rasterizer polls the
// state of the core data model and syncs the vulkan state to match
internal void VulkanCreateMaterial(VulkanRenderer *renderer, u32 id,
    u32 albedoTextureId, u32 cubeMapId, u32 irradianceMapId)
{
    VulkanImage *albedoTexture = VulkanFindTextureById(renderer, albedoTextureId);
    Assert(albedoTexture != NULL);

    VulkanImage *cubeMap = VulkanFindTextureById(renderer, cubeMapId);
    Assert(cubeMap != NULL);

    VulkanImage *irradianceMap = VulkanFindTextureById(renderer, irradianceMapId);
    Assert(irradianceMap != NULL);

    VulkanMaterial material = {};

    // Allocate descriptor sets
    {
        VkDescriptorSetLayout layouts[2] = {
            renderer->descriptorSetLayout, renderer->descriptorSetLayout};
        Assert(ArrayCount(layouts) == ArrayCount(material.descriptorSets));

        VulkanAllocateDescriptorSets(renderer->device, renderer->descriptorPool,
            layouts, ArrayCount(layouts), material.descriptorSets);

        VkDescriptorSetLayout shadowDescriptorSetLayouts[2] = {
            renderer->shadowDescriptorSetLayout, renderer->shadowDescriptorSetLayout};
        Assert(ArrayCount(shadowDescriptorSetLayouts) ==
               ArrayCount(material.shadowDescriptorSets));
        VulkanAllocateDescriptorSets(renderer->device, renderer->descriptorPool,
            shadowDescriptorSetLayouts, ArrayCount(shadowDescriptorSetLayouts),
            material.shadowDescriptorSets);
    }

    // Create pipeline
    VulkanPipelineDefinition pipelineDefinition = {};
    pipelineDefinition.vertexStride = sizeof(VertexPNT);
    pipelineDefinition.primitive = Primitive_Triangle;
    pipelineDefinition.polygonMode = PolygonMode_Fill;
    pipelineDefinition.cullMode = CullMode_Back;
    pipelineDefinition.depthTestEnabled = true;
    pipelineDefinition.depthWriteEnabled = true;

    material.pipeline = VulkanCreatePipeline(renderer->device,
        renderer->pipelineCache, renderer->hdrRenderPass,
        renderer->pipelineLayout, renderer->testVertexShader,
        renderer->testFragmentShader, &pipelineDefinition);

    material.shadowPipeline = VulkanCreatePipeline(renderer->device,
        renderer->pipelineCache, renderer->shadowPass.renderPass,
        renderer->shadowPipelineLayout, renderer->testVertexShader,
        renderer->shadowFragmentShader, &pipelineDefinition);

    // Update descriptor sets
    Assert(renderer->swapchain.imageCount == 2);
    for (u32 i = 0; i < renderer->swapchain.imageCount; ++i)
    {
        WriteMaterialDescriptorSet(renderer, material.descriptorSets[i],
            albedoTexture, cubeMap, irradianceMap,
            renderer->shadowPass.buffers[i].depth);

        WriteShadowDescriptorSet(renderer, material.shadowDescriptorSets[i]);
    }

    StoreVulkanMaterial(renderer, id, material);
}

internal void DrawMeshes(VulkanRenderer *renderer, Scene scene, u32 imageIndex,
    b32 useShadowPipeline = false, u32 layer = 0)
{
    // Draw mesh
    for (u32 i = 0; i < scene.count; ++i)
    {
        u32 meshId = scene.entities[i].mesh;
        Mesh *mesh = VulkanFindMeshById(renderer, meshId);
        Assert(mesh != NULL);
        u32 materialId = scene.entities[i].material;

        VulkanMaterial *material = VulkanFindMaterialById(renderer, materialId);
        if (material != NULL)
        {
            VkPipeline pipeline = material->pipeline;
            VkPipelineLayout pipelineLayout = renderer->pipelineLayout;
            VkDescriptorSet descriptorSet =
                material->descriptorSets[imageIndex];

            u32 cameraIndex = CameraIndex_Default;

            if (useShadowPipeline)
            {
                pipeline = material->shadowPipeline;
                pipelineLayout = renderer->shadowPipelineLayout;
                descriptorSet = material->shadowDescriptorSets[imageIndex];
                cameraIndex = CameraIndex_ShadowPass + layer;
            }

            vkCmdBindPipeline(renderer->commandBuffer,
                VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

            vkCmdBindDescriptorSets(renderer->commandBuffer,
                VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1,
                &descriptorSet, 0, NULL);

            // Bind index buffer
            vkCmdBindIndexBuffer(renderer->commandBuffer,
                renderer->indexBuffer.handle, mesh->indexDataOffset,
                VK_INDEX_TYPE_UINT32);

            MeshPushConstants pushConstants = {};
            pushConstants.modelMatrixIndex =
                i + 1; // FIXME: 0 is reserved for skybox
            pushConstants.vertexDataOffset = mesh->vertexDataOffset;
            pushConstants.cameraIndex = cameraIndex;
            pushConstants.layer = layer;

            vkCmdPushConstants(renderer->commandBuffer,
                renderer->pipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0,
                sizeof(pushConstants), &pushConstants);

            vkCmdDrawIndexed(
                renderer->commandBuffer, mesh->indexCount, 1, 0, 0, 0);
        }
    }
}

internal void ExecuteShadowPass(VulkanRenderer *renderer, Scene scene, u32 imageIndex)
{
    VulkanShadowPass shadowPass = renderer->shadowPass;
    VkCommandBuffer commandBuffer = renderer->commandBuffer;

    VkClearValue clearValue = {};
    clearValue.depthStencil = {1.0f, 0};

    VkRenderPassBeginInfo renderPassBegin = {
        VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};
    renderPassBegin.renderPass = shadowPass.renderPass;
    renderPassBegin.framebuffer = shadowPass.buffers[imageIndex].handle;
    renderPassBegin.renderArea.extent.width = shadowPass.width;
    renderPassBegin.renderArea.extent.height = shadowPass.height;
    renderPassBegin.clearValueCount = 1;
    renderPassBegin.pClearValues = &clearValue;
    vkCmdBeginRenderPass(
        commandBuffer, &renderPassBegin, VK_SUBPASS_CONTENTS_INLINE);

    // Set dynamic pipeline state
    VkViewport viewport = {
        0, 0, (f32)shadowPass.width, (f32)shadowPass.height, 0.0f, 1.0f};
    VkRect2D scissor = {0, 0, shadowPass.width, shadowPass.height};
    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);
    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    for (u32 i = 0; i < scene.lightData->directionalLightCount; i++)
    {
        DrawMeshes(renderer, scene, imageIndex, true, i);
    }

    vkCmdEndRenderPass(commandBuffer);
}

internal void VulkanRender(
    VulkanRenderer *renderer, u32 outputFlags, Scene scene,
    HdrImage *testImageOutput = NULL)
{
    UploadSceneDataToGpu(renderer, scene);

    u32 imageIndex;
    VK_CHECK(vkAcquireNextImageKHR(renderer->device, renderer->swapchain.handle,
        0, renderer->acquireSemaphore, VK_NULL_HANDLE, &imageIndex));

    VK_CHECK(vkResetCommandPool(renderer->device, renderer->commandPool, 0));

    VkCommandBufferBeginInfo beginInfo = {
        VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO};
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    VK_CHECK(vkBeginCommandBuffer(renderer->commandBuffer, &beginInfo));

    // Execution barrier
    vkCmdPipelineBarrier(renderer->commandBuffer,
        VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
        VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
        0, 0, NULL, 0, NULL, 0, NULL);

    ExecuteShadowPass(renderer, scene, imageIndex);

    // Begin render pass
    VkClearValue clearValues[2] = {};
    clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
    clearValues[1].depthStencil = {1.0f, 0};

    VkRenderPassBeginInfo renderPassBegin = {
        VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};
    renderPassBegin.renderPass = renderer->hdrRenderPass;
    renderPassBegin.framebuffer = renderer->hdrSwapchain.framebuffers[imageIndex].handle;
    renderPassBegin.renderArea.extent.width = renderer->swapchain.width;
    renderPassBegin.renderArea.extent.height = renderer->swapchain.height;
    renderPassBegin.clearValueCount = ArrayCount(clearValues);
    renderPassBegin.pClearValues = clearValues;
    vkCmdBeginRenderPass(
        renderer->commandBuffer, &renderPassBegin, VK_SUBPASS_CONTENTS_INLINE);

    // Set dynamic pipeline state
    VkViewport viewport = {0, 0, (f32)renderer->swapchain.width,
        (f32)renderer->swapchain.height, 0.0f, 1.0f};
    VkRect2D scissor = {
        0, 0, renderer->swapchain.width, renderer->swapchain.height};
    vkCmdSetViewport(renderer->commandBuffer, 0, 1, &viewport);
    vkCmdSetScissor(renderer->commandBuffer, 0, 1, &scissor);

    // Perform scene render pass
    // Bind pipeline
#if 1
    // Draw skybox
    {
        u32 materialIndex = scene.backgroundMaterial;
        Mesh *mesh = VulkanFindMeshById(renderer, Mesh_Cube);
        Assert(mesh != NULL);

        vkCmdBindPipeline(renderer->commandBuffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS, renderer->skyboxPipeline);

        vkCmdBindDescriptorSets(renderer->commandBuffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS, renderer->pipelineLayout, 0, 1,
            &renderer->descriptorSets[imageIndex], 0, NULL);

        // FIXME: Code dup
        vkCmdBindIndexBuffer(renderer->commandBuffer,
            renderer->indexBuffer.handle, mesh->indexDataOffset,
            VK_INDEX_TYPE_UINT32);

        MeshPushConstants pushConstants = {};
        pushConstants.modelMatrixIndex = 0;
        pushConstants.vertexDataOffset = mesh->vertexDataOffset;
        pushConstants.cameraIndex = CameraIndex_Skybox;
        pushConstants.layer = 0;

        vkCmdPushConstants(renderer->commandBuffer, renderer->pipelineLayout,
            VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(pushConstants),
            &pushConstants);

        vkCmdDrawIndexed(renderer->commandBuffer, mesh->indexCount, 1, 0, 0, 0);
    }
#endif

    // Draw mesh
    DrawMeshes(renderer, scene, imageIndex);

    // Draw debug buffer
    if (outputFlags & Output_ShowDebugDrawing)
    {
        vkCmdBindDescriptorSets(renderer->commandBuffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS, renderer->debugDrawPipelineLayout,
            0, 1, &renderer->debugDrawDescriptorSets[imageIndex], 0, NULL);

        vkCmdBindPipeline(renderer->commandBuffer,
            VK_PIPELINE_BIND_POINT_GRAPHICS, renderer->debugDrawPipeline);

        vkCmdDraw(
            renderer->commandBuffer, renderer->debugDrawVertexCount, 1, 0, 0);
    }

    vkCmdEndRenderPass(renderer->commandBuffer);
    // Perform post-processing pass
    VkRenderPassBeginInfo renderPassPresentBegin = {
        VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO};
    renderPassPresentBegin.renderPass = renderer->renderPass;
    renderPassPresentBegin.framebuffer =
        renderer->swapchain.framebuffers[imageIndex];
    renderPassPresentBegin.renderArea.extent.width = renderer->swapchain.width;
    renderPassPresentBegin.renderArea.extent.height =
        renderer->swapchain.height;
    renderPassPresentBegin.clearValueCount = ArrayCount(clearValues);
    renderPassPresentBegin.pClearValues = clearValues;
    vkCmdBeginRenderPass(renderer->commandBuffer, &renderPassPresentBegin,
        VK_SUBPASS_CONTENTS_INLINE);

    // Set dynamic pipeline state
    VkViewport viewportPresent = {0, 0, (f32)renderer->swapchain.width,
        (f32)renderer->swapchain.height, 0.0f, 1.0f};
    VkRect2D scissorPresent = {
        0, 0, renderer->swapchain.width, renderer->swapchain.height};
    vkCmdSetViewport(renderer->commandBuffer, 0, 1, &viewportPresent);
    vkCmdSetScissor(renderer->commandBuffer, 0, 1, &scissorPresent);

    // Bind pipeline
    vkCmdBindDescriptorSets(renderer->commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS, renderer->pipelineLayout, 0, 1,
        &renderer->postProcessDescriptorSets[imageIndex], 0, NULL);

    vkCmdBindPipeline(renderer->commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS, renderer->postProcessPipeline);

    // Draw mesh
    vkCmdDraw(renderer->commandBuffer, 6, 1, 0, 0);

    vkCmdEndRenderPass(renderer->commandBuffer);

    VK_CHECK(vkEndCommandBuffer(renderer->commandBuffer));

    VkPipelineStageFlags submitStageMask =
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo = {VK_STRUCTURE_TYPE_SUBMIT_INFO};
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &renderer->acquireSemaphore;
    submitInfo.pWaitDstStageMask = &submitStageMask;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &renderer->commandBuffer;
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &renderer->releaseSemaphore;
    VK_CHECK(vkQueueSubmit(renderer->graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE));

    VkPresentInfoKHR presentInfo = {VK_STRUCTURE_TYPE_PRESENT_INFO_KHR};
    presentInfo.swapchainCount = 1;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &renderer->releaseSemaphore;
    presentInfo.pSwapchains = &renderer->swapchain.handle;
    presentInfo.pImageIndices = &imageIndex;
    VK_CHECK(vkQueuePresentKHR(renderer->presentQueue, &presentInfo));

    // FIXME: Remove this to allow CPU and GPU to run in parallel
    VK_CHECK(vkDeviceWaitIdle(renderer->device));

    b32 testMode = testImageOutput != NULL;
    if (testMode)
    {
        // Grab last swapchain image
        VkImage srcImage = renderer->hdrSwapchain.framebuffers[imageIndex].color.handle;
        VkImage dstImage = VK_NULL_HANDLE;
        VkDeviceMemory dstImageMemory = VK_NULL_HANDLE;
        {
            VkImageCreateInfo createInfo = {
                VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO};
            createInfo.imageType = VK_IMAGE_TYPE_2D;
            createInfo.extent.width = testImageOutput->width;
            createInfo.extent.height = testImageOutput->height;
            createInfo.extent.depth = 1;
            createInfo.mipLevels = 1;
            createInfo.arrayLayers = 1;
            createInfo.format = VK_FORMAT_R32G32B32A32_SFLOAT;
            createInfo.tiling = VK_IMAGE_TILING_LINEAR;
            createInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            createInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
            createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
            createInfo.samples = VK_SAMPLE_COUNT_1_BIT;
            VK_CHECK(
                vkCreateImage(renderer->device, &createInfo, NULL, &dstImage));

            VkMemoryRequirements memoryRequirements = {};
            vkGetImageMemoryRequirements(
                renderer->device, dstImage, &memoryRequirements);

            VkMemoryAllocateInfo allocInfo = {
                VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO};
            allocInfo.allocationSize = memoryRequirements.size;
            allocInfo.memoryTypeIndex = VulkanFindMemoryType(
                renderer->physicalDevice, memoryRequirements.memoryTypeBits,
                VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT |
                    VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

            VK_CHECK(vkAllocateMemory(
                renderer->device, &allocInfo, NULL, &dstImageMemory));

            vkBindImageMemory(renderer->device, dstImage, dstImageMemory, 0);
        }

        VkCommandBuffer commandBuffer =
            VulkanBeginSingleTimeCommands(renderer->device, renderer->commandPool);

        // Transition dst image from undefined layout to dst_optimal layout
        {
            VkImageMemoryBarrier barrier = {VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
            barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
            barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.image = dstImage;
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            barrier.subresourceRange.baseMipLevel = 0;
            barrier.subresourceRange.levelCount = 1;
            barrier.subresourceRange.baseArrayLayer = 0;
            barrier.subresourceRange.layerCount = 1;
            barrier.srcAccessMask = 0;
            barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

            vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT,
                VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1,
                &barrier);
        }

        // Transition srcImage from shader read-only layout to transfer src layout
        {
            VkImageMemoryBarrier barrier = {VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
            barrier.oldLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
            barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.image = srcImage;
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            barrier.subresourceRange.baseMipLevel = 0;
            barrier.subresourceRange.levelCount = 1;
            barrier.subresourceRange.baseArrayLayer = 0;
            barrier.subresourceRange.layerCount = 1;
            barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
            barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

            vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT,
                VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1,
                &barrier);
        }

        // TODO: See if we can use blit

        VkImageCopy imageCopyRegion = {};
        imageCopyRegion.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageCopyRegion.srcSubresource.layerCount = 1;
        imageCopyRegion.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        imageCopyRegion.dstSubresource.layerCount = 1;
        imageCopyRegion.extent.width = testImageOutput->width;
        imageCopyRegion.extent.height = testImageOutput->height;
        imageCopyRegion.extent.depth = 1;

        // Issue the copy command
        vkCmdCopyImage(commandBuffer, srcImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            dstImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1,
            &imageCopyRegion);

        // Transition dest image to general layout so we can map into host memory
        {
            VkImageMemoryBarrier barrier = {VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
            barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
            barrier.newLayout = VK_IMAGE_LAYOUT_GENERAL;
            barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.image = dstImage;
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            barrier.subresourceRange.baseMipLevel = 0;
            barrier.subresourceRange.levelCount = 1;
            barrier.subresourceRange.baseArrayLayer = 0;
            barrier.subresourceRange.layerCount = 1;
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;

            vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT,
                VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1,
                &barrier);
        }

        // Transition swapchain image back to shader read only layout
        {
            VkImageMemoryBarrier barrier = {VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER};
            barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
            barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
            barrier.image = srcImage;
            barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            barrier.subresourceRange.baseMipLevel = 0;
            barrier.subresourceRange.levelCount = 1;
            barrier.subresourceRange.baseArrayLayer = 0;
            barrier.subresourceRange.layerCount = 1;
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT;

            vkCmdPipelineBarrier(commandBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT,
                VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, NULL, 0, NULL, 1,
                &barrier);
        }

        VulkanEndSingleTimeCommands(commandBuffer, renderer->device,
            renderer->commandPool, renderer->graphicsQueue);

        // Get layout of the image (including row pitch)
        VkImageSubresource subResource = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 0};
        VkSubresourceLayout subResourceLayout = {};
        vkGetImageSubresourceLayout(
            renderer->device, dstImage, &subResource, &subResourceLayout);

        // Map memory
        void *data = NULL;
        vkMapMemory(renderer->device, dstImageMemory, 0, VK_WHOLE_SIZE, 0, &data);
        data = (u8 *)data + subResourceLayout.offset;

        Assert(
            subResourceLayout.size <=
            testImageOutput->width * testImageOutput->height * sizeof(f32) * 4);
        CopyMemory(testImageOutput->pixels, data, (u32)subResourceLayout.size);

        vkUnmapMemory(renderer->device, dstImageMemory);
        vkDestroyImage(renderer->device, dstImage, NULL);
        vkFreeMemory(renderer->device, dstImageMemory, NULL);
    }

}

internal void VulkanFramebufferResize(
    VulkanRenderer *renderer, u32 framebufferWidth, u32 framebufferHeight)
{
    // Make doubly sure no resources are still in use
    vkDeviceWaitIdle(renderer->device);

    // Destroy swapchain
    VulkanDestroySwapchain(renderer->swapchain, renderer->device);
    DestroyHdrSwapchain(renderer->device, renderer->hdrSwapchain);

    // Recreate swapchain
    renderer->swapchain =
        VulkanSetupSwapchain(renderer->device, renderer->physicalDevice,
            renderer->surface, renderer->queueFamilyIndices,
            renderer->renderPass, 2, framebufferWidth, framebufferHeight);

    // TODO: Config option to support 16-bit floats?
    VkFormat linearFormat = VK_FORMAT_R32G32B32A32_SFLOAT;

    renderer->hdrSwapchain = CreateHdrSwapchain(renderer->device,
        renderer->physicalDevice, linearFormat, framebufferWidth,
        framebufferHeight, 2, renderer->hdrRenderPass);

    // Update descriptor sets to point to new image views
    UpdatePostProcessingDescriptorSets(renderer);
}

internal void CopyMeshDataToUploadBuffer(
    VulkanRenderer *renderer, MeshData meshData, u32 meshId)
{
    Mesh mesh = {};

    // Vertex data
    CopyMemory((u8 *)renderer->vertexDataUploadBuffer.data +
                   renderer->vertexDataUploadBufferSize,
        meshData.vertices, sizeof(VertexPNT) * meshData.vertexCount);
    mesh.vertexDataOffset =
        renderer->vertexDataUploadBufferSize / sizeof(VertexPNT);
    renderer->vertexDataUploadBufferSize +=
        sizeof(VertexPNT) * meshData.vertexCount;

    // Index data
    CopyMemory((u8 *)renderer->indexUploadBuffer.data +
                   renderer->indexUploadBufferSize,
        meshData.indices, sizeof(u32) * meshData.indexCount);
    mesh.indexDataOffset = renderer->indexUploadBufferSize;
    renderer->indexUploadBufferSize += sizeof(u32) * meshData.indexCount;

    mesh.indexCount = meshData.indexCount;

    StoreVulkanMesh(renderer, meshId, mesh);
}

internal void UploadHdrImageToGPU(
    VulkanRenderer *renderer, HdrImage image, u32 imageId, u32 dstBinding)
{
    // Create image
    VkFormat format = VK_FORMAT_R32G32B32A32_SFLOAT;
    VulkanImage texture = VulkanCreateImage(renderer->device,
        renderer->physicalDevice, image.width, image.height, format,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_COLOR_BIT);

    StoreVulkanTexture(renderer, imageId, texture);

    // Map memory
    MemoryArena textureUploadArena = {};
    InitializeMemoryArena(&textureUploadArena,
        renderer->textureUploadBuffer.data, TEXTURE_UPLOAD_BUFFER_SIZE);

    // Copy data to upload buffer
    f32 *pixels =
        AllocateArray(&textureUploadArena, f32, image.width * image.height * 4);
    CopyMemory(
        pixels, image.pixels, image.width * image.height * sizeof(f32) * 4);

    // Update image
    VulkanTransitionImageLayout(texture.handle,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, renderer->device,
        renderer->commandPool, renderer->graphicsQueue);

    VulkanCopyBufferToImage(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->textureUploadBuffer.handle,
        texture.handle, image.width, image.height, 0);

    VulkanTransitionImageLayout(texture.handle,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, renderer->device,
        renderer->commandPool, renderer->graphicsQueue);


    // FIXME: This seems horrific!
    Assert(renderer->swapchain.imageCount == 2);
    for (u32 i = 0; i < renderer->swapchain.imageCount; ++i)
    {
        VkDescriptorImageInfo imageInfo = {};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = texture.view;

        VkWriteDescriptorSet descriptorWrites[1] = {};
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = renderer->descriptorSets[i];
        descriptorWrites[0].dstBinding = dstBinding;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pImageInfo = &imageInfo;
        vkUpdateDescriptorSets(renderer->device, ArrayCount(descriptorWrites),
            descriptorWrites, 0, NULL);
    }
}

internal void UploadCubeMapToGPU(VulkanRenderer *renderer, HdrCubeMap cubeMap,
    u32 imageId, u32 dstBinding, u32 width, u32 height)
{
    u32 bytesPerPixel = sizeof(f32) * 4; // Using VK_FORMAT_R32G32B32A32_SFLOAT

    // Create image
    VkFormat format = VK_FORMAT_R32G32B32A32_SFLOAT;
    VulkanImage texture = VulkanCreateImage(renderer->device,
        renderer->physicalDevice, width, height, format,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_ASPECT_COLOR_BIT, true);

    StoreVulkanTexture(renderer, imageId, texture);

    // Allocate cube map from upload buffer
    MemoryArena textureUploadArena = {};
    InitializeMemoryArena(&textureUploadArena,
        renderer->textureUploadBuffer.data, TEXTURE_UPLOAD_BUFFER_SIZE);

    u32 layerCount = 6;
    void *pixels = AllocateBytes(
        &textureUploadArena, width * height * bytesPerPixel * layerCount);

    for (u32 layerIndex = 0; layerIndex < layerCount; ++layerIndex)
    {
        // Map layer index to basis vectors for cube map face
        BasisVectors basis =
            MapCubeMapLayerIndexToBasisVectors(layerIndex);

        HdrImage *srcImage = cubeMap.images + layerIndex;

        u32 layerOffset = layerIndex * width * height * bytesPerPixel;
        void *dst = (u8 *)pixels + layerOffset;

        CopyMemory(dst, srcImage->pixels, width * height * bytesPerPixel);
    }

    // Submit cube map data for upload to GPU
    VulkanTransitionImageLayout(texture.handle,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, renderer->device,
        renderer->commandPool, renderer->graphicsQueue, true);

    VulkanCopyBufferToImage(renderer->device, renderer->commandPool,
        renderer->graphicsQueue, renderer->textureUploadBuffer.handle,
        texture.handle, width, height, 0, true);

    VulkanTransitionImageLayout(texture.handle,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, renderer->device,
        renderer->commandPool, renderer->graphicsQueue, true);

    for (u32 i = 0; i < renderer->swapchain.imageCount; ++i)
    {
        VkDescriptorImageInfo imageInfo = {};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = texture.view;

        VkWriteDescriptorSet descriptorWrites[1] = {};
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = renderer->descriptorSets[i];
        descriptorWrites[0].dstBinding = dstBinding;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pImageInfo = &imageInfo;
        vkUpdateDescriptorSets(renderer->device, ArrayCount(descriptorWrites),
            descriptorWrites, 0, NULL);
    }
}

internal void VulkanClearTextures(VulkanRenderer *renderer)
{
    for (u32 i = 0; i < renderer->textures.count; i++)
    {
        u32 key = renderer->textures.keys[i];
        VulkanImage *texture =
            Dict_FindItem(&renderer->textures, VulkanImage, key);
        VulkanDestroyImage(renderer->device, *texture);
    }

    Dict_Clear(&renderer->textures);
}

internal void VulkanDestroyMaterial(
    VulkanRenderer *renderer, VulkanMaterial material)
{
    vkDestroyPipeline(renderer->device, material.pipeline, NULL);
    vkFreeDescriptorSets(
        renderer->device, renderer->descriptorPool, 2, material.descriptorSets);
}

internal void VulkanClearMaterials(VulkanRenderer *renderer)
{
    for (u32 i = 0; i < renderer->materials.count; i++)
    {
        u32 key = renderer->materials.keys[i];
        VulkanMaterial *material =
            Dict_FindItem(&renderer->materials, VulkanMaterial, key);
        VulkanDestroyMaterial(renderer, *material);
    }

    Dict_Clear(&renderer->materials);
}

internal void VulkanClearMeshes(VulkanRenderer *renderer)
{
    // Reset write cursor for upload buffers
    renderer->vertexDataUploadBufferSize = 0;
    renderer->indexUploadBufferSize = 0;

    Dict_Clear(&renderer->meshes);
}

internal void VulkanClearResources(VulkanRenderer *renderer)
{
    VulkanClearMaterials(renderer);
    VulkanClearTextures(renderer);
    VulkanClearMeshes(renderer);
}

internal void DispatchComputeShader(VkDevice device, VkCommandPool commandPool,
    VkQueue queue, VkPipeline pipeline, VkPipelineLayout layout,
    VkDescriptorSet descriptorSet, u32 groupCountX, u32 groupCountY,
    u32 groupCountZ)
{
    VkCommandBuffer commandBuffer =
        VulkanBeginSingleTimeCommands(device, commandPool);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, pipeline);
    vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE,
        layout, 0, 1, &descriptorSet, 0, NULL);

    vkCmdDispatch(commandBuffer, groupCountX, groupCountY, groupCountZ);

    VulkanEndSingleTimeCommands(commandBuffer, device, commandPool, queue);
}
