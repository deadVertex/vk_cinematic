#pragma once

struct HdrCubeMap
{
    HdrImage images[6];
};

struct BasisVectors
{
    vec3 forward;
    vec3 up;
    vec3 right;
};

enum
{
    CubeMapFace_PositiveX,
    CubeMapFace_NegativeX,
    CubeMapFace_PositiveY,
    CubeMapFace_NegativeY,
    CubeMapFace_PositiveZ,
    CubeMapFace_NegativeZ,
};

