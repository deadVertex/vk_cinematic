#pragma once

// TODO: Find a better place for this!
// NOTE: Needs to be kept in sync with shaders
#define RS_MAX_SPHERE_LIGHTS 20
#define MAX_DISK_LIGHTS 4
#define RS_MAX_DIRECTIONAL_LIGHTS 4

struct SphereLightData
{
    vec3 position;
    vec3 radiance;
    f32 radius;
};

struct AmbientLightData
{
    vec3 radiance;
};

struct DiskLightData
{
    vec3 normal;
    vec3 position;
    vec3 radiance;
    float radius;
};

struct DirectionalLightData
{
    vec3 radiance;
    vec3 direction;
};

struct LightData
{
    u32 sphereLightCount;
    SphereLightData sphereLights[RS_MAX_SPHERE_LIGHTS];

    AmbientLightData ambientLight;
    u32 diskLightCount;
    DiskLightData diskLights[MAX_DISK_LIGHTS];

    u32 directionalLightCount;
    DirectionalLightData directionalLights[RS_MAX_DIRECTIONAL_LIGHTS];
};

struct Entity
{
    vec3 position;
    vec3 scale;
    quat rotation;
    u32 mesh;
    u32 material;
    vec3 aabbMin;
    vec3 aabbMax;
};

struct Scene
{
    Entity *entities;
    u32 count;
    u32 max;

    u32 backgroundMaterial;

    LightData *lightData;
    Aabb aabb;
};
