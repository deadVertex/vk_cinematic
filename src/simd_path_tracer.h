#pragma once

struct Basis
{
    vec3 right;
    vec3 up;
    vec3 forward;
};

struct sp_Camera
{
    Basis basis;
    vec3 position;
    vec3 filmCenter;

    HdrImage *outputImage;

    f32 halfPixelWidth;
    f32 halfPixelHeight;

    f32 halfFilmWidth;
    f32 halfFilmHeight;

};

struct sp_Context
{
    // Camera
    sp_Camera *camera;

    // Scene
    sp_Scene *scene;
    sp_LightSystem *lightSystem;

    // Material data
    sp_MaterialSystem *materialSystem;

    // Texture data

    // Config
    u32 sampleCount;
    // TODO: Bounce count

    // Feature flags
    b32 enableDirectionalLights;
    b32 enablePointLights;
};
