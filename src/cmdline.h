#pragma once

struct CommandLineArgs
{
    const char *assetDir;
    const char *inputJsonFile;
    b32 singleFrameMode;
    b32 runIntegrationTests;
    u32 sampleCount;
};
