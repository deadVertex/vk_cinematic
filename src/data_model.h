#pragma once

enum
{
    TextureSource_Disk,
    TextureSource_SingleColor,
    TextureSource_Checkerboard,
};

struct DataModelTexture
{
    u32 id;
    u32 source;
    const char *path;
    vec4 singleColor; // TODO: Not sure if we will keep this
};

struct DataModelMaterial
{
    u32 id;
    u32 albedoTexture;
    u32 emissionTexture;
    f32 roughness;
};

enum
{
    GeneratedMesh_Cube,
    GeneratedMesh_Triangle,
    GeneratedMesh_Plane,
    GeneratedMesh_Sphere,
};

struct DataModelMesh
{
    u32 id;
    u32 generator;
    const char *path;
};

struct DataModelEntity
{
    u32 id;

    u32 meshId;
    u32 materialId;

    vec3 position;
    quat rotation;
    vec3 scale;
};

struct DataModelCamera
{
    vec3 position;
    quat rotation;
};

struct DataModelDirectionalLight
{
    u32 id;
    vec3 direction;
    vec3 radiance;
};

struct DataModelPointLight
{
    u32 id;
    vec3 position;
    vec3 radiance;
};

#define MAX_ENTITIES 32
#define MAX_TEXTURES 32
#define MAX_MATERIALS 32
#define MAX_MESHES 32
#define MAX_DIRECTIONAL_LIGHTS 8
#define MAX_POINT_LIGHTS 8
struct DataModel
{
    DataModelTexture textures[MAX_TEXTURES];
    u32 textureCount;

    DataModelMaterial materials[MAX_MATERIALS];
    u32 materialCount;

    DataModelMesh meshes[MAX_MESHES];
    u32 meshCount;

    DataModelEntity entities[MAX_ENTITIES];
    u32 entityCount;

    DataModelDirectionalLight directionalLights[MAX_DIRECTIONAL_LIGHTS];
    u32 directionalLightCount;

    DataModelPointLight pointLights[MAX_POINT_LIGHTS];
    u32 pointLightCount;

    DataModelCamera camera;

    u32 backgroundMaterialId;
};

inline void RegisterTexture(DataModel *dataModel, u32 id, vec4 singleColor)
{
    Assert(dataModel->textureCount < MAX_TEXTURES);
    DataModelTexture *texture = dataModel->textures + dataModel->textureCount++;
    texture->id = id;
    texture->source = TextureSource_SingleColor;
    texture->singleColor = singleColor;
}

inline void RegisterTextureCheckerboard(DataModel *dataModel, u32 id)
{
    Assert(dataModel->textureCount < MAX_TEXTURES);
    DataModelTexture *texture = dataModel->textures + dataModel->textureCount++;
    texture->id = id;
    texture->source = TextureSource_Checkerboard;
}

inline void RegisterTexture(DataModel *dataModel, u32 id, const char *path)
{
    Assert(dataModel->textureCount < MAX_TEXTURES);
    DataModelTexture *texture = dataModel->textures + dataModel->textureCount++;
    texture->id = id;
    texture->source = TextureSource_Disk;
    texture->path = path;
}

inline void RegisterMaterial(DataModel *dataModel, u32 id, DataModelMaterial material)
{
    Assert(dataModel->materialCount < MAX_MATERIALS);
    DataModelMaterial *dst = dataModel->materials + dataModel->materialCount++;

    material.id = id;
    *dst = material;
}

inline void RegisterMesh(DataModel *dataModel, u32 id, const char *path)
{
    Assert(dataModel->meshCount < MAX_MESHES);
    DataModelMesh *mesh = dataModel->meshes + dataModel->meshCount++;
    mesh->id = id;
    mesh->path = path;
}

inline void RegisterMesh(DataModel *dataModel, u32 id, u32 generator)
{
    Assert(dataModel->meshCount < MAX_MESHES);
    DataModelMesh *mesh = dataModel->meshes + dataModel->meshCount++;
    mesh->id = id;
    mesh->generator = generator;
}

inline void AddEntity(DataModel *dataModel, u32 id,
        u32 meshId, u32 materialId,
        vec3 position, quat rotation, vec3 scale)
{
    Assert(dataModel->entityCount < MAX_ENTITIES);
    DataModelEntity *entity = dataModel->entities + dataModel->entityCount++;
    entity->id = id;
    entity->meshId = meshId;
    entity->materialId = materialId;
    entity->position = position;
    entity->rotation = rotation;
    entity->scale = scale;
}

inline void ConfigureCamera(DataModel *dataModel, vec3 position, quat rotation)
{
    dataModel->camera.position = position;
    dataModel->camera.rotation = rotation;
}

inline void AddDirectionalLight(DataModel *dataModel, u32 id,
        vec3 radiance, vec3 direction)
{
    Assert(dataModel->directionalLightCount < MAX_DIRECTIONAL_LIGHTS);
    DataModelDirectionalLight *light =
        dataModel->directionalLights + dataModel->directionalLightCount++;
    light->id = id;
    light->radiance = radiance;
    light->direction = direction;
}

inline void AddPointLight(
    DataModel *dataModel, u32 id, vec3 radiance, vec3 position)
{
    Assert(dataModel->pointLightCount < MAX_POINT_LIGHTS);
    DataModelPointLight *light =
        dataModel->pointLights + dataModel->pointLightCount++;
    light->id = id;
    light->radiance = radiance;
    light->position = position;
}
