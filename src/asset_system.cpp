internal void InitializeAssetSystem(AssetSystem *assetSystem,
    MemoryArena *arena, AssetSystemConfig config)
{
    assetSystem->textures =
        Dict_CreateFromArena(arena, AssetSystemTexture, config.maxTextures);
    assetSystem->meshes =
        Dict_CreateFromArena(arena, AssetSystemMesh, config.maxMeshes);

    assetSystem->imageDataArena =
        SubAllocateArena(arena, config.imageDataArenaSize);
    assetSystem->meshDataArena =
        SubAllocateArena(arena, config.meshDataArenaSize);
}

internal b32 RegisterMeshFromDisk(AssetSystem *assetSystem,
        u32 id, const char *path)
{
    b32 result = false;
    AssetSystemMesh *mesh =
        Dict_AddItem(&assetSystem->meshes, AssetSystemMesh, id);
    if (mesh != NULL)
    {
        memset(mesh, 0, sizeof(*mesh));
        mesh->path = path;
        result = true;
    }

    return result;
}

internal b32 RegisterGeneratedMesh(
    AssetSystem *assetSystem, u32 id, u32 generatorId)
{
    b32 result = false;
    AssetSystemMesh *mesh =
        Dict_AddItem(&assetSystem->meshes, AssetSystemMesh, id);
    if (mesh != NULL)
    {
        memset(mesh, 0, sizeof(*mesh));
        mesh->generator = generatorId;
        result = true;
    }

    return result;
}

internal b32 RegisterTextureFromDisk(
    AssetSystem *assetSystem, u32 id, const char *path)
{
    b32 result = false;
    AssetSystemTexture *texture =
        Dict_AddItem(&assetSystem->textures, AssetSystemTexture, id);
    if (texture != NULL)
    {
        memset(texture, 0, sizeof(*texture));
        texture->source = AssetSystemTextureSource_Disk;
        texture->path = path;
        result = true;
    }

    return result;
}

internal b32 RegisterSingleColorTexture(AssetSystem *assetSystem, u32 id, vec4 color)
{
    b32 result = false;
    AssetSystemTexture *texture =
        Dict_AddItem(&assetSystem->textures, AssetSystemTexture, id);
    if (texture != NULL)
    {
        memset(texture, 0, sizeof(*texture));
        texture->source = AssetSystemTextureSource_SingleColor;
        texture->singleColor = color;
        result = true;
    }

    return result;
}

internal b32 RegisterCheckerboardTexture(AssetSystem *assetSystem, u32 id)
{
    b32 result = false;
    AssetSystemTexture *texture =
        Dict_AddItem(&assetSystem->textures, AssetSystemTexture, id);
    if (texture != NULL)
    {
        memset(texture, 0, sizeof(*texture));
        texture->source = AssetSystemTextureSource_Checkerboard;
        result = true;
    }

    return result;
}

internal HdrImage *GetTexture(AssetSystem *assetSystem, u32 id)
{
    HdrImage *result = NULL;
    AssetSystemTexture *texture =
        Dict_FindItem(&assetSystem->textures, AssetSystemTexture, id);
    if (texture != NULL)
    {
        result = &texture->data;
    }

    return result;
}

internal b32 HasTexture(AssetSystem *assetSystem, u32 id)
{
    b32 result =
        Dict_FindItem(&assetSystem->textures, AssetSystemTexture, id) != NULL;

    return result;
}

internal MeshData *GetMesh(AssetSystem *assetSystem, u32 id)
{
    MeshData *result = NULL;
    AssetSystemMesh *mesh =
        Dict_FindItem(&assetSystem->meshes, AssetSystemMesh, id);
    if (mesh != NULL)
    {
        result = &mesh->data;
    }

    return result;
}

internal Aabb* GetMeshAabb(AssetSystem *assetSystem, u32 id)
{
    Aabb *result = NULL;
    AssetSystemMesh *mesh =
        Dict_FindItem(&assetSystem->meshes, AssetSystemMesh, id);
    if (mesh != NULL)
    {
        result = &mesh->aabb;
    }

    return result;
}

internal b32 HasMesh(AssetSystem *assetSystem, u32 id)
{
    b32 result =
        Dict_FindItem(&assetSystem->meshes, AssetSystemMesh, id) != NULL;

    return result;
}

internal void GenerateAssetData(
    AssetSystem *assetSystem, AssetSystemGenerator generator, void *userData)
{
    for (u32 i = 0; i < assetSystem->textures.count; i++)
    {
        u32 key = assetSystem->textures.keys[i];
        AssetSystemTexture *texture =
            Dict_FindItem(&assetSystem->textures, AssetSystemTexture, key);
        Assert(texture != NULL);
        if (texture->data.pixels == NULL)
        {
            if (texture->source == AssetSystemTextureSource_SingleColor)
            {
                // TODO: Check return value?
                generator.singleColor(&texture->data,
                    &assetSystem->imageDataArena, texture->singleColor,
                    userData);
            }
            else if (texture->source == AssetSystemTextureSource_Checkerboard)
            {
                generator.checkerboard(
                    &texture->data, &assetSystem->imageDataArena, userData);
            }
        }
    }

    for (u32 i = 0; i < assetSystem->meshes.count; i++)
    {
        u32 key = assetSystem->meshes.keys[i];
        AssetSystemMesh *mesh =
            Dict_FindItem(&assetSystem->meshes, AssetSystemMesh, key);
        Assert(mesh != NULL);

        if (mesh->data.vertices == NULL)
        {
            if (mesh->path == NULL)
            {
                generator.mesh(&mesh->data, &mesh->aabb, mesh->generator,
                    &assetSystem->meshDataArena, userData);
            }
        }
    }
}

internal void LoadAssetsFromDisk(
    AssetSystem *assetSystem, AssetSystemLoader loader, void *userData)
{
    for (u32 i = 0; i < assetSystem->textures.count; i++)
    {
        u32 key = assetSystem->textures.keys[i];
        AssetSystemTexture *texture =
            Dict_FindItem(&assetSystem->textures, AssetSystemTexture, key);
        Assert(texture != NULL);

        // TODO: How to prevent reloading
        if (texture->source == AssetSystemTextureSource_Disk)
        {
            Assert(texture->path != NULL);
            b32 result = loader.readTextureFromDisk(&texture->data,
                &assetSystem->imageDataArena, texture->path, userData);
        }
    }

    for (u32 i = 0; i < assetSystem->meshes.count; i++)
    {
        u32 key = assetSystem->meshes.keys[i];
        AssetSystemMesh *mesh =
            Dict_FindItem(&assetSystem->meshes, AssetSystemMesh, key);
        Assert(mesh != NULL);

        if (mesh->path != NULL)
        {
            b32 result = loader.readMeshFromDisk(&mesh->data, &mesh->aabb,
                &assetSystem->meshDataArena, mesh->path, userData);
        }
    }
}
