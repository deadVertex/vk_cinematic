@echo off

set CONFIG=RelWithDebInfo
cmake --build cmake_build --config %CONFIG% --target main
cmake --install cmake_build --prefix . --config %CONFIG%

REM cmake --build cmake_build --config Debug --target test_runner
REM ctest --test-dir cmake_build\unit_tests -C Debug --output-on-failure
