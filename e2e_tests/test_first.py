import subprocess
import os

def test_first():
    # Clean up output images if they exist
    path_tracer_output_path = "bin/path_tracer.hdr"
    rasterizer_output_path = "bin/rasterizer.hdr"
    if os.path.exists(path_tracer_output_path):
        os.remove(path_tracer_output_path);
    if os.path.exists(rasterizer_output_path):
        os.remove(rasterizer_output_path);

    # TODO: Use log file for output, trying to read from STDOUT in Windows is a no-go
    proc = subprocess.Popen(["bin/main.exe", "--asset-dir", "../assets", "--single-frame-mode"], cwd="bin")

    # Kill process after 5 seconds
    try:
        proc.wait(5)
    except subprocess.TimeoutExpired:
        proc.terminate()

    assert(os.path.exists(path_tracer_output_path))
    assert(os.path.exists(rasterizer_output_path))
